INSERT into skllzz._sd_stepdetails (sd_id, metadata_sd)
values (nextval('skllzz.sd_sequence'), 0);
INSERT into skllzz._sd_sdd_stepdetails_day (sd_sdd_sd_id, sd_sdd_stepdetails_day, metadata_sd_sdd)
values (currval('skllzz.sd_sequence'), @stepDay, 0);
INSERT into skllzz._sd_sdm_stepdetails_meters (sd_sdm_sd_id, sd_sdm_stepdetails_meters, metadata_sd_sdm)
values (currval('skllzz.sd_sequence'), @meters, 0);
INSERT into skllzz._sd_sds_stepdetails_steps (sd_sds_sd_id, sd_sds_stepdetails_steps, metadata_sd_sds)
values (currval('skllzz.sd_sequence'), @steps, 0);
