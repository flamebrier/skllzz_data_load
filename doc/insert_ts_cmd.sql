INSERT into skllzz._ts_trainingsession (ts_id, metadata_ts)
values (nextval('skllzz.ts_sequence'), 0);
INSERT into skllzz._ts_tid_trainingsession_id (ts_tid_ts_id, ts_tid_trainingsession_id, metadata_ts_tid)
values (currval('skllzz.ts_sequence'), @id, 0);
INSERT into skllzz._ts_tkc_trainingsession_kcal (ts_tkc_ts_id, ts_tkc_trainingsession_kcal, metadata_ts_tkc)
values (currval('skllzz.ts_sequence'), @kkal, 0);
INSERT into skllzz._ts_tpr_trainingsession_profileid (ts_tpr_ts_id, ts_tpr_trainingsession_profileid, metadata_ts_tpr)
values (currval('skllzz.ts_sequence'), @profile_id, 0);
INSERT into skllzz._ts_tsi_trainingsession_sourceid (ts_tsi_ts_id, ts_tsi_trainingsession_sourceid, metadata_ts_tsi)
values (currval('skllzz.ts_sequence'), @source_id, 0);
INSERT into skllzz._ts_tsr_trainingsession_startmillis (ts_tsr_ts_id, ts_tsr_trainingsession_startmillis, metadata_ts_tsr)
values (currval('skllzz.ts_sequence'), @start_millis, 0);
INSERT into skllzz._ts_tst_trainingsession_stopmillis (ts_tst_ts_id, ts_tst_trainingsession_stopmillis, metadata_ts_tst)
values (currval('skllzz.ts_sequence'), @stop_millis, 0);
INSERT into skllzz._ts_tsy_trainingsession_syncmillis (ts_tsy_ts_id, ts_tsy_trainingsession_syncmillis, metadata_ts_tsy)
values (currval('skllzz.ts_sequence'), @sync_millis, 0);
INSERT into skllzz._ts_tsz_trainingsession_skllzz (ts_tsz_ts_id, ts_tsz_trainingsession_skllzz, metadata_ts_tsz)
values (currval('skllzz.ts_sequence'), @skllzz, 0);
INSERT into skllzz._ts_ttz_trainingsession_timezone (ts_ttz_ts_id, ts_ttz_trainingsession_timezone, metadata_ts_ttz)
values (currval('skllzz.ts_sequence'), @timezone, 0);