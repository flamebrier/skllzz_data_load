-- DATABASE INITIALIZATION -----------------------------------------------------
--
-- The following code performs the initial setup of the PostgreSQL database with
-- required objects for the anchor database.
--
--------------------------------------------------------------------------------
-- create schema
CREATE SCHEMA IF NOT EXISTS skllzz;
-- set schema search path
SET search_path = skllzz;
-- drop universal function that generates checksum values
-- DROP FUNCTION IF EXISTS skllzz.generateChecksum(text);
-- create universal function that generates checksum values
CREATE OR REPLACE FUNCTION skllzz.generateChecksum(
    value text
) RETURNS bytea AS '
    BEGIN
        return cast(
                substring(
                        MD5(value) for 16
                    ) as bytea
            );
    END;
' LANGUAGE plpgsql;
-- KNOTS --------------------------------------------------------------------------------------------------------------
--
-- Knots are used to store finite sets of values, normally used to describe states
-- of entities (through knotted attributes) or relationships (through knotted ties).
-- Knots have their own surrogate identities and are therefore immutable.
-- Values can be added to the set over time though.
-- Knots should have values that are mutually exclusive and exhaustive.
-- Knots are unfolded when using equivalence.
--
-- KNOT TRIGGERS ---------------------------------------------------------------------------------------------------
--
-- The following triggers enable calculation and storing checksum values.
--
-- ANCHORS AND ATTRIBUTES ---------------------------------------------------------------------------------------------
--
-- Anchors are used to store the identities of entities.
-- Anchors are immutable.
-- Attributes are used to store values for properties of entities.
-- Attributes are mutable, their values may change over one or more types of time.
-- Attributes have four flavors: static, historized, knotted static, and knotted historized.
-- Anchors may have zero or more adjoined attributes.
--
-- Anchor table -------------------------------------------------------------------------------------------------------
-- TS_TrainingSession table (with 11 attributes)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TrainingSession;
CREATE TABLE IF NOT EXISTS skllzz._TS_TrainingSession (
                                                          TS_ID serial not null,
                                                          Metadata_TS int not null,
                                                          constraint pkTS_TrainingSession primary key (
                                                                                                       TS_ID
                                                              )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TrainingSession CLUSTER ON pkTS_TrainingSession;
-- DROP VIEW IF EXISTS skllzz.TS_TrainingSession;
CREATE OR REPLACE VIEW skllzz.TS_TrainingSession AS SELECT
                                                        TS_ID,
                                                        Metadata_TS
                                                    FROM skllzz._TS_TrainingSession;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TID_TrainingSession_Id table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TID_TrainingSession_Id;
CREATE TABLE IF NOT EXISTS skllzz._TS_TID_TrainingSession_Id (
                                                                 TS_TID_TS_ID int not null,
                                                                 TS_TID_TrainingSession_Id varchar(100) not null,
                                                                 Metadata_TS_TID int not null,
                                                                 constraint fkTS_TID_TrainingSession_Id foreign key (
                                                                                                                     TS_TID_TS_ID
                                                                     ) references skllzz._TS_TrainingSession(TS_ID),
                                                                 constraint pkTS_TID_TrainingSession_Id primary key (
                                                                                                                     TS_TID_TS_ID
                                                                     )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TID_TrainingSession_Id CLUSTER ON pkTS_TID_TrainingSession_Id;
-- DROP VIEW IF EXISTS skllzz.TS_TID_TrainingSession_Id;
CREATE OR REPLACE VIEW skllzz.TS_TID_TrainingSession_Id AS SELECT
                                                               TS_TID_TS_ID,
                                                               TS_TID_TrainingSession_Id,
                                                               Metadata_TS_TID
                                                           FROM skllzz._TS_TID_TrainingSession_Id;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TSR_TrainingSession_StartMillis table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TSR_TrainingSession_StartMillis;
CREATE TABLE IF NOT EXISTS skllzz._TS_TSR_TrainingSession_StartMillis (
                                                                          TS_TSR_TS_ID int not null,
                                                                          TS_TSR_TrainingSession_StartMillis bigint not null,
                                                                          Metadata_TS_TSR int not null,
                                                                          constraint fkTS_TSR_TrainingSession_StartMillis foreign key (
                                                                                                                                       TS_TSR_TS_ID
                                                                              ) references skllzz._TS_TrainingSession(TS_ID),
                                                                          constraint pkTS_TSR_TrainingSession_StartMillis primary key (
                                                                                                                                       TS_TSR_TS_ID
                                                                              )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TSR_TrainingSession_StartMillis CLUSTER ON pkTS_TSR_TrainingSession_StartMillis;
-- DROP VIEW IF EXISTS skllzz.TS_TSR_TrainingSession_StartMillis;
CREATE OR REPLACE VIEW skllzz.TS_TSR_TrainingSession_StartMillis AS SELECT
                                                                        TS_TSR_TS_ID,
                                                                        TS_TSR_TrainingSession_StartMillis,
                                                                        Metadata_TS_TSR
                                                                    FROM skllzz._TS_TSR_TrainingSession_StartMillis;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TST_TrainingSession_StopMillis table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TST_TrainingSession_StopMillis;
CREATE TABLE IF NOT EXISTS skllzz._TS_TST_TrainingSession_StopMillis (
                                                                         TS_TST_TS_ID int not null,
                                                                         TS_TST_TrainingSession_StopMillis bigint not null,
                                                                         Metadata_TS_TST int not null,
                                                                         constraint fkTS_TST_TrainingSession_StopMillis foreign key (
                                                                                                                                     TS_TST_TS_ID
                                                                             ) references skllzz._TS_TrainingSession(TS_ID),
                                                                         constraint pkTS_TST_TrainingSession_StopMillis primary key (
                                                                                                                                     TS_TST_TS_ID
                                                                             )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TST_TrainingSession_StopMillis CLUSTER ON pkTS_TST_TrainingSession_StopMillis;
-- DROP VIEW IF EXISTS skllzz.TS_TST_TrainingSession_StopMillis;
CREATE OR REPLACE VIEW skllzz.TS_TST_TrainingSession_StopMillis AS SELECT
                                                                       TS_TST_TS_ID,
                                                                       TS_TST_TrainingSession_StopMillis,
                                                                       Metadata_TS_TST
                                                                   FROM skllzz._TS_TST_TrainingSession_StopMillis;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TSZ_TrainingSession_Skllzz table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TSZ_TrainingSession_Skllzz;
CREATE TABLE IF NOT EXISTS skllzz._TS_TSZ_TrainingSession_Skllzz (
                                                                     TS_TSZ_TS_ID int not null,
                                                                     TS_TSZ_TrainingSession_Skllzz DOUBLE PRECISION not null,
                                                                     Metadata_TS_TSZ int not null,
                                                                     constraint fkTS_TSZ_TrainingSession_Skllzz foreign key (
                                                                                                                             TS_TSZ_TS_ID
                                                                         ) references skllzz._TS_TrainingSession(TS_ID),
                                                                     constraint pkTS_TSZ_TrainingSession_Skllzz primary key (
                                                                                                                             TS_TSZ_TS_ID
                                                                         )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TSZ_TrainingSession_Skllzz CLUSTER ON pkTS_TSZ_TrainingSession_Skllzz;
-- DROP VIEW IF EXISTS skllzz.TS_TSZ_TrainingSession_Skllzz;
CREATE OR REPLACE VIEW skllzz.TS_TSZ_TrainingSession_Skllzz AS SELECT
                                                                   TS_TSZ_TS_ID,
                                                                   TS_TSZ_TrainingSession_Skllzz,
                                                                   Metadata_TS_TSZ
                                                               FROM skllzz._TS_TSZ_TrainingSession_Skllzz;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TDL_TrainingSession_Deleted table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TDL_TrainingSession_Deleted;
CREATE TABLE IF NOT EXISTS skllzz._TS_TDL_TrainingSession_Deleted (
                                                                      TS_TDL_TS_ID int not null,
                                                                      TS_TDL_TrainingSession_Deleted boolean not null,
                                                                      Metadata_TS_TDL int not null,
                                                                      constraint fkTS_TDL_TrainingSession_Deleted foreign key (
                                                                                                                               TS_TDL_TS_ID
                                                                          ) references skllzz._TS_TrainingSession(TS_ID),
                                                                      constraint pkTS_TDL_TrainingSession_Deleted primary key (
                                                                                                                               TS_TDL_TS_ID
                                                                          )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TDL_TrainingSession_Deleted CLUSTER ON pkTS_TDL_TrainingSession_Deleted;
-- DROP VIEW IF EXISTS skllzz.TS_TDL_TrainingSession_Deleted;
CREATE OR REPLACE VIEW skllzz.TS_TDL_TrainingSession_Deleted AS SELECT
                                                                    TS_TDL_TS_ID,
                                                                    TS_TDL_TrainingSession_Deleted,
                                                                    Metadata_TS_TDL
                                                                FROM skllzz._TS_TDL_TrainingSession_Deleted;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TSI_TrainingSession_SourceId table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TSI_TrainingSession_SourceId;
CREATE TABLE IF NOT EXISTS skllzz._TS_TSI_TrainingSession_SourceId (
                                                                       TS_TSI_TS_ID int not null,
                                                                       TS_TSI_TrainingSession_SourceId varchar(100) not null,
                                                                       Metadata_TS_TSI int not null,
                                                                       constraint fkTS_TSI_TrainingSession_SourceId foreign key (
                                                                                                                                 TS_TSI_TS_ID
                                                                           ) references skllzz._TS_TrainingSession(TS_ID),
                                                                       constraint pkTS_TSI_TrainingSession_SourceId primary key (
                                                                                                                                 TS_TSI_TS_ID
                                                                           )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TSI_TrainingSession_SourceId CLUSTER ON pkTS_TSI_TrainingSession_SourceId;
-- DROP VIEW IF EXISTS skllzz.TS_TSI_TrainingSession_SourceId;
CREATE OR REPLACE VIEW skllzz.TS_TSI_TrainingSession_SourceId AS SELECT
                                                                     TS_TSI_TS_ID,
                                                                     TS_TSI_TrainingSession_SourceId,
                                                                     Metadata_TS_TSI
                                                                 FROM skllzz._TS_TSI_TrainingSession_SourceId;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TSV_TrainingSession_Version table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TSV_TrainingSession_Version;
CREATE TABLE IF NOT EXISTS skllzz._TS_TSV_TrainingSession_Version (
                                                                      TS_TSV_TS_ID int not null,
                                                                      TS_TSV_TrainingSession_Version integer not null,
                                                                      Metadata_TS_TSV int not null,
                                                                      constraint fkTS_TSV_TrainingSession_Version foreign key (
                                                                                                                               TS_TSV_TS_ID
                                                                          ) references skllzz._TS_TrainingSession(TS_ID),
                                                                      constraint pkTS_TSV_TrainingSession_Version primary key (
                                                                                                                               TS_TSV_TS_ID
                                                                          )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TSV_TrainingSession_Version CLUSTER ON pkTS_TSV_TrainingSession_Version;
-- DROP VIEW IF EXISTS skllzz.TS_TSV_TrainingSession_Version;
CREATE OR REPLACE VIEW skllzz.TS_TSV_TrainingSession_Version AS SELECT
                                                                    TS_TSV_TS_ID,
                                                                    TS_TSV_TrainingSession_Version,
                                                                    Metadata_TS_TSV
                                                                FROM skllzz._TS_TSV_TrainingSession_Version;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TTZ_TrainingSession_Timezone table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TTZ_TrainingSession_Timezone;
CREATE TABLE IF NOT EXISTS skllzz._TS_TTZ_TrainingSession_Timezone (
                                                                       TS_TTZ_TS_ID int not null,
                                                                       TS_TTZ_TrainingSession_Timezone varchar(255) not null,
                                                                       Metadata_TS_TTZ int not null,
                                                                       constraint fkTS_TTZ_TrainingSession_Timezone foreign key (
                                                                                                                                 TS_TTZ_TS_ID
                                                                           ) references skllzz._TS_TrainingSession(TS_ID),
                                                                       constraint pkTS_TTZ_TrainingSession_Timezone primary key (
                                                                                                                                 TS_TTZ_TS_ID
                                                                           )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TTZ_TrainingSession_Timezone CLUSTER ON pkTS_TTZ_TrainingSession_Timezone;
-- DROP VIEW IF EXISTS skllzz.TS_TTZ_TrainingSession_Timezone;
CREATE OR REPLACE VIEW skllzz.TS_TTZ_TrainingSession_Timezone AS SELECT
                                                                     TS_TTZ_TS_ID,
                                                                     TS_TTZ_TrainingSession_Timezone,
                                                                     Metadata_TS_TTZ
                                                                 FROM skllzz._TS_TTZ_TrainingSession_Timezone;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TSY_TrainingSession_SyncMillis table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TSY_TrainingSession_SyncMillis;
CREATE TABLE IF NOT EXISTS skllzz._TS_TSY_TrainingSession_SyncMillis (
                                                                         TS_TSY_TS_ID int not null,
                                                                         TS_TSY_TrainingSession_SyncMillis bigint not null,
                                                                         Metadata_TS_TSY int not null,
                                                                         constraint fkTS_TSY_TrainingSession_SyncMillis foreign key (
                                                                                                                                     TS_TSY_TS_ID
                                                                             ) references skllzz._TS_TrainingSession(TS_ID),
                                                                         constraint pkTS_TSY_TrainingSession_SyncMillis primary key (
                                                                                                                                     TS_TSY_TS_ID
                                                                             )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TSY_TrainingSession_SyncMillis CLUSTER ON pkTS_TSY_TrainingSession_SyncMillis;
-- DROP VIEW IF EXISTS skllzz.TS_TSY_TrainingSession_SyncMillis;
CREATE OR REPLACE VIEW skllzz.TS_TSY_TrainingSession_SyncMillis AS SELECT
                                                                       TS_TSY_TS_ID,
                                                                       TS_TSY_TrainingSession_SyncMillis,
                                                                       Metadata_TS_TSY
                                                                   FROM skllzz._TS_TSY_TrainingSession_SyncMillis;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TKC_TrainingSession_KCal table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TKC_TrainingSession_KCal;
CREATE TABLE IF NOT EXISTS skllzz._TS_TKC_TrainingSession_KCal (
                                                                   TS_TKC_TS_ID int not null,
                                                                   TS_TKC_TrainingSession_KCal DOUBLE PRECISION not null,
                                                                   Metadata_TS_TKC int not null,
                                                                   constraint fkTS_TKC_TrainingSession_KCal foreign key (
                                                                                                                         TS_TKC_TS_ID
                                                                       ) references skllzz._TS_TrainingSession(TS_ID),
                                                                   constraint pkTS_TKC_TrainingSession_KCal primary key (
                                                                                                                         TS_TKC_TS_ID
                                                                       )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TKC_TrainingSession_KCal CLUSTER ON pkTS_TKC_TrainingSession_KCal;
-- DROP VIEW IF EXISTS skllzz.TS_TKC_TrainingSession_KCal;
CREATE OR REPLACE VIEW skllzz.TS_TKC_TrainingSession_KCal AS SELECT
                                                                 TS_TKC_TS_ID,
                                                                 TS_TKC_TrainingSession_KCal,
                                                                 Metadata_TS_TKC
                                                             FROM skllzz._TS_TKC_TrainingSession_KCal;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- TS_TPR_TrainingSession_ProfileId table (on TS_TrainingSession)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_TPR_TrainingSession_ProfileId;
CREATE TABLE IF NOT EXISTS skllzz._TS_TPR_TrainingSession_ProfileId (
                                                                        TS_TPR_TS_ID int not null,
                                                                        TS_TPR_TrainingSession_ProfileId varchar(100) not null,
                                                                        Metadata_TS_TPR int not null,
                                                                        constraint fkTS_TPR_TrainingSession_ProfileId foreign key (
                                                                                                                                   TS_TPR_TS_ID
                                                                            ) references skllzz._TS_TrainingSession(TS_ID),
                                                                        constraint pkTS_TPR_TrainingSession_ProfileId primary key (
                                                                                                                                   TS_TPR_TS_ID
                                                                            )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_TPR_TrainingSession_ProfileId CLUSTER ON pkTS_TPR_TrainingSession_ProfileId;
-- DROP VIEW IF EXISTS skllzz.TS_TPR_TrainingSession_ProfileId;
CREATE OR REPLACE VIEW skllzz.TS_TPR_TrainingSession_ProfileId AS SELECT
                                                                      TS_TPR_TS_ID,
                                                                      TS_TPR_TrainingSession_ProfileId,
                                                                      Metadata_TS_TPR
                                                                  FROM skllzz._TS_TPR_TrainingSession_ProfileId;
-- Anchor table -------------------------------------------------------------------------------------------------------
-- SD_StepDetails table (with 3 attributes)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._SD_StepDetails;
CREATE TABLE IF NOT EXISTS skllzz._SD_StepDetails (
                                                      SD_ID serial not null,
                                                      Metadata_SD int not null,
                                                      constraint pkSD_StepDetails primary key (
                                                                                               SD_ID
                                                          )
);
ALTER TABLE IF EXISTS ONLY skllzz._SD_StepDetails CLUSTER ON pkSD_StepDetails;
-- DROP VIEW IF EXISTS skllzz.SD_StepDetails;
CREATE OR REPLACE VIEW skllzz.SD_StepDetails AS SELECT
                                                    SD_ID,
                                                    Metadata_SD
                                                FROM skllzz._SD_StepDetails;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- SD_SDS_StepDetails_Steps table (on SD_StepDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._SD_SDS_StepDetails_Steps;
CREATE TABLE IF NOT EXISTS skllzz._SD_SDS_StepDetails_Steps (
                                                                SD_SDS_SD_ID int not null,
                                                                SD_SDS_StepDetails_Steps integer not null,
                                                                Metadata_SD_SDS int not null,
                                                                constraint fkSD_SDS_StepDetails_Steps foreign key (
                                                                                                                   SD_SDS_SD_ID
                                                                    ) references skllzz._SD_StepDetails(SD_ID),
                                                                constraint pkSD_SDS_StepDetails_Steps primary key (
                                                                                                                   SD_SDS_SD_ID
                                                                    )
);
ALTER TABLE IF EXISTS ONLY skllzz._SD_SDS_StepDetails_Steps CLUSTER ON pkSD_SDS_StepDetails_Steps;
-- DROP VIEW IF EXISTS skllzz.SD_SDS_StepDetails_Steps;
CREATE OR REPLACE VIEW skllzz.SD_SDS_StepDetails_Steps AS SELECT
                                                              SD_SDS_SD_ID,
                                                              SD_SDS_StepDetails_Steps,
                                                              Metadata_SD_SDS
                                                          FROM skllzz._SD_SDS_StepDetails_Steps;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- SD_SDD_StepDetails_Day table (on SD_StepDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._SD_SDD_StepDetails_Day;
CREATE TABLE IF NOT EXISTS skllzz._SD_SDD_StepDetails_Day (
                                                              SD_SDD_SD_ID int not null,
                                                              SD_SDD_StepDetails_Day integer not null,
                                                              Metadata_SD_SDD int not null,
                                                              constraint fkSD_SDD_StepDetails_Day foreign key (
                                                                                                               SD_SDD_SD_ID
                                                                  ) references skllzz._SD_StepDetails(SD_ID),
                                                              constraint pkSD_SDD_StepDetails_Day primary key (
                                                                                                               SD_SDD_SD_ID
                                                                  )
);
ALTER TABLE IF EXISTS ONLY skllzz._SD_SDD_StepDetails_Day CLUSTER ON pkSD_SDD_StepDetails_Day;
-- DROP VIEW IF EXISTS skllzz.SD_SDD_StepDetails_Day;
CREATE OR REPLACE VIEW skllzz.SD_SDD_StepDetails_Day AS SELECT
                                                            SD_SDD_SD_ID,
                                                            SD_SDD_StepDetails_Day,
                                                            Metadata_SD_SDD
                                                        FROM skllzz._SD_SDD_StepDetails_Day;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- SD_SDM_StepDetails_Meters table (on SD_StepDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._SD_SDM_StepDetails_Meters;
CREATE TABLE IF NOT EXISTS skllzz._SD_SDM_StepDetails_Meters (
                                                                 SD_SDM_SD_ID int not null,
                                                                 SD_SDM_StepDetails_Meters integer not null,
                                                                 Metadata_SD_SDM int not null,
                                                                 constraint fkSD_SDM_StepDetails_Meters foreign key (
                                                                                                                     SD_SDM_SD_ID
                                                                     ) references skllzz._SD_StepDetails(SD_ID),
                                                                 constraint pkSD_SDM_StepDetails_Meters primary key (
                                                                                                                     SD_SDM_SD_ID
                                                                     )
);
ALTER TABLE IF EXISTS ONLY skllzz._SD_SDM_StepDetails_Meters CLUSTER ON pkSD_SDM_StepDetails_Meters;
-- DROP VIEW IF EXISTS skllzz.SD_SDM_StepDetails_Meters;
CREATE OR REPLACE VIEW skllzz.SD_SDM_StepDetails_Meters AS SELECT
                                                               SD_SDM_SD_ID,
                                                               SD_SDM_StepDetails_Meters,
                                                               Metadata_SD_SDM
                                                           FROM skllzz._SD_SDM_StepDetails_Meters;
-- Anchor table -------------------------------------------------------------------------------------------------------
-- HD_HrDetails table (with 7 attributes)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_HrDetails;
CREATE TABLE IF NOT EXISTS skllzz._HD_HrDetails (
                                                    HD_ID serial not null,
                                                    Metadata_HD int not null,
                                                    constraint pkHD_HrDetails primary key (
                                                                                           HD_ID
                                                        )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_HrDetails CLUSTER ON pkHD_HrDetails;
-- DROP VIEW IF EXISTS skllzz.HD_HrDetails;
CREATE OR REPLACE VIEW skllzz.HD_HrDetails AS SELECT
                                                  HD_ID,
                                                  Metadata_HD
                                              FROM skllzz._HD_HrDetails;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- HD_HMN_HrDetails_MinHr table (on HD_HrDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_HMN_HrDetails_MinHr;
CREATE TABLE IF NOT EXISTS skllzz._HD_HMN_HrDetails_MinHr (
                                                              HD_HMN_HD_ID int not null,
                                                              HD_HMN_HrDetails_MinHr integer not null,
                                                              Metadata_HD_HMN int not null,
                                                              constraint fkHD_HMN_HrDetails_MinHr foreign key (
                                                                                                               HD_HMN_HD_ID
                                                                  ) references skllzz._HD_HrDetails(HD_ID),
                                                              constraint pkHD_HMN_HrDetails_MinHr primary key (
                                                                                                               HD_HMN_HD_ID
                                                                  )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_HMN_HrDetails_MinHr CLUSTER ON pkHD_HMN_HrDetails_MinHr;
-- DROP VIEW IF EXISTS skllzz.HD_HMN_HrDetails_MinHr;
CREATE OR REPLACE VIEW skllzz.HD_HMN_HrDetails_MinHr AS SELECT
                                                            HD_HMN_HD_ID,
                                                            HD_HMN_HrDetails_MinHr,
                                                            Metadata_HD_HMN
                                                        FROM skllzz._HD_HMN_HrDetails_MinHr;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- HD_HAV_HrDetails_AvgHr table (on HD_HrDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_HAV_HrDetails_AvgHr;
CREATE TABLE IF NOT EXISTS skllzz._HD_HAV_HrDetails_AvgHr (
                                                              HD_HAV_HD_ID int not null,
                                                              HD_HAV_HrDetails_AvgHr integer not null,
                                                              Metadata_HD_HAV int not null,
                                                              constraint fkHD_HAV_HrDetails_AvgHr foreign key (
                                                                                                               HD_HAV_HD_ID
                                                                  ) references skllzz._HD_HrDetails(HD_ID),
                                                              constraint pkHD_HAV_HrDetails_AvgHr primary key (
                                                                                                               HD_HAV_HD_ID
                                                                  )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_HAV_HrDetails_AvgHr CLUSTER ON pkHD_HAV_HrDetails_AvgHr;
-- DROP VIEW IF EXISTS skllzz.HD_HAV_HrDetails_AvgHr;
CREATE OR REPLACE VIEW skllzz.HD_HAV_HrDetails_AvgHr AS SELECT
                                                            HD_HAV_HD_ID,
                                                            HD_HAV_HrDetails_AvgHr,
                                                            Metadata_HD_HAV
                                                        FROM skllzz._HD_HAV_HrDetails_AvgHr;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- HD_MHR_HrDetails_MaxHr table (on HD_HrDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_MHR_HrDetails_MaxHr;
CREATE TABLE IF NOT EXISTS skllzz._HD_MHR_HrDetails_MaxHr (
                                                              HD_MHR_HD_ID int not null,
                                                              HD_MHR_HrDetails_MaxHr integer not null,
                                                              Metadata_HD_MHR int not null,
                                                              constraint fkHD_MHR_HrDetails_MaxHr foreign key (
                                                                                                               HD_MHR_HD_ID
                                                                  ) references skllzz._HD_HrDetails(HD_ID),
                                                              constraint pkHD_MHR_HrDetails_MaxHr primary key (
                                                                                                               HD_MHR_HD_ID
                                                                  )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_MHR_HrDetails_MaxHr CLUSTER ON pkHD_MHR_HrDetails_MaxHr;
-- DROP VIEW IF EXISTS skllzz.HD_MHR_HrDetails_MaxHr;
CREATE OR REPLACE VIEW skllzz.HD_MHR_HrDetails_MaxHr AS SELECT
                                                            HD_MHR_HD_ID,
                                                            HD_MHR_HrDetails_MaxHr,
                                                            Metadata_HD_MHR
                                                        FROM skllzz._HD_MHR_HrDetails_MaxHr;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- HD_MNH_HrDetails_MinHardness table (on HD_HrDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_MNH_HrDetails_MinHardness;
CREATE TABLE IF NOT EXISTS skllzz._HD_MNH_HrDetails_MinHardness (
                                                                    HD_MNH_HD_ID int not null,
                                                                    HD_MNH_HrDetails_MinHardness double precision not null,
                                                                    Metadata_HD_MNH int not null,
                                                                    constraint fkHD_MNH_HrDetails_MinHardness foreign key (
                                                                                                                           HD_MNH_HD_ID
                                                                        ) references skllzz._HD_HrDetails(HD_ID),
                                                                    constraint pkHD_MNH_HrDetails_MinHardness primary key (
                                                                                                                           HD_MNH_HD_ID
                                                                        )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_MNH_HrDetails_MinHardness CLUSTER ON pkHD_MNH_HrDetails_MinHardness;
-- DROP VIEW IF EXISTS skllzz.HD_MNH_HrDetails_MinHardness;
CREATE OR REPLACE VIEW skllzz.HD_MNH_HrDetails_MinHardness AS SELECT
                                                                  HD_MNH_HD_ID,
                                                                  HD_MNH_HrDetails_MinHardness,
                                                                  Metadata_HD_MNH
                                                              FROM skllzz._HD_MNH_HrDetails_MinHardness;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- HD_AHR_HrDetails_AvgHardness table (on HD_HrDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_AHR_HrDetails_AvgHardness;
CREATE TABLE IF NOT EXISTS skllzz._HD_AHR_HrDetails_AvgHardness (
                                                                    HD_AHR_HD_ID int not null,
                                                                    HD_AHR_HrDetails_AvgHardness double precision not null,
                                                                    Metadata_HD_AHR int not null,
                                                                    constraint fkHD_AHR_HrDetails_AvgHardness foreign key (
                                                                                                                           HD_AHR_HD_ID
                                                                        ) references skllzz._HD_HrDetails(HD_ID),
                                                                    constraint pkHD_AHR_HrDetails_AvgHardness primary key (
                                                                                                                           HD_AHR_HD_ID
                                                                        )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_AHR_HrDetails_AvgHardness CLUSTER ON pkHD_AHR_HrDetails_AvgHardness;
-- DROP VIEW IF EXISTS skllzz.HD_AHR_HrDetails_AvgHardness;
CREATE OR REPLACE VIEW skllzz.HD_AHR_HrDetails_AvgHardness AS SELECT
                                                                  HD_AHR_HD_ID,
                                                                  HD_AHR_HrDetails_AvgHardness,
                                                                  Metadata_HD_AHR
                                                              FROM skllzz._HD_AHR_HrDetails_AvgHardness;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- HD_MXH_HrDetails_MaxHardness table (on HD_HrDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_MXH_HrDetails_MaxHardness;
CREATE TABLE IF NOT EXISTS skllzz._HD_MXH_HrDetails_MaxHardness (
                                                                    HD_MXH_HD_ID int not null,
                                                                    HD_MXH_HrDetails_MaxHardness double precision not null,
                                                                    Metadata_HD_MXH int not null,
                                                                    constraint fkHD_MXH_HrDetails_MaxHardness foreign key (
                                                                                                                           HD_MXH_HD_ID
                                                                        ) references skllzz._HD_HrDetails(HD_ID),
                                                                    constraint pkHD_MXH_HrDetails_MaxHardness primary key (
                                                                                                                           HD_MXH_HD_ID
                                                                        )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_MXH_HrDetails_MaxHardness CLUSTER ON pkHD_MXH_HrDetails_MaxHardness;
-- DROP VIEW IF EXISTS skllzz.HD_MXH_HrDetails_MaxHardness;
CREATE OR REPLACE VIEW skllzz.HD_MXH_HrDetails_MaxHardness AS SELECT
                                                                  HD_MXH_HD_ID,
                                                                  HD_MXH_HrDetails_MaxHardness,
                                                                  Metadata_HD_MXH
                                                              FROM skllzz._HD_MXH_HrDetails_MaxHardness;
-- Static attribute table ---------------------------------------------------------------------------------------------
-- HD_HRD_HrDetails_Device table (on HD_HrDetails)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._HD_HRD_HrDetails_Device;
CREATE TABLE IF NOT EXISTS skllzz._HD_HRD_HrDetails_Device (
                                                               HD_HRD_HD_ID int not null,
                                                               HD_HRD_HrDetails_Device varchar(100)[] not null,
                                                               Metadata_HD_HRD int not null,
                                                               constraint fkHD_HRD_HrDetails_Device foreign key (
                                                                                                                 HD_HRD_HD_ID
                                                                   ) references skllzz._HD_HrDetails(HD_ID),
                                                               constraint pkHD_HRD_HrDetails_Device primary key (
                                                                                                                 HD_HRD_HD_ID
                                                                   )
);
ALTER TABLE IF EXISTS ONLY skllzz._HD_HRD_HrDetails_Device CLUSTER ON pkHD_HRD_HrDetails_Device;
-- DROP VIEW IF EXISTS skllzz.HD_HRD_HrDetails_Device;
CREATE OR REPLACE VIEW skllzz.HD_HRD_HrDetails_Device AS SELECT
                                                             HD_HRD_HD_ID,
                                                             HD_HRD_HrDetails_Device,
                                                             Metadata_HD_HRD
                                                         FROM skllzz._HD_HRD_HrDetails_Device;
-- TIES ---------------------------------------------------------------------------------------------------------------
--
-- Ties are used to represent relationships between entities.
-- They come in four flavors: static, historized, knotted static, and knotted historized.
-- Ties have cardinality, constraining how members may participate in the relationship.
-- Every entity that is a member in a tie has a specified role in the relationship.
-- Ties must have at least two anchor roles and zero or more knot roles.
--
-- Static tie table ---------------------------------------------------------------------------------------------------
-- TS_holds_SD_containedIn table (having 2 roles)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_holds_SD_containedIn;
CREATE TABLE IF NOT EXISTS skllzz._TS_holds_SD_containedIn (
                                                               TS_ID_holds int not null,
                                                               SD_ID_containedIn int not null,
                                                               Metadata_TS_holds_SD_containedIn int not null,
                                                               constraint TS_holds_SD_containedIn_fkTS_holds foreign key (
                                                                                                                          TS_ID_holds
                                                                   ) references skllzz._TS_TrainingSession(TS_ID),
                                                               constraint TS_holds_SD_containedIn_fkSD_containedIn foreign key (
                                                                                                                                SD_ID_containedIn
                                                                   ) references skllzz._SD_StepDetails(SD_ID),
                                                               constraint pkTS_holds_SD_containedIn primary key (
                                                                                                                 TS_ID_holds,
                                                                                                                 SD_ID_containedIn
                                                                   )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_holds_SD_containedIn CLUSTER ON pkTS_holds_SD_containedIn;
-- DROP VIEW IF EXISTS skllzz.TS_holds_SD_containedIn;
CREATE OR REPLACE VIEW skllzz.TS_holds_SD_containedIn AS SELECT
                                                             TS_ID_holds,
                                                             SD_ID_containedIn,
                                                             Metadata_TS_holds_SD_containedIn
                                                         FROM skllzz._TS_holds_SD_containedIn;
-- Static tie table ---------------------------------------------------------------------------------------------------
-- TS_holds_HD_containedIn table (having 2 roles)
-----------------------------------------------------------------------------------------------------------------------
-- DROP TABLE IF EXISTS skllzz._TS_holds_HD_containedIn;
CREATE TABLE IF NOT EXISTS skllzz._TS_holds_HD_containedIn (
                                                               TS_ID_holds int not null,
                                                               HD_ID_containedIn int not null,
                                                               Metadata_TS_holds_HD_containedIn int not null,
                                                               constraint TS_holds_HD_containedIn_fkTS_holds foreign key (
                                                                                                                          TS_ID_holds
                                                                   ) references skllzz._TS_TrainingSession(TS_ID),
                                                               constraint TS_holds_HD_containedIn_fkHD_containedIn foreign key (
                                                                                                                                HD_ID_containedIn
                                                                   ) references skllzz._HD_HrDetails(HD_ID),
                                                               constraint pkTS_holds_HD_containedIn primary key (
                                                                                                                 TS_ID_holds,
                                                                                                                 HD_ID_containedIn
                                                                   )
);
ALTER TABLE IF EXISTS ONLY skllzz._TS_holds_HD_containedIn CLUSTER ON pkTS_holds_HD_containedIn;
-- DROP VIEW IF EXISTS skllzz.TS_holds_HD_containedIn;
CREATE OR REPLACE VIEW skllzz.TS_holds_HD_containedIn AS SELECT
                                                             TS_ID_holds,
                                                             HD_ID_containedIn,
                                                             Metadata_TS_holds_HD_containedIn
                                                         FROM skllzz._TS_holds_HD_containedIn;
-- KEY GENERATORS -----------------------------------------------------------------------------------------------------
--
-- These stored procedures can be used to generate identities of entities.
-- Corresponding anchors must have an incrementing identity column.
--
-- Key Generation Stored Procedure ------------------------------------------------------------------------------------
-- kTS_TrainingSession identity by surrogate key generation stored procedure
-----------------------------------------------------------------------------------------------------------------------
--DROP FUNCTION IF EXISTS skllzz.kTS_TrainingSession(
-- bigint,
-- int
--);
CREATE OR REPLACE FUNCTION skllzz.kTS_TrainingSession(
    requestedNumberOfIdentities bigint,
    metadata int
) RETURNS void AS '
    BEGIN
        IF requestedNumberOfIdentities > 0
        THEN
            WITH RECURSIVE idGenerator (idNumber) AS (
                SELECT
                    1
                UNION ALL
                SELECT
                        idNumber + 1
                FROM
                    idGenerator
                WHERE
                        idNumber < requestedNumberOfIdentities
            )
            INSERT INTO skllzz.TS_TrainingSession (
                Metadata_TS
            )
            SELECT
                metadata
            FROM
                idGenerator;
        END IF;
    END;
' LANGUAGE plpgsql;
-- Key Generation Stored Procedure ------------------------------------------------------------------------------------
-- kSD_StepDetails identity by surrogate key generation stored procedure
-----------------------------------------------------------------------------------------------------------------------
--DROP FUNCTION IF EXISTS skllzz.kSD_StepDetails(
-- bigint,
-- int
--);
CREATE OR REPLACE FUNCTION skllzz.kSD_StepDetails(
    requestedNumberOfIdentities bigint,
    metadata int
) RETURNS void AS '
    BEGIN
        IF requestedNumberOfIdentities > 0
        THEN
            WITH RECURSIVE idGenerator (idNumber) AS (
                SELECT
                    1
                UNION ALL
                SELECT
                        idNumber + 1
                FROM
                    idGenerator
                WHERE
                        idNumber < requestedNumberOfIdentities
            )
            INSERT INTO skllzz.SD_StepDetails (
                Metadata_SD
            )
            SELECT
                metadata
            FROM
                idGenerator;
        END IF;
    END;
' LANGUAGE plpgsql;
-- Key Generation Stored Procedure ------------------------------------------------------------------------------------
-- kHD_HrDetails identity by surrogate key generation stored procedure
-----------------------------------------------------------------------------------------------------------------------
--DROP FUNCTION IF EXISTS skllzz.kHD_HrDetails(
-- bigint,
-- int
--);
CREATE OR REPLACE FUNCTION skllzz.kHD_HrDetails(
    requestedNumberOfIdentities bigint,
    metadata int
) RETURNS void AS '
    BEGIN
        IF requestedNumberOfIdentities > 0
        THEN
            WITH RECURSIVE idGenerator (idNumber) AS (
                SELECT
                    1
                UNION ALL
                SELECT
                        idNumber + 1
                FROM
                    idGenerator
                WHERE
                        idNumber < requestedNumberOfIdentities
            )
            INSERT INTO skllzz.HD_HrDetails (
                Metadata_HD
            )
            SELECT
                metadata
            FROM
                idGenerator;
        END IF;
    END;
' LANGUAGE plpgsql;
-- ATTRIBUTE REWINDERS ------------------------------------------------------------------------------------------------
--
-- These table valued functions rewind an attribute table to the given
-- point in changing time. It does not pick a temporal perspective and
-- instead shows all rows that have been in effect before that point
-- in time.
--
-- changingTimepoint the point in changing time to rewind to
--
-- ANCHOR TEMPORAL PERSPECTIVES ---------------------------------------------------------------------------------------
--
-- These functions simplify temporal querying by providing a temporal
-- perspective of each anchor. There are four types of perspectives: latest,
-- point-in-time, difference, and now. They also denormalize the anchor, its attributes,
-- and referenced knots from sixth to third normal form.
--
-- The latest perspective shows the latest available information for each anchor.
-- The now perspective shows the information as it is right now.
-- The point-in-time perspective lets you travel through the information to the given timepoint.
--
-- changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints, and for
-- changes in all or a selection of attributes.
--
-- intervalStart the start of the interval for finding changes
-- intervalEnd the end of the interval for finding changes
-- selection a list of mnemonics for tracked attributes, ie 'MNE MON ICS', or null for all
--
-- Under equivalence all these views default to equivalent = 0, however, corresponding
-- prepended-e perspectives are provided in order to select a specific equivalent.
--
-- equivalent the equivalent for which to retrieve data
--
-- DROP ANCHOR TEMPORAL PERSPECTIVES ----------------------------------------------------------------------------------
/*
DROP FUNCTION IF EXISTS skllzz.dTS_TrainingSession(
    timestamp without time zone,
    timestamp without time zone,
    text
);
DROP VIEW IF EXISTS skllzz.nTS_TrainingSession;
DROP FUNCTION IF EXISTS skllzz.pTS_TrainingSession(
    timestamp without time zone
);
DROP VIEW IF EXISTS skllzz.lTS_TrainingSession;
DROP FUNCTION IF EXISTS skllzz.dSD_StepDetails(
    timestamp without time zone,
    timestamp without time zone,
    text
);
DROP VIEW IF EXISTS skllzz.nSD_StepDetails;
DROP FUNCTION IF EXISTS skllzz.pSD_StepDetails(
    timestamp without time zone
);
DROP VIEW IF EXISTS skllzz.lSD_StepDetails;
DROP FUNCTION IF EXISTS skllzz.dHD_HrDetails(
    timestamp without time zone,
    timestamp without time zone,
    text
);
DROP VIEW IF EXISTS skllzz.nHD_HrDetails;
DROP FUNCTION IF EXISTS skllzz.pHD_HrDetails(
    timestamp without time zone
);
DROP VIEW IF EXISTS skllzz.lHD_HrDetails;
*/
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lTS_TrainingSession viewed by the latest available information (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.lTS_TrainingSession AS
SELECT
    TS.TS_ID,
    TS.Metadata_TS,
    TID.TS_TID_TS_ID,
    TID.Metadata_TS_TID,
    TID.TS_TID_TrainingSession_Id,
    TSR.TS_TSR_TS_ID,
    TSR.Metadata_TS_TSR,
    TSR.TS_TSR_TrainingSession_StartMillis,
    TST.TS_TST_TS_ID,
    TST.Metadata_TS_TST,
    TST.TS_TST_TrainingSession_StopMillis,
    TSZ.TS_TSZ_TS_ID,
    TSZ.Metadata_TS_TSZ,
    TSZ.TS_TSZ_TrainingSession_Skllzz,
    TDL.TS_TDL_TS_ID,
    TDL.Metadata_TS_TDL,
    TDL.TS_TDL_TrainingSession_Deleted,
    TSI.TS_TSI_TS_ID,
    TSI.Metadata_TS_TSI,
    TSI.TS_TSI_TrainingSession_SourceId,
    TSV.TS_TSV_TS_ID,
    TSV.Metadata_TS_TSV,
    TSV.TS_TSV_TrainingSession_Version,
    TTZ.TS_TTZ_TS_ID,
    TTZ.Metadata_TS_TTZ,
    TTZ.TS_TTZ_TrainingSession_Timezone,
    TSY.TS_TSY_TS_ID,
    TSY.Metadata_TS_TSY,
    TSY.TS_TSY_TrainingSession_SyncMillis,
    TKC.TS_TKC_TS_ID,
    TKC.Metadata_TS_TKC,
    TKC.TS_TKC_TrainingSession_KCal,
    TPR.TS_TPR_TS_ID,
    TPR.Metadata_TS_TPR,
    TPR.TS_TPR_TrainingSession_ProfileId
FROM
    skllzz.TS_TrainingSession TS
        LEFT JOIN
    skllzz.TS_TID_TrainingSession_Id TID
    ON
            TID.TS_TID_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TSR_TrainingSession_StartMillis TSR
    ON
            TSR.TS_TSR_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TST_TrainingSession_StopMillis TST
    ON
            TST.TS_TST_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TSZ_TrainingSession_Skllzz TSZ
    ON
            TSZ.TS_TSZ_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TDL_TrainingSession_Deleted TDL
    ON
            TDL.TS_TDL_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TSI_TrainingSession_SourceId TSI
    ON
            TSI.TS_TSI_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TSV_TrainingSession_Version TSV
    ON
            TSV.TS_TSV_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TTZ_TrainingSession_Timezone TTZ
    ON
            TTZ.TS_TTZ_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TSY_TrainingSession_SyncMillis TSY
    ON
            TSY.TS_TSY_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TKC_TrainingSession_KCal TKC
    ON
            TKC.TS_TKC_TS_ID = TS.TS_ID
        LEFT JOIN
    skllzz.TS_TPR_TrainingSession_ProfileId TPR
    ON
            TPR.TS_TPR_TS_ID = TS.TS_ID;
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lSD_StepDetails viewed by the latest available information (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.lSD_StepDetails AS
SELECT
    SD.SD_ID,
    SD.Metadata_SD,
    SDS.SD_SDS_SD_ID,
    SDS.Metadata_SD_SDS,
    SDS.SD_SDS_StepDetails_Steps,
    SDD.SD_SDD_SD_ID,
    SDD.Metadata_SD_SDD,
    SDD.SD_SDD_StepDetails_Day,
    SDM.SD_SDM_SD_ID,
    SDM.Metadata_SD_SDM,
    SDM.SD_SDM_StepDetails_Meters
FROM
    skllzz.SD_StepDetails SD
        LEFT JOIN
    skllzz.SD_SDS_StepDetails_Steps SDS
    ON
            SDS.SD_SDS_SD_ID = SD.SD_ID
        LEFT JOIN
    skllzz.SD_SDD_StepDetails_Day SDD
    ON
            SDD.SD_SDD_SD_ID = SD.SD_ID
        LEFT JOIN
    skllzz.SD_SDM_StepDetails_Meters SDM
    ON
            SDM.SD_SDM_SD_ID = SD.SD_ID;
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lHD_HrDetails viewed by the latest available information (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.lHD_HrDetails AS
SELECT
    HD.HD_ID,
    HD.Metadata_HD,
    HMN.HD_HMN_HD_ID,
    HMN.Metadata_HD_HMN,
    HMN.HD_HMN_HrDetails_MinHr,
    HAV.HD_HAV_HD_ID,
    HAV.Metadata_HD_HAV,
    HAV.HD_HAV_HrDetails_AvgHr,
    MHR.HD_MHR_HD_ID,
    MHR.Metadata_HD_MHR,
    MHR.HD_MHR_HrDetails_MaxHr,
    MNH.HD_MNH_HD_ID,
    MNH.Metadata_HD_MNH,
    MNH.HD_MNH_HrDetails_MinHardness,
    AHR.HD_AHR_HD_ID,
    AHR.Metadata_HD_AHR,
    AHR.HD_AHR_HrDetails_AvgHardness,
    MXH.HD_MXH_HD_ID,
    MXH.Metadata_HD_MXH,
    MXH.HD_MXH_HrDetails_MaxHardness,
    HRD.HD_HRD_HD_ID,
    HRD.Metadata_HD_HRD,
    HRD.HD_HRD_HrDetails_Device
FROM
    skllzz.HD_HrDetails HD
        LEFT JOIN
    skllzz.HD_HMN_HrDetails_MinHr HMN
    ON
            HMN.HD_HMN_HD_ID = HD.HD_ID
        LEFT JOIN
    skllzz.HD_HAV_HrDetails_AvgHr HAV
    ON
            HAV.HD_HAV_HD_ID = HD.HD_ID
        LEFT JOIN
    skllzz.HD_MHR_HrDetails_MaxHr MHR
    ON
            MHR.HD_MHR_HD_ID = HD.HD_ID
        LEFT JOIN
    skllzz.HD_MNH_HrDetails_MinHardness MNH
    ON
            MNH.HD_MNH_HD_ID = HD.HD_ID
        LEFT JOIN
    skllzz.HD_AHR_HrDetails_AvgHardness AHR
    ON
            AHR.HD_AHR_HD_ID = HD.HD_ID
        LEFT JOIN
    skllzz.HD_MXH_HrDetails_MaxHardness MXH
    ON
            MXH.HD_MXH_HD_ID = HD.HD_ID
        LEFT JOIN
    skllzz.HD_HRD_HrDetails_Device HRD
    ON
            HRD.HD_HRD_HD_ID = HD.HD_ID;
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pTS_TrainingSession viewed as it was on the given timepoint
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION skllzz.pTS_TrainingSession (
    changingTimepoint timestamp without time zone
)
    RETURNS TABLE (
                      TS_ID int,
                      Metadata_TS int,
                      TS_TID_TS_ID int,
                      Metadata_TS_TID int,
                      TS_TID_TrainingSession_Id varchar(100),
                      TS_TSR_TS_ID int,
                      Metadata_TS_TSR int,
                      TS_TSR_TrainingSession_StartMillis bigint,
                      TS_TST_TS_ID int,
                      Metadata_TS_TST int,
                      TS_TST_TrainingSession_StopMillis bigint,
                      TS_TSZ_TS_ID int,
                      Metadata_TS_TSZ int,
                      TS_TSZ_TrainingSession_Skllzz DOUBLE PRECISION,
                      TS_TDL_TS_ID int,
                      Metadata_TS_TDL int,
                      TS_TDL_TrainingSession_Deleted boolean,
                      TS_TSI_TS_ID int,
                      Metadata_TS_TSI int,
                      TS_TSI_TrainingSession_SourceId varchar(100),
                      TS_TSV_TS_ID int,
                      Metadata_TS_TSV int,
                      TS_TSV_TrainingSession_Version integer,
                      TS_TTZ_TS_ID int,
                      Metadata_TS_TTZ int,
                      TS_TTZ_TrainingSession_Timezone varchar(255),
                      TS_TSY_TS_ID int,
                      Metadata_TS_TSY int,
                      TS_TSY_TrainingSession_SyncMillis bigint,
                      TS_TKC_TS_ID int,
                      Metadata_TS_TKC int,
                      TS_TKC_TrainingSession_KCal DOUBLE PRECISION,
                      TS_TPR_TS_ID int,
                      Metadata_TS_TPR int,
                      TS_TPR_TrainingSession_ProfileId varchar(100)
                  ) AS '
    SELECT
        TS.TS_ID,
        TS.Metadata_TS,
        TID.TS_TID_TS_ID,
        TID.Metadata_TS_TID,
        TID.TS_TID_TrainingSession_Id,
        TSR.TS_TSR_TS_ID,
        TSR.Metadata_TS_TSR,
        TSR.TS_TSR_TrainingSession_StartMillis,
        TST.TS_TST_TS_ID,
        TST.Metadata_TS_TST,
        TST.TS_TST_TrainingSession_StopMillis,
        TSZ.TS_TSZ_TS_ID,
        TSZ.Metadata_TS_TSZ,
        TSZ.TS_TSZ_TrainingSession_Skllzz,
        TDL.TS_TDL_TS_ID,
        TDL.Metadata_TS_TDL,
        TDL.TS_TDL_TrainingSession_Deleted,
        TSI.TS_TSI_TS_ID,
        TSI.Metadata_TS_TSI,
        TSI.TS_TSI_TrainingSession_SourceId,
        TSV.TS_TSV_TS_ID,
        TSV.Metadata_TS_TSV,
        TSV.TS_TSV_TrainingSession_Version,
        TTZ.TS_TTZ_TS_ID,
        TTZ.Metadata_TS_TTZ,
        TTZ.TS_TTZ_TrainingSession_Timezone,
        TSY.TS_TSY_TS_ID,
        TSY.Metadata_TS_TSY,
        TSY.TS_TSY_TrainingSession_SyncMillis,
        TKC.TS_TKC_TS_ID,
        TKC.Metadata_TS_TKC,
        TKC.TS_TKC_TrainingSession_KCal,
        TPR.TS_TPR_TS_ID,
        TPR.Metadata_TS_TPR,
        TPR.TS_TPR_TrainingSession_ProfileId
    FROM
        skllzz.TS_TrainingSession TS
            LEFT JOIN
        skllzz.TS_TID_TrainingSession_Id TID
        ON
                TID.TS_TID_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TSR_TrainingSession_StartMillis TSR
        ON
                TSR.TS_TSR_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TST_TrainingSession_StopMillis TST
        ON
                TST.TS_TST_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TSZ_TrainingSession_Skllzz TSZ
        ON
                TSZ.TS_TSZ_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TDL_TrainingSession_Deleted TDL
        ON
                TDL.TS_TDL_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TSI_TrainingSession_SourceId TSI
        ON
                TSI.TS_TSI_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TSV_TrainingSession_Version TSV
        ON
                TSV.TS_TSV_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TTZ_TrainingSession_Timezone TTZ
        ON
                TTZ.TS_TTZ_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TSY_TrainingSession_SyncMillis TSY
        ON
                TSY.TS_TSY_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TKC_TrainingSession_KCal TKC
        ON
                TKC.TS_TKC_TS_ID = TS.TS_ID
            LEFT JOIN
        skllzz.TS_TPR_TrainingSession_ProfileId TPR
        ON
                TPR.TS_TPR_TS_ID = TS.TS_ID;
' LANGUAGE SQL;
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pSD_StepDetails viewed as it was on the given timepoint
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION skllzz.pSD_StepDetails (
    changingTimepoint timestamp without time zone
)
    RETURNS TABLE (
                      SD_ID int,
                      Metadata_SD int,
                      SD_SDS_SD_ID int,
                      Metadata_SD_SDS int,
                      SD_SDS_StepDetails_Steps integer,
                      SD_SDD_SD_ID int,
                      Metadata_SD_SDD int,
                      SD_SDD_StepDetails_Day integer,
                      SD_SDM_SD_ID int,
                      Metadata_SD_SDM int,
                      SD_SDM_StepDetails_Meters integer
                  ) AS '
    SELECT
        SD.SD_ID,
        SD.Metadata_SD,
        SDS.SD_SDS_SD_ID,
        SDS.Metadata_SD_SDS,
        SDS.SD_SDS_StepDetails_Steps,
        SDD.SD_SDD_SD_ID,
        SDD.Metadata_SD_SDD,
        SDD.SD_SDD_StepDetails_Day,
        SDM.SD_SDM_SD_ID,
        SDM.Metadata_SD_SDM,
        SDM.SD_SDM_StepDetails_Meters
    FROM
        skllzz.SD_StepDetails SD
            LEFT JOIN
        skllzz.SD_SDS_StepDetails_Steps SDS
        ON
                SDS.SD_SDS_SD_ID = SD.SD_ID
            LEFT JOIN
        skllzz.SD_SDD_StepDetails_Day SDD
        ON
                SDD.SD_SDD_SD_ID = SD.SD_ID
            LEFT JOIN
        skllzz.SD_SDM_StepDetails_Meters SDM
        ON
                SDM.SD_SDM_SD_ID = SD.SD_ID;
' LANGUAGE SQL;
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pHD_HrDetails viewed as it was on the given timepoint
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION skllzz.pHD_HrDetails (
    changingTimepoint timestamp without time zone
)
    RETURNS TABLE (
                      HD_ID int,
                      Metadata_HD int,
                      HD_HMN_HD_ID int,
                      Metadata_HD_HMN int,
                      HD_HMN_HrDetails_MinHr integer,
                      HD_HAV_HD_ID int,
                      Metadata_HD_HAV int,
                      HD_HAV_HrDetails_AvgHr integer,
                      HD_MHR_HD_ID int,
                      Metadata_HD_MHR int,
                      HD_MHR_HrDetails_MaxHr integer,
                      HD_MNH_HD_ID int,
                      Metadata_HD_MNH int,
                      HD_MNH_HrDetails_MinHardness double precision,
                      HD_AHR_HD_ID int,
                      Metadata_HD_AHR int,
                      HD_AHR_HrDetails_AvgHardness double precision,
                      HD_MXH_HD_ID int,
                      Metadata_HD_MXH int,
                      HD_MXH_HrDetails_MaxHardness double precision,
                      HD_HRD_HD_ID int,
                      Metadata_HD_HRD int,
                      HD_HRD_HrDetails_Device varchar(100)[]
                  ) AS '
    SELECT
        HD.HD_ID,
        HD.Metadata_HD,
        HMN.HD_HMN_HD_ID,
        HMN.Metadata_HD_HMN,
        HMN.HD_HMN_HrDetails_MinHr,
        HAV.HD_HAV_HD_ID,
        HAV.Metadata_HD_HAV,
        HAV.HD_HAV_HrDetails_AvgHr,
        MHR.HD_MHR_HD_ID,
        MHR.Metadata_HD_MHR,
        MHR.HD_MHR_HrDetails_MaxHr,
        MNH.HD_MNH_HD_ID,
        MNH.Metadata_HD_MNH,
        MNH.HD_MNH_HrDetails_MinHardness,
        AHR.HD_AHR_HD_ID,
        AHR.Metadata_HD_AHR,
        AHR.HD_AHR_HrDetails_AvgHardness,
        MXH.HD_MXH_HD_ID,
        MXH.Metadata_HD_MXH,
        MXH.HD_MXH_HrDetails_MaxHardness,
        HRD.HD_HRD_HD_ID,
        HRD.Metadata_HD_HRD,
        HRD.HD_HRD_HrDetails_Device
    FROM
        skllzz.HD_HrDetails HD
            LEFT JOIN
        skllzz.HD_HMN_HrDetails_MinHr HMN
        ON
                HMN.HD_HMN_HD_ID = HD.HD_ID
            LEFT JOIN
        skllzz.HD_HAV_HrDetails_AvgHr HAV
        ON
                HAV.HD_HAV_HD_ID = HD.HD_ID
            LEFT JOIN
        skllzz.HD_MHR_HrDetails_MaxHr MHR
        ON
                MHR.HD_MHR_HD_ID = HD.HD_ID
            LEFT JOIN
        skllzz.HD_MNH_HrDetails_MinHardness MNH
        ON
                MNH.HD_MNH_HD_ID = HD.HD_ID
            LEFT JOIN
        skllzz.HD_AHR_HrDetails_AvgHardness AHR
        ON
                AHR.HD_AHR_HD_ID = HD.HD_ID
            LEFT JOIN
        skllzz.HD_MXH_HrDetails_MaxHardness MXH
        ON
                MXH.HD_MXH_HD_ID = HD.HD_ID
            LEFT JOIN
        skllzz.HD_HRD_HrDetails_Device HRD
        ON
                HRD.HD_HRD_HD_ID = HD.HD_ID;
' LANGUAGE SQL;
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nTS_TrainingSession viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.nTS_TrainingSession AS
SELECT
    *
FROM
    skllzz.pTS_TrainingSession(LOCALTIMESTAMP);
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nSD_StepDetails viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.nSD_StepDetails AS
SELECT
    *
FROM
    skllzz.pSD_StepDetails(LOCALTIMESTAMP);
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nHD_HrDetails viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.nHD_HrDetails AS
SELECT
    *
FROM
    skllzz.pHD_HrDetails(LOCALTIMESTAMP);
-- ATTRIBUTE TRIGGERS -------------------------------------------------------------------------------------------------
--
-- The following triggers on the attributes make them behave like tables.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TID_TrainingSession_Id ON skllzz.TS_TID_TrainingSession_Id;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TID_TrainingSession_Id();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TID_TrainingSession_Id() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TID_TrainingSession_Id (
                                                                                TS_TID_TS_ID int not null,
                                                                                Metadata_TS_TID int not null,
                                                                                TS_TID_TrainingSession_Id varchar(100) not null,
                                                                                TS_TID_Version bigint not null,
                                                                                TS_TID_StatementType char(1) not null,
                                                                                primary key(
                                                                                            TS_TID_Version,
                                                                                            TS_TID_TS_ID
                                                                                    )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TID_TrainingSession_Id
    BEFORE INSERT ON skllzz.TS_TID_TrainingSession_Id
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TID_TrainingSession_Id();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TID_TrainingSession_Id ON skllzz.TS_TID_TrainingSession_Id;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TID_TrainingSession_Id();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TID_TrainingSession_Id() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TID_TrainingSession_Id
        SELECT
            NEW.TS_TID_TS_ID,
            NEW.Metadata_TS_TID,
            NEW.TS_TID_TrainingSession_Id,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TID_TrainingSession_Id
    INSTEAD OF INSERT ON skllzz.TS_TID_TrainingSession_Id
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TID_TrainingSession_Id();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TID_TrainingSession_Id ON skllzz.TS_TID_TrainingSession_Id;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TID_TrainingSession_Id();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TID_TrainingSession_Id() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TID_Version) INTO maxVersion
        FROM
            _tmp_TS_TID_TrainingSession_Id;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TID_TrainingSession_Id
            SET
                TS_TID_StatementType =
                    CASE
                        WHEN TID.TS_TID_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TID_TrainingSession_Id v
                    LEFT JOIN
                skllzz._TS_TID_TrainingSession_Id TID
                ON
                            TID.TS_TID_TS_ID = v.TS_TID_TS_ID
                        AND
                            TID.TS_TID_TrainingSession_Id = v.TS_TID_TrainingSession_Id
            WHERE
                    v.TS_TID_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TID_TrainingSession_Id (
                TS_TID_TS_ID,
                Metadata_TS_TID,
                TS_TID_TrainingSession_Id
            )
            SELECT
                TS_TID_TS_ID,
                Metadata_TS_TID,
                TS_TID_TrainingSession_Id
            FROM
                _tmp_TS_TID_TrainingSession_Id
            WHERE
                    TS_TID_Version = currentVersion
              AND
                    TS_TID_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TID_TrainingSession_Id;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TID_TrainingSession_Id
    AFTER INSERT ON skllzz.TS_TID_TrainingSession_Id
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TID_TrainingSession_Id();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TSR_TrainingSession_StartMillis ON skllzz.TS_TSR_TrainingSession_StartMillis;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TSR_TrainingSession_StartMillis();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TSR_TrainingSession_StartMillis() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TSR_TrainingSession_StartMillis (
                                                                                         TS_TSR_TS_ID int not null,
                                                                                         Metadata_TS_TSR int not null,
                                                                                         TS_TSR_TrainingSession_StartMillis bigint not null,
                                                                                         TS_TSR_Version bigint not null,
                                                                                         TS_TSR_StatementType char(1) not null,
                                                                                         primary key(
                                                                                                     TS_TSR_Version,
                                                                                                     TS_TSR_TS_ID
                                                                                             )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TSR_TrainingSession_StartMillis
    BEFORE INSERT ON skllzz.TS_TSR_TrainingSession_StartMillis
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TSR_TrainingSession_StartMillis();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TSR_TrainingSession_StartMillis ON skllzz.TS_TSR_TrainingSession_StartMillis;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TSR_TrainingSession_StartMillis();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TSR_TrainingSession_StartMillis() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TSR_TrainingSession_StartMillis
        SELECT
            NEW.TS_TSR_TS_ID,
            NEW.Metadata_TS_TSR,
            NEW.TS_TSR_TrainingSession_StartMillis,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TSR_TrainingSession_StartMillis
    INSTEAD OF INSERT ON skllzz.TS_TSR_TrainingSession_StartMillis
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TSR_TrainingSession_StartMillis();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TSR_TrainingSession_StartMillis ON skllzz.TS_TSR_TrainingSession_StartMillis;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TSR_TrainingSession_StartMillis();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TSR_TrainingSession_StartMillis() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TSR_Version) INTO maxVersion
        FROM
            _tmp_TS_TSR_TrainingSession_StartMillis;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TSR_TrainingSession_StartMillis
            SET
                TS_TSR_StatementType =
                    CASE
                        WHEN TSR.TS_TSR_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TSR_TrainingSession_StartMillis v
                    LEFT JOIN
                skllzz._TS_TSR_TrainingSession_StartMillis TSR
                ON
                            TSR.TS_TSR_TS_ID = v.TS_TSR_TS_ID
                        AND
                            TSR.TS_TSR_TrainingSession_StartMillis = v.TS_TSR_TrainingSession_StartMillis
            WHERE
                    v.TS_TSR_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TSR_TrainingSession_StartMillis (
                TS_TSR_TS_ID,
                Metadata_TS_TSR,
                TS_TSR_TrainingSession_StartMillis
            )
            SELECT
                TS_TSR_TS_ID,
                Metadata_TS_TSR,
                TS_TSR_TrainingSession_StartMillis
            FROM
                _tmp_TS_TSR_TrainingSession_StartMillis
            WHERE
                    TS_TSR_Version = currentVersion
              AND
                    TS_TSR_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TSR_TrainingSession_StartMillis;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TSR_TrainingSession_StartMillis
    AFTER INSERT ON skllzz.TS_TSR_TrainingSession_StartMillis
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TSR_TrainingSession_StartMillis();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TST_TrainingSession_StopMillis ON skllzz.TS_TST_TrainingSession_StopMillis;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TST_TrainingSession_StopMillis();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TST_TrainingSession_StopMillis() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TST_TrainingSession_StopMillis (
                                                                                        TS_TST_TS_ID int not null,
                                                                                        Metadata_TS_TST int not null,
                                                                                        TS_TST_TrainingSession_StopMillis bigint not null,
                                                                                        TS_TST_Version bigint not null,
                                                                                        TS_TST_StatementType char(1) not null,
                                                                                        primary key(
                                                                                                    TS_TST_Version,
                                                                                                    TS_TST_TS_ID
                                                                                            )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TST_TrainingSession_StopMillis
    BEFORE INSERT ON skllzz.TS_TST_TrainingSession_StopMillis
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TST_TrainingSession_StopMillis();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TST_TrainingSession_StopMillis ON skllzz.TS_TST_TrainingSession_StopMillis;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TST_TrainingSession_StopMillis();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TST_TrainingSession_StopMillis() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TST_TrainingSession_StopMillis
        SELECT
            NEW.TS_TST_TS_ID,
            NEW.Metadata_TS_TST,
            NEW.TS_TST_TrainingSession_StopMillis,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TST_TrainingSession_StopMillis
    INSTEAD OF INSERT ON skllzz.TS_TST_TrainingSession_StopMillis
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TST_TrainingSession_StopMillis();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TST_TrainingSession_StopMillis ON skllzz.TS_TST_TrainingSession_StopMillis;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TST_TrainingSession_StopMillis();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TST_TrainingSession_StopMillis() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TST_Version) INTO maxVersion
        FROM
            _tmp_TS_TST_TrainingSession_StopMillis;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TST_TrainingSession_StopMillis
            SET
                TS_TST_StatementType =
                    CASE
                        WHEN TST.TS_TST_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TST_TrainingSession_StopMillis v
                    LEFT JOIN
                skllzz._TS_TST_TrainingSession_StopMillis TST
                ON
                            TST.TS_TST_TS_ID = v.TS_TST_TS_ID
                        AND
                            TST.TS_TST_TrainingSession_StopMillis = v.TS_TST_TrainingSession_StopMillis
            WHERE
                    v.TS_TST_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TST_TrainingSession_StopMillis (
                TS_TST_TS_ID,
                Metadata_TS_TST,
                TS_TST_TrainingSession_StopMillis
            )
            SELECT
                TS_TST_TS_ID,
                Metadata_TS_TST,
                TS_TST_TrainingSession_StopMillis
            FROM
                _tmp_TS_TST_TrainingSession_StopMillis
            WHERE
                    TS_TST_Version = currentVersion
              AND
                    TS_TST_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TST_TrainingSession_StopMillis;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TST_TrainingSession_StopMillis
    AFTER INSERT ON skllzz.TS_TST_TrainingSession_StopMillis
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TST_TrainingSession_StopMillis();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TSZ_TrainingSession_Skllzz ON skllzz.TS_TSZ_TrainingSession_Skllzz;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TSZ_TrainingSession_Skllzz();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TSZ_TrainingSession_Skllzz() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TSZ_TrainingSession_Skllzz (
                                                                                    TS_TSZ_TS_ID int not null,
                                                                                    Metadata_TS_TSZ int not null,
                                                                                    TS_TSZ_TrainingSession_Skllzz DOUBLE PRECISION not null,
                                                                                    TS_TSZ_Version bigint not null,
                                                                                    TS_TSZ_StatementType char(1) not null,
                                                                                    primary key(
                                                                                                TS_TSZ_Version,
                                                                                                TS_TSZ_TS_ID
                                                                                        )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TSZ_TrainingSession_Skllzz
    BEFORE INSERT ON skllzz.TS_TSZ_TrainingSession_Skllzz
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TSZ_TrainingSession_Skllzz();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TSZ_TrainingSession_Skllzz ON skllzz.TS_TSZ_TrainingSession_Skllzz;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TSZ_TrainingSession_Skllzz();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TSZ_TrainingSession_Skllzz() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TSZ_TrainingSession_Skllzz
        SELECT
            NEW.TS_TSZ_TS_ID,
            NEW.Metadata_TS_TSZ,
            NEW.TS_TSZ_TrainingSession_Skllzz,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TSZ_TrainingSession_Skllzz
    INSTEAD OF INSERT ON skllzz.TS_TSZ_TrainingSession_Skllzz
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TSZ_TrainingSession_Skllzz();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TSZ_TrainingSession_Skllzz ON skllzz.TS_TSZ_TrainingSession_Skllzz;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TSZ_TrainingSession_Skllzz();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TSZ_TrainingSession_Skllzz() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TSZ_Version) INTO maxVersion
        FROM
            _tmp_TS_TSZ_TrainingSession_Skllzz;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TSZ_TrainingSession_Skllzz
            SET
                TS_TSZ_StatementType =
                    CASE
                        WHEN TSZ.TS_TSZ_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TSZ_TrainingSession_Skllzz v
                    LEFT JOIN
                skllzz._TS_TSZ_TrainingSession_Skllzz TSZ
                ON
                            TSZ.TS_TSZ_TS_ID = v.TS_TSZ_TS_ID
                        AND
                            TSZ.TS_TSZ_TrainingSession_Skllzz = v.TS_TSZ_TrainingSession_Skllzz
            WHERE
                    v.TS_TSZ_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TSZ_TrainingSession_Skllzz (
                TS_TSZ_TS_ID,
                Metadata_TS_TSZ,
                TS_TSZ_TrainingSession_Skllzz
            )
            SELECT
                TS_TSZ_TS_ID,
                Metadata_TS_TSZ,
                TS_TSZ_TrainingSession_Skllzz
            FROM
                _tmp_TS_TSZ_TrainingSession_Skllzz
            WHERE
                    TS_TSZ_Version = currentVersion
              AND
                    TS_TSZ_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TSZ_TrainingSession_Skllzz;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TSZ_TrainingSession_Skllzz
    AFTER INSERT ON skllzz.TS_TSZ_TrainingSession_Skllzz
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TSZ_TrainingSession_Skllzz();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TDL_TrainingSession_Deleted ON skllzz.TS_TDL_TrainingSession_Deleted;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TDL_TrainingSession_Deleted();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TDL_TrainingSession_Deleted() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TDL_TrainingSession_Deleted (
                                                                                     TS_TDL_TS_ID int not null,
                                                                                     Metadata_TS_TDL int not null,
                                                                                     TS_TDL_TrainingSession_Deleted boolean not null,
                                                                                     TS_TDL_Version bigint not null,
                                                                                     TS_TDL_StatementType char(1) not null,
                                                                                     primary key(
                                                                                                 TS_TDL_Version,
                                                                                                 TS_TDL_TS_ID
                                                                                         )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TDL_TrainingSession_Deleted
    BEFORE INSERT ON skllzz.TS_TDL_TrainingSession_Deleted
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TDL_TrainingSession_Deleted();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TDL_TrainingSession_Deleted ON skllzz.TS_TDL_TrainingSession_Deleted;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TDL_TrainingSession_Deleted();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TDL_TrainingSession_Deleted() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TDL_TrainingSession_Deleted
        SELECT
            NEW.TS_TDL_TS_ID,
            NEW.Metadata_TS_TDL,
            NEW.TS_TDL_TrainingSession_Deleted,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TDL_TrainingSession_Deleted
    INSTEAD OF INSERT ON skllzz.TS_TDL_TrainingSession_Deleted
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TDL_TrainingSession_Deleted();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TDL_TrainingSession_Deleted ON skllzz.TS_TDL_TrainingSession_Deleted;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TDL_TrainingSession_Deleted();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TDL_TrainingSession_Deleted() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TDL_Version) INTO maxVersion
        FROM
            _tmp_TS_TDL_TrainingSession_Deleted;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TDL_TrainingSession_Deleted
            SET
                TS_TDL_StatementType =
                    CASE
                        WHEN TDL.TS_TDL_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TDL_TrainingSession_Deleted v
                    LEFT JOIN
                skllzz._TS_TDL_TrainingSession_Deleted TDL
                ON
                            TDL.TS_TDL_TS_ID = v.TS_TDL_TS_ID
                        AND
                            TDL.TS_TDL_TrainingSession_Deleted = v.TS_TDL_TrainingSession_Deleted
            WHERE
                    v.TS_TDL_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TDL_TrainingSession_Deleted (
                TS_TDL_TS_ID,
                Metadata_TS_TDL,
                TS_TDL_TrainingSession_Deleted
            )
            SELECT
                TS_TDL_TS_ID,
                Metadata_TS_TDL,
                TS_TDL_TrainingSession_Deleted
            FROM
                _tmp_TS_TDL_TrainingSession_Deleted
            WHERE
                    TS_TDL_Version = currentVersion
              AND
                    TS_TDL_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TDL_TrainingSession_Deleted;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TDL_TrainingSession_Deleted
    AFTER INSERT ON skllzz.TS_TDL_TrainingSession_Deleted
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TDL_TrainingSession_Deleted();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TSI_TrainingSession_SourceId ON skllzz.TS_TSI_TrainingSession_SourceId;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TSI_TrainingSession_SourceId();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TSI_TrainingSession_SourceId() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TSI_TrainingSession_SourceId (
                                                                                      TS_TSI_TS_ID int not null,
                                                                                      Metadata_TS_TSI int not null,
                                                                                      TS_TSI_TrainingSession_SourceId varchar(100) not null,
                                                                                      TS_TSI_Version bigint not null,
                                                                                      TS_TSI_StatementType char(1) not null,
                                                                                      primary key(
                                                                                                  TS_TSI_Version,
                                                                                                  TS_TSI_TS_ID
                                                                                          )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TSI_TrainingSession_SourceId
    BEFORE INSERT ON skllzz.TS_TSI_TrainingSession_SourceId
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TSI_TrainingSession_SourceId();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TSI_TrainingSession_SourceId ON skllzz.TS_TSI_TrainingSession_SourceId;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TSI_TrainingSession_SourceId();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TSI_TrainingSession_SourceId() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TSI_TrainingSession_SourceId
        SELECT
            NEW.TS_TSI_TS_ID,
            NEW.Metadata_TS_TSI,
            NEW.TS_TSI_TrainingSession_SourceId,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TSI_TrainingSession_SourceId
    INSTEAD OF INSERT ON skllzz.TS_TSI_TrainingSession_SourceId
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TSI_TrainingSession_SourceId();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TSI_TrainingSession_SourceId ON skllzz.TS_TSI_TrainingSession_SourceId;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TSI_TrainingSession_SourceId();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TSI_TrainingSession_SourceId() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TSI_Version) INTO maxVersion
        FROM
            _tmp_TS_TSI_TrainingSession_SourceId;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TSI_TrainingSession_SourceId
            SET
                TS_TSI_StatementType =
                    CASE
                        WHEN TSI.TS_TSI_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TSI_TrainingSession_SourceId v
                    LEFT JOIN
                skllzz._TS_TSI_TrainingSession_SourceId TSI
                ON
                            TSI.TS_TSI_TS_ID = v.TS_TSI_TS_ID
                        AND
                            TSI.TS_TSI_TrainingSession_SourceId = v.TS_TSI_TrainingSession_SourceId
            WHERE
                    v.TS_TSI_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TSI_TrainingSession_SourceId (
                TS_TSI_TS_ID,
                Metadata_TS_TSI,
                TS_TSI_TrainingSession_SourceId
            )
            SELECT
                TS_TSI_TS_ID,
                Metadata_TS_TSI,
                TS_TSI_TrainingSession_SourceId
            FROM
                _tmp_TS_TSI_TrainingSession_SourceId
            WHERE
                    TS_TSI_Version = currentVersion
              AND
                    TS_TSI_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TSI_TrainingSession_SourceId;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TSI_TrainingSession_SourceId
    AFTER INSERT ON skllzz.TS_TSI_TrainingSession_SourceId
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TSI_TrainingSession_SourceId();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TSV_TrainingSession_Version ON skllzz.TS_TSV_TrainingSession_Version;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TSV_TrainingSession_Version();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TSV_TrainingSession_Version() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TSV_TrainingSession_Version (
                                                                                     TS_TSV_TS_ID int not null,
                                                                                     Metadata_TS_TSV int not null,
                                                                                     TS_TSV_TrainingSession_Version integer not null,
                                                                                     TS_TSV_Version bigint not null,
                                                                                     TS_TSV_StatementType char(1) not null,
                                                                                     primary key(
                                                                                                 TS_TSV_Version,
                                                                                                 TS_TSV_TS_ID
                                                                                         )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TSV_TrainingSession_Version
    BEFORE INSERT ON skllzz.TS_TSV_TrainingSession_Version
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TSV_TrainingSession_Version();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TSV_TrainingSession_Version ON skllzz.TS_TSV_TrainingSession_Version;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TSV_TrainingSession_Version();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TSV_TrainingSession_Version() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TSV_TrainingSession_Version
        SELECT
            NEW.TS_TSV_TS_ID,
            NEW.Metadata_TS_TSV,
            NEW.TS_TSV_TrainingSession_Version,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TSV_TrainingSession_Version
    INSTEAD OF INSERT ON skllzz.TS_TSV_TrainingSession_Version
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TSV_TrainingSession_Version();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TSV_TrainingSession_Version ON skllzz.TS_TSV_TrainingSession_Version;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TSV_TrainingSession_Version();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TSV_TrainingSession_Version() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TSV_Version) INTO maxVersion
        FROM
            _tmp_TS_TSV_TrainingSession_Version;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TSV_TrainingSession_Version
            SET
                TS_TSV_StatementType =
                    CASE
                        WHEN TSV.TS_TSV_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TSV_TrainingSession_Version v
                    LEFT JOIN
                skllzz._TS_TSV_TrainingSession_Version TSV
                ON
                            TSV.TS_TSV_TS_ID = v.TS_TSV_TS_ID
                        AND
                            TSV.TS_TSV_TrainingSession_Version = v.TS_TSV_TrainingSession_Version
            WHERE
                    v.TS_TSV_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TSV_TrainingSession_Version (
                TS_TSV_TS_ID,
                Metadata_TS_TSV,
                TS_TSV_TrainingSession_Version
            )
            SELECT
                TS_TSV_TS_ID,
                Metadata_TS_TSV,
                TS_TSV_TrainingSession_Version
            FROM
                _tmp_TS_TSV_TrainingSession_Version
            WHERE
                    TS_TSV_Version = currentVersion
              AND
                    TS_TSV_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TSV_TrainingSession_Version;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TSV_TrainingSession_Version
    AFTER INSERT ON skllzz.TS_TSV_TrainingSession_Version
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TSV_TrainingSession_Version();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TTZ_TrainingSession_Timezone ON skllzz.TS_TTZ_TrainingSession_Timezone;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TTZ_TrainingSession_Timezone();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TTZ_TrainingSession_Timezone() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TTZ_TrainingSession_Timezone (
                                                                                      TS_TTZ_TS_ID int not null,
                                                                                      Metadata_TS_TTZ int not null,
                                                                                      TS_TTZ_TrainingSession_Timezone varchar(255) not null,
                                                                                      TS_TTZ_Version bigint not null,
                                                                                      TS_TTZ_StatementType char(1) not null,
                                                                                      primary key(
                                                                                                  TS_TTZ_Version,
                                                                                                  TS_TTZ_TS_ID
                                                                                          )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TTZ_TrainingSession_Timezone
    BEFORE INSERT ON skllzz.TS_TTZ_TrainingSession_Timezone
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TTZ_TrainingSession_Timezone();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TTZ_TrainingSession_Timezone ON skllzz.TS_TTZ_TrainingSession_Timezone;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TTZ_TrainingSession_Timezone();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TTZ_TrainingSession_Timezone() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TTZ_TrainingSession_Timezone
        SELECT
            NEW.TS_TTZ_TS_ID,
            NEW.Metadata_TS_TTZ,
            NEW.TS_TTZ_TrainingSession_Timezone,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TTZ_TrainingSession_Timezone
    INSTEAD OF INSERT ON skllzz.TS_TTZ_TrainingSession_Timezone
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TTZ_TrainingSession_Timezone();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TTZ_TrainingSession_Timezone ON skllzz.TS_TTZ_TrainingSession_Timezone;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TTZ_TrainingSession_Timezone();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TTZ_TrainingSession_Timezone() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TTZ_Version) INTO maxVersion
        FROM
            _tmp_TS_TTZ_TrainingSession_Timezone;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TTZ_TrainingSession_Timezone
            SET
                TS_TTZ_StatementType =
                    CASE
                        WHEN TTZ.TS_TTZ_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TTZ_TrainingSession_Timezone v
                    LEFT JOIN
                skllzz._TS_TTZ_TrainingSession_Timezone TTZ
                ON
                            TTZ.TS_TTZ_TS_ID = v.TS_TTZ_TS_ID
                        AND
                            TTZ.TS_TTZ_TrainingSession_Timezone = v.TS_TTZ_TrainingSession_Timezone
            WHERE
                    v.TS_TTZ_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TTZ_TrainingSession_Timezone (
                TS_TTZ_TS_ID,
                Metadata_TS_TTZ,
                TS_TTZ_TrainingSession_Timezone
            )
            SELECT
                TS_TTZ_TS_ID,
                Metadata_TS_TTZ,
                TS_TTZ_TrainingSession_Timezone
            FROM
                _tmp_TS_TTZ_TrainingSession_Timezone
            WHERE
                    TS_TTZ_Version = currentVersion
              AND
                    TS_TTZ_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TTZ_TrainingSession_Timezone;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TTZ_TrainingSession_Timezone
    AFTER INSERT ON skllzz.TS_TTZ_TrainingSession_Timezone
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TTZ_TrainingSession_Timezone();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TSY_TrainingSession_SyncMillis ON skllzz.TS_TSY_TrainingSession_SyncMillis;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TSY_TrainingSession_SyncMillis();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TSY_TrainingSession_SyncMillis() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TSY_TrainingSession_SyncMillis (
                                                                                        TS_TSY_TS_ID int not null,
                                                                                        Metadata_TS_TSY int not null,
                                                                                        TS_TSY_TrainingSession_SyncMillis bigint not null,
                                                                                        TS_TSY_Version bigint not null,
                                                                                        TS_TSY_StatementType char(1) not null,
                                                                                        primary key(
                                                                                                    TS_TSY_Version,
                                                                                                    TS_TSY_TS_ID
                                                                                            )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TSY_TrainingSession_SyncMillis
    BEFORE INSERT ON skllzz.TS_TSY_TrainingSession_SyncMillis
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TSY_TrainingSession_SyncMillis();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TSY_TrainingSession_SyncMillis ON skllzz.TS_TSY_TrainingSession_SyncMillis;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TSY_TrainingSession_SyncMillis();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TSY_TrainingSession_SyncMillis() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TSY_TrainingSession_SyncMillis
        SELECT
            NEW.TS_TSY_TS_ID,
            NEW.Metadata_TS_TSY,
            NEW.TS_TSY_TrainingSession_SyncMillis,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TSY_TrainingSession_SyncMillis
    INSTEAD OF INSERT ON skllzz.TS_TSY_TrainingSession_SyncMillis
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TSY_TrainingSession_SyncMillis();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TSY_TrainingSession_SyncMillis ON skllzz.TS_TSY_TrainingSession_SyncMillis;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TSY_TrainingSession_SyncMillis();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TSY_TrainingSession_SyncMillis() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TSY_Version) INTO maxVersion
        FROM
            _tmp_TS_TSY_TrainingSession_SyncMillis;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TSY_TrainingSession_SyncMillis
            SET
                TS_TSY_StatementType =
                    CASE
                        WHEN TSY.TS_TSY_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TSY_TrainingSession_SyncMillis v
                    LEFT JOIN
                skllzz._TS_TSY_TrainingSession_SyncMillis TSY
                ON
                            TSY.TS_TSY_TS_ID = v.TS_TSY_TS_ID
                        AND
                            TSY.TS_TSY_TrainingSession_SyncMillis = v.TS_TSY_TrainingSession_SyncMillis
            WHERE
                    v.TS_TSY_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TSY_TrainingSession_SyncMillis (
                TS_TSY_TS_ID,
                Metadata_TS_TSY,
                TS_TSY_TrainingSession_SyncMillis
            )
            SELECT
                TS_TSY_TS_ID,
                Metadata_TS_TSY,
                TS_TSY_TrainingSession_SyncMillis
            FROM
                _tmp_TS_TSY_TrainingSession_SyncMillis
            WHERE
                    TS_TSY_Version = currentVersion
              AND
                    TS_TSY_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TSY_TrainingSession_SyncMillis;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TSY_TrainingSession_SyncMillis
    AFTER INSERT ON skllzz.TS_TSY_TrainingSession_SyncMillis
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TSY_TrainingSession_SyncMillis();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TKC_TrainingSession_KCal ON skllzz.TS_TKC_TrainingSession_KCal;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TKC_TrainingSession_KCal();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TKC_TrainingSession_KCal() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TKC_TrainingSession_KCal (
                                                                                  TS_TKC_TS_ID int not null,
                                                                                  Metadata_TS_TKC int not null,
                                                                                  TS_TKC_TrainingSession_KCal DOUBLE PRECISION not null,
                                                                                  TS_TKC_Version bigint not null,
                                                                                  TS_TKC_StatementType char(1) not null,
                                                                                  primary key(
                                                                                              TS_TKC_Version,
                                                                                              TS_TKC_TS_ID
                                                                                      )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TKC_TrainingSession_KCal
    BEFORE INSERT ON skllzz.TS_TKC_TrainingSession_KCal
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TKC_TrainingSession_KCal();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TKC_TrainingSession_KCal ON skllzz.TS_TKC_TrainingSession_KCal;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TKC_TrainingSession_KCal();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TKC_TrainingSession_KCal() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TKC_TrainingSession_KCal
        SELECT
            NEW.TS_TKC_TS_ID,
            NEW.Metadata_TS_TKC,
            NEW.TS_TKC_TrainingSession_KCal,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TKC_TrainingSession_KCal
    INSTEAD OF INSERT ON skllzz.TS_TKC_TrainingSession_KCal
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TKC_TrainingSession_KCal();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TKC_TrainingSession_KCal ON skllzz.TS_TKC_TrainingSession_KCal;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TKC_TrainingSession_KCal();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TKC_TrainingSession_KCal() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TKC_Version) INTO maxVersion
        FROM
            _tmp_TS_TKC_TrainingSession_KCal;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TKC_TrainingSession_KCal
            SET
                TS_TKC_StatementType =
                    CASE
                        WHEN TKC.TS_TKC_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TKC_TrainingSession_KCal v
                    LEFT JOIN
                skllzz._TS_TKC_TrainingSession_KCal TKC
                ON
                            TKC.TS_TKC_TS_ID = v.TS_TKC_TS_ID
                        AND
                            TKC.TS_TKC_TrainingSession_KCal = v.TS_TKC_TrainingSession_KCal
            WHERE
                    v.TS_TKC_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TKC_TrainingSession_KCal (
                TS_TKC_TS_ID,
                Metadata_TS_TKC,
                TS_TKC_TrainingSession_KCal
            )
            SELECT
                TS_TKC_TS_ID,
                Metadata_TS_TKC,
                TS_TKC_TrainingSession_KCal
            FROM
                _tmp_TS_TKC_TrainingSession_KCal
            WHERE
                    TS_TKC_Version = currentVersion
              AND
                    TS_TKC_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TKC_TrainingSession_KCal;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TKC_TrainingSession_KCal
    AFTER INSERT ON skllzz.TS_TKC_TrainingSession_KCal
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TKC_TrainingSession_KCal();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbTS_TPR_TrainingSession_ProfileId ON skllzz.TS_TPR_TrainingSession_ProfileId;
-- DROP FUNCTION IF EXISTS skllzz.tcbTS_TPR_TrainingSession_ProfileId();
CREATE OR REPLACE FUNCTION skllzz.tcbTS_TPR_TrainingSession_ProfileId() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_TS_TPR_TrainingSession_ProfileId (
                                                                                       TS_TPR_TS_ID int not null,
                                                                                       Metadata_TS_TPR int not null,
                                                                                       TS_TPR_TrainingSession_ProfileId varchar(100) not null,
                                                                                       TS_TPR_Version bigint not null,
                                                                                       TS_TPR_StatementType char(1) not null,
                                                                                       primary key(
                                                                                                   TS_TPR_Version,
                                                                                                   TS_TPR_TS_ID
                                                                                           )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbTS_TPR_TrainingSession_ProfileId
    BEFORE INSERT ON skllzz.TS_TPR_TrainingSession_ProfileId
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbTS_TPR_TrainingSession_ProfileId();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciTS_TPR_TrainingSession_ProfileId ON skllzz.TS_TPR_TrainingSession_ProfileId;
-- DROP FUNCTION IF EXISTS skllzz.tciTS_TPR_TrainingSession_ProfileId();
CREATE OR REPLACE FUNCTION skllzz.tciTS_TPR_TrainingSession_ProfileId() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_TS_TPR_TrainingSession_ProfileId
        SELECT
            NEW.TS_TPR_TS_ID,
            NEW.Metadata_TS_TPR,
            NEW.TS_TPR_TrainingSession_ProfileId,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciTS_TPR_TrainingSession_ProfileId
    INSTEAD OF INSERT ON skllzz.TS_TPR_TrainingSession_ProfileId
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciTS_TPR_TrainingSession_ProfileId();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaTS_TPR_TrainingSession_ProfileId ON skllzz.TS_TPR_TrainingSession_ProfileId;
-- DROP FUNCTION IF EXISTS skllzz.tcaTS_TPR_TrainingSession_ProfileId();
CREATE OR REPLACE FUNCTION skllzz.tcaTS_TPR_TrainingSession_ProfileId() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(TS_TPR_Version) INTO maxVersion
        FROM
            _tmp_TS_TPR_TrainingSession_ProfileId;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_TS_TPR_TrainingSession_ProfileId
            SET
                TS_TPR_StatementType =
                    CASE
                        WHEN TPR.TS_TPR_TS_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_TS_TPR_TrainingSession_ProfileId v
                    LEFT JOIN
                skllzz._TS_TPR_TrainingSession_ProfileId TPR
                ON
                            TPR.TS_TPR_TS_ID = v.TS_TPR_TS_ID
                        AND
                            TPR.TS_TPR_TrainingSession_ProfileId = v.TS_TPR_TrainingSession_ProfileId
            WHERE
                    v.TS_TPR_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._TS_TPR_TrainingSession_ProfileId (
                TS_TPR_TS_ID,
                Metadata_TS_TPR,
                TS_TPR_TrainingSession_ProfileId
            )
            SELECT
                TS_TPR_TS_ID,
                Metadata_TS_TPR,
                TS_TPR_TrainingSession_ProfileId
            FROM
                _tmp_TS_TPR_TrainingSession_ProfileId
            WHERE
                    TS_TPR_Version = currentVersion
              AND
                    TS_TPR_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_TS_TPR_TrainingSession_ProfileId;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaTS_TPR_TrainingSession_ProfileId
    AFTER INSERT ON skllzz.TS_TPR_TrainingSession_ProfileId
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaTS_TPR_TrainingSession_ProfileId();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbSD_SDS_StepDetails_Steps ON skllzz.SD_SDS_StepDetails_Steps;
-- DROP FUNCTION IF EXISTS skllzz.tcbSD_SDS_StepDetails_Steps();
CREATE OR REPLACE FUNCTION skllzz.tcbSD_SDS_StepDetails_Steps() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_SD_SDS_StepDetails_Steps (
                                                                               SD_SDS_SD_ID int not null,
                                                                               Metadata_SD_SDS int not null,
                                                                               SD_SDS_StepDetails_Steps integer not null,
                                                                               SD_SDS_Version bigint not null,
                                                                               SD_SDS_StatementType char(1) not null,
                                                                               primary key(
                                                                                           SD_SDS_Version,
                                                                                           SD_SDS_SD_ID
                                                                                   )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbSD_SDS_StepDetails_Steps
    BEFORE INSERT ON skllzz.SD_SDS_StepDetails_Steps
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbSD_SDS_StepDetails_Steps();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciSD_SDS_StepDetails_Steps ON skllzz.SD_SDS_StepDetails_Steps;
-- DROP FUNCTION IF EXISTS skllzz.tciSD_SDS_StepDetails_Steps();
CREATE OR REPLACE FUNCTION skllzz.tciSD_SDS_StepDetails_Steps() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_SD_SDS_StepDetails_Steps
        SELECT
            NEW.SD_SDS_SD_ID,
            NEW.Metadata_SD_SDS,
            NEW.SD_SDS_StepDetails_Steps,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciSD_SDS_StepDetails_Steps
    INSTEAD OF INSERT ON skllzz.SD_SDS_StepDetails_Steps
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciSD_SDS_StepDetails_Steps();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaSD_SDS_StepDetails_Steps ON skllzz.SD_SDS_StepDetails_Steps;
-- DROP FUNCTION IF EXISTS skllzz.tcaSD_SDS_StepDetails_Steps();
CREATE OR REPLACE FUNCTION skllzz.tcaSD_SDS_StepDetails_Steps() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(SD_SDS_Version) INTO maxVersion
        FROM
            _tmp_SD_SDS_StepDetails_Steps;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_SD_SDS_StepDetails_Steps
            SET
                SD_SDS_StatementType =
                    CASE
                        WHEN SDS.SD_SDS_SD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_SD_SDS_StepDetails_Steps v
                    LEFT JOIN
                skllzz._SD_SDS_StepDetails_Steps SDS
                ON
                            SDS.SD_SDS_SD_ID = v.SD_SDS_SD_ID
                        AND
                            SDS.SD_SDS_StepDetails_Steps = v.SD_SDS_StepDetails_Steps
            WHERE
                    v.SD_SDS_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._SD_SDS_StepDetails_Steps (
                SD_SDS_SD_ID,
                Metadata_SD_SDS,
                SD_SDS_StepDetails_Steps
            )
            SELECT
                SD_SDS_SD_ID,
                Metadata_SD_SDS,
                SD_SDS_StepDetails_Steps
            FROM
                _tmp_SD_SDS_StepDetails_Steps
            WHERE
                    SD_SDS_Version = currentVersion
              AND
                    SD_SDS_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_SD_SDS_StepDetails_Steps;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaSD_SDS_StepDetails_Steps
    AFTER INSERT ON skllzz.SD_SDS_StepDetails_Steps
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaSD_SDS_StepDetails_Steps();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbSD_SDD_StepDetails_Day ON skllzz.SD_SDD_StepDetails_Day;
-- DROP FUNCTION IF EXISTS skllzz.tcbSD_SDD_StepDetails_Day();
CREATE OR REPLACE FUNCTION skllzz.tcbSD_SDD_StepDetails_Day() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_SD_SDD_StepDetails_Day (
                                                                             SD_SDD_SD_ID int not null,
                                                                             Metadata_SD_SDD int not null,
                                                                             SD_SDD_StepDetails_Day integer not null,
                                                                             SD_SDD_Version bigint not null,
                                                                             SD_SDD_StatementType char(1) not null,
                                                                             primary key(
                                                                                         SD_SDD_Version,
                                                                                         SD_SDD_SD_ID
                                                                                 )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbSD_SDD_StepDetails_Day
    BEFORE INSERT ON skllzz.SD_SDD_StepDetails_Day
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbSD_SDD_StepDetails_Day();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciSD_SDD_StepDetails_Day ON skllzz.SD_SDD_StepDetails_Day;
-- DROP FUNCTION IF EXISTS skllzz.tciSD_SDD_StepDetails_Day();
CREATE OR REPLACE FUNCTION skllzz.tciSD_SDD_StepDetails_Day() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_SD_SDD_StepDetails_Day
        SELECT
            NEW.SD_SDD_SD_ID,
            NEW.Metadata_SD_SDD,
            NEW.SD_SDD_StepDetails_Day,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciSD_SDD_StepDetails_Day
    INSTEAD OF INSERT ON skllzz.SD_SDD_StepDetails_Day
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciSD_SDD_StepDetails_Day();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaSD_SDD_StepDetails_Day ON skllzz.SD_SDD_StepDetails_Day;
-- DROP FUNCTION IF EXISTS skllzz.tcaSD_SDD_StepDetails_Day();
CREATE OR REPLACE FUNCTION skllzz.tcaSD_SDD_StepDetails_Day() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(SD_SDD_Version) INTO maxVersion
        FROM
            _tmp_SD_SDD_StepDetails_Day;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_SD_SDD_StepDetails_Day
            SET
                SD_SDD_StatementType =
                    CASE
                        WHEN SDD.SD_SDD_SD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_SD_SDD_StepDetails_Day v
                    LEFT JOIN
                skllzz._SD_SDD_StepDetails_Day SDD
                ON
                            SDD.SD_SDD_SD_ID = v.SD_SDD_SD_ID
                        AND
                            SDD.SD_SDD_StepDetails_Day = v.SD_SDD_StepDetails_Day
            WHERE
                    v.SD_SDD_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._SD_SDD_StepDetails_Day (
                SD_SDD_SD_ID,
                Metadata_SD_SDD,
                SD_SDD_StepDetails_Day
            )
            SELECT
                SD_SDD_SD_ID,
                Metadata_SD_SDD,
                SD_SDD_StepDetails_Day
            FROM
                _tmp_SD_SDD_StepDetails_Day
            WHERE
                    SD_SDD_Version = currentVersion
              AND
                    SD_SDD_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_SD_SDD_StepDetails_Day;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaSD_SDD_StepDetails_Day
    AFTER INSERT ON skllzz.SD_SDD_StepDetails_Day
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaSD_SDD_StepDetails_Day();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbSD_SDM_StepDetails_Meters ON skllzz.SD_SDM_StepDetails_Meters;
-- DROP FUNCTION IF EXISTS skllzz.tcbSD_SDM_StepDetails_Meters();
CREATE OR REPLACE FUNCTION skllzz.tcbSD_SDM_StepDetails_Meters() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_SD_SDM_StepDetails_Meters (
                                                                                SD_SDM_SD_ID int not null,
                                                                                Metadata_SD_SDM int not null,
                                                                                SD_SDM_StepDetails_Meters integer not null,
                                                                                SD_SDM_Version bigint not null,
                                                                                SD_SDM_StatementType char(1) not null,
                                                                                primary key(
                                                                                            SD_SDM_Version,
                                                                                            SD_SDM_SD_ID
                                                                                    )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbSD_SDM_StepDetails_Meters
    BEFORE INSERT ON skllzz.SD_SDM_StepDetails_Meters
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbSD_SDM_StepDetails_Meters();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciSD_SDM_StepDetails_Meters ON skllzz.SD_SDM_StepDetails_Meters;
-- DROP FUNCTION IF EXISTS skllzz.tciSD_SDM_StepDetails_Meters();
CREATE OR REPLACE FUNCTION skllzz.tciSD_SDM_StepDetails_Meters() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_SD_SDM_StepDetails_Meters
        SELECT
            NEW.SD_SDM_SD_ID,
            NEW.Metadata_SD_SDM,
            NEW.SD_SDM_StepDetails_Meters,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciSD_SDM_StepDetails_Meters
    INSTEAD OF INSERT ON skllzz.SD_SDM_StepDetails_Meters
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciSD_SDM_StepDetails_Meters();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaSD_SDM_StepDetails_Meters ON skllzz.SD_SDM_StepDetails_Meters;
-- DROP FUNCTION IF EXISTS skllzz.tcaSD_SDM_StepDetails_Meters();
CREATE OR REPLACE FUNCTION skllzz.tcaSD_SDM_StepDetails_Meters() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(SD_SDM_Version) INTO maxVersion
        FROM
            _tmp_SD_SDM_StepDetails_Meters;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_SD_SDM_StepDetails_Meters
            SET
                SD_SDM_StatementType =
                    CASE
                        WHEN SDM.SD_SDM_SD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_SD_SDM_StepDetails_Meters v
                    LEFT JOIN
                skllzz._SD_SDM_StepDetails_Meters SDM
                ON
                            SDM.SD_SDM_SD_ID = v.SD_SDM_SD_ID
                        AND
                            SDM.SD_SDM_StepDetails_Meters = v.SD_SDM_StepDetails_Meters
            WHERE
                    v.SD_SDM_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._SD_SDM_StepDetails_Meters (
                SD_SDM_SD_ID,
                Metadata_SD_SDM,
                SD_SDM_StepDetails_Meters
            )
            SELECT
                SD_SDM_SD_ID,
                Metadata_SD_SDM,
                SD_SDM_StepDetails_Meters
            FROM
                _tmp_SD_SDM_StepDetails_Meters
            WHERE
                    SD_SDM_Version = currentVersion
              AND
                    SD_SDM_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_SD_SDM_StepDetails_Meters;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaSD_SDM_StepDetails_Meters
    AFTER INSERT ON skllzz.SD_SDM_StepDetails_Meters
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaSD_SDM_StepDetails_Meters();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbHD_HMN_HrDetails_MinHr ON skllzz.HD_HMN_HrDetails_MinHr;
-- DROP FUNCTION IF EXISTS skllzz.tcbHD_HMN_HrDetails_MinHr();
CREATE OR REPLACE FUNCTION skllzz.tcbHD_HMN_HrDetails_MinHr() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_HD_HMN_HrDetails_MinHr (
                                                                             HD_HMN_HD_ID int not null,
                                                                             Metadata_HD_HMN int not null,
                                                                             HD_HMN_HrDetails_MinHr integer not null,
                                                                             HD_HMN_Version bigint not null,
                                                                             HD_HMN_StatementType char(1) not null,
                                                                             primary key(
                                                                                         HD_HMN_Version,
                                                                                         HD_HMN_HD_ID
                                                                                 )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbHD_HMN_HrDetails_MinHr
    BEFORE INSERT ON skllzz.HD_HMN_HrDetails_MinHr
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbHD_HMN_HrDetails_MinHr();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciHD_HMN_HrDetails_MinHr ON skllzz.HD_HMN_HrDetails_MinHr;
-- DROP FUNCTION IF EXISTS skllzz.tciHD_HMN_HrDetails_MinHr();
CREATE OR REPLACE FUNCTION skllzz.tciHD_HMN_HrDetails_MinHr() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_HD_HMN_HrDetails_MinHr
        SELECT
            NEW.HD_HMN_HD_ID,
            NEW.Metadata_HD_HMN,
            NEW.HD_HMN_HrDetails_MinHr,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciHD_HMN_HrDetails_MinHr
    INSTEAD OF INSERT ON skllzz.HD_HMN_HrDetails_MinHr
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciHD_HMN_HrDetails_MinHr();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaHD_HMN_HrDetails_MinHr ON skllzz.HD_HMN_HrDetails_MinHr;
-- DROP FUNCTION IF EXISTS skllzz.tcaHD_HMN_HrDetails_MinHr();
CREATE OR REPLACE FUNCTION skllzz.tcaHD_HMN_HrDetails_MinHr() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(HD_HMN_Version) INTO maxVersion
        FROM
            _tmp_HD_HMN_HrDetails_MinHr;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_HD_HMN_HrDetails_MinHr
            SET
                HD_HMN_StatementType =
                    CASE
                        WHEN HMN.HD_HMN_HD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_HD_HMN_HrDetails_MinHr v
                    LEFT JOIN
                skllzz._HD_HMN_HrDetails_MinHr HMN
                ON
                            HMN.HD_HMN_HD_ID = v.HD_HMN_HD_ID
                        AND
                            HMN.HD_HMN_HrDetails_MinHr = v.HD_HMN_HrDetails_MinHr
            WHERE
                    v.HD_HMN_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._HD_HMN_HrDetails_MinHr (
                HD_HMN_HD_ID,
                Metadata_HD_HMN,
                HD_HMN_HrDetails_MinHr
            )
            SELECT
                HD_HMN_HD_ID,
                Metadata_HD_HMN,
                HD_HMN_HrDetails_MinHr
            FROM
                _tmp_HD_HMN_HrDetails_MinHr
            WHERE
                    HD_HMN_Version = currentVersion
              AND
                    HD_HMN_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_HD_HMN_HrDetails_MinHr;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaHD_HMN_HrDetails_MinHr
    AFTER INSERT ON skllzz.HD_HMN_HrDetails_MinHr
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaHD_HMN_HrDetails_MinHr();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbHD_HAV_HrDetails_AvgHr ON skllzz.HD_HAV_HrDetails_AvgHr;
-- DROP FUNCTION IF EXISTS skllzz.tcbHD_HAV_HrDetails_AvgHr();
CREATE OR REPLACE FUNCTION skllzz.tcbHD_HAV_HrDetails_AvgHr() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_HD_HAV_HrDetails_AvgHr (
                                                                             HD_HAV_HD_ID int not null,
                                                                             Metadata_HD_HAV int not null,
                                                                             HD_HAV_HrDetails_AvgHr integer not null,
                                                                             HD_HAV_Version bigint not null,
                                                                             HD_HAV_StatementType char(1) not null,
                                                                             primary key(
                                                                                         HD_HAV_Version,
                                                                                         HD_HAV_HD_ID
                                                                                 )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbHD_HAV_HrDetails_AvgHr
    BEFORE INSERT ON skllzz.HD_HAV_HrDetails_AvgHr
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbHD_HAV_HrDetails_AvgHr();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciHD_HAV_HrDetails_AvgHr ON skllzz.HD_HAV_HrDetails_AvgHr;
-- DROP FUNCTION IF EXISTS skllzz.tciHD_HAV_HrDetails_AvgHr();
CREATE OR REPLACE FUNCTION skllzz.tciHD_HAV_HrDetails_AvgHr() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_HD_HAV_HrDetails_AvgHr
        SELECT
            NEW.HD_HAV_HD_ID,
            NEW.Metadata_HD_HAV,
            NEW.HD_HAV_HrDetails_AvgHr,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciHD_HAV_HrDetails_AvgHr
    INSTEAD OF INSERT ON skllzz.HD_HAV_HrDetails_AvgHr
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciHD_HAV_HrDetails_AvgHr();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaHD_HAV_HrDetails_AvgHr ON skllzz.HD_HAV_HrDetails_AvgHr;
-- DROP FUNCTION IF EXISTS skllzz.tcaHD_HAV_HrDetails_AvgHr();
CREATE OR REPLACE FUNCTION skllzz.tcaHD_HAV_HrDetails_AvgHr() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(HD_HAV_Version) INTO maxVersion
        FROM
            _tmp_HD_HAV_HrDetails_AvgHr;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_HD_HAV_HrDetails_AvgHr
            SET
                HD_HAV_StatementType =
                    CASE
                        WHEN HAV.HD_HAV_HD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_HD_HAV_HrDetails_AvgHr v
                    LEFT JOIN
                skllzz._HD_HAV_HrDetails_AvgHr HAV
                ON
                            HAV.HD_HAV_HD_ID = v.HD_HAV_HD_ID
                        AND
                            HAV.HD_HAV_HrDetails_AvgHr = v.HD_HAV_HrDetails_AvgHr
            WHERE
                    v.HD_HAV_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._HD_HAV_HrDetails_AvgHr (
                HD_HAV_HD_ID,
                Metadata_HD_HAV,
                HD_HAV_HrDetails_AvgHr
            )
            SELECT
                HD_HAV_HD_ID,
                Metadata_HD_HAV,
                HD_HAV_HrDetails_AvgHr
            FROM
                _tmp_HD_HAV_HrDetails_AvgHr
            WHERE
                    HD_HAV_Version = currentVersion
              AND
                    HD_HAV_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_HD_HAV_HrDetails_AvgHr;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaHD_HAV_HrDetails_AvgHr
    AFTER INSERT ON skllzz.HD_HAV_HrDetails_AvgHr
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaHD_HAV_HrDetails_AvgHr();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbHD_MHR_HrDetails_MaxHr ON skllzz.HD_MHR_HrDetails_MaxHr;
-- DROP FUNCTION IF EXISTS skllzz.tcbHD_MHR_HrDetails_MaxHr();
CREATE OR REPLACE FUNCTION skllzz.tcbHD_MHR_HrDetails_MaxHr() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_HD_MHR_HrDetails_MaxHr (
                                                                             HD_MHR_HD_ID int not null,
                                                                             Metadata_HD_MHR int not null,
                                                                             HD_MHR_HrDetails_MaxHr integer not null,
                                                                             HD_MHR_Version bigint not null,
                                                                             HD_MHR_StatementType char(1) not null,
                                                                             primary key(
                                                                                         HD_MHR_Version,
                                                                                         HD_MHR_HD_ID
                                                                                 )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbHD_MHR_HrDetails_MaxHr
    BEFORE INSERT ON skllzz.HD_MHR_HrDetails_MaxHr
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbHD_MHR_HrDetails_MaxHr();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciHD_MHR_HrDetails_MaxHr ON skllzz.HD_MHR_HrDetails_MaxHr;
-- DROP FUNCTION IF EXISTS skllzz.tciHD_MHR_HrDetails_MaxHr();
CREATE OR REPLACE FUNCTION skllzz.tciHD_MHR_HrDetails_MaxHr() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_HD_MHR_HrDetails_MaxHr
        SELECT
            NEW.HD_MHR_HD_ID,
            NEW.Metadata_HD_MHR,
            NEW.HD_MHR_HrDetails_MaxHr,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciHD_MHR_HrDetails_MaxHr
    INSTEAD OF INSERT ON skllzz.HD_MHR_HrDetails_MaxHr
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciHD_MHR_HrDetails_MaxHr();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaHD_MHR_HrDetails_MaxHr ON skllzz.HD_MHR_HrDetails_MaxHr;
-- DROP FUNCTION IF EXISTS skllzz.tcaHD_MHR_HrDetails_MaxHr();
CREATE OR REPLACE FUNCTION skllzz.tcaHD_MHR_HrDetails_MaxHr() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(HD_MHR_Version) INTO maxVersion
        FROM
            _tmp_HD_MHR_HrDetails_MaxHr;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_HD_MHR_HrDetails_MaxHr
            SET
                HD_MHR_StatementType =
                    CASE
                        WHEN MHR.HD_MHR_HD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_HD_MHR_HrDetails_MaxHr v
                    LEFT JOIN
                skllzz._HD_MHR_HrDetails_MaxHr MHR
                ON
                            MHR.HD_MHR_HD_ID = v.HD_MHR_HD_ID
                        AND
                            MHR.HD_MHR_HrDetails_MaxHr = v.HD_MHR_HrDetails_MaxHr
            WHERE
                    v.HD_MHR_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._HD_MHR_HrDetails_MaxHr (
                HD_MHR_HD_ID,
                Metadata_HD_MHR,
                HD_MHR_HrDetails_MaxHr
            )
            SELECT
                HD_MHR_HD_ID,
                Metadata_HD_MHR,
                HD_MHR_HrDetails_MaxHr
            FROM
                _tmp_HD_MHR_HrDetails_MaxHr
            WHERE
                    HD_MHR_Version = currentVersion
              AND
                    HD_MHR_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_HD_MHR_HrDetails_MaxHr;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaHD_MHR_HrDetails_MaxHr
    AFTER INSERT ON skllzz.HD_MHR_HrDetails_MaxHr
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaHD_MHR_HrDetails_MaxHr();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbHD_MNH_HrDetails_MinHardness ON skllzz.HD_MNH_HrDetails_MinHardness;
-- DROP FUNCTION IF EXISTS skllzz.tcbHD_MNH_HrDetails_MinHardness();
CREATE OR REPLACE FUNCTION skllzz.tcbHD_MNH_HrDetails_MinHardness() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_HD_MNH_HrDetails_MinHardness (
                                                                                   HD_MNH_HD_ID int not null,
                                                                                   Metadata_HD_MNH int not null,
                                                                                   HD_MNH_HrDetails_MinHardness double precision not null,
                                                                                   HD_MNH_Version bigint not null,
                                                                                   HD_MNH_StatementType char(1) not null,
                                                                                   primary key(
                                                                                               HD_MNH_Version,
                                                                                               HD_MNH_HD_ID
                                                                                       )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbHD_MNH_HrDetails_MinHardness
    BEFORE INSERT ON skllzz.HD_MNH_HrDetails_MinHardness
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbHD_MNH_HrDetails_MinHardness();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciHD_MNH_HrDetails_MinHardness ON skllzz.HD_MNH_HrDetails_MinHardness;
-- DROP FUNCTION IF EXISTS skllzz.tciHD_MNH_HrDetails_MinHardness();
CREATE OR REPLACE FUNCTION skllzz.tciHD_MNH_HrDetails_MinHardness() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_HD_MNH_HrDetails_MinHardness
        SELECT
            NEW.HD_MNH_HD_ID,
            NEW.Metadata_HD_MNH,
            NEW.HD_MNH_HrDetails_MinHardness,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciHD_MNH_HrDetails_MinHardness
    INSTEAD OF INSERT ON skllzz.HD_MNH_HrDetails_MinHardness
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciHD_MNH_HrDetails_MinHardness();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaHD_MNH_HrDetails_MinHardness ON skllzz.HD_MNH_HrDetails_MinHardness;
-- DROP FUNCTION IF EXISTS skllzz.tcaHD_MNH_HrDetails_MinHardness();
CREATE OR REPLACE FUNCTION skllzz.tcaHD_MNH_HrDetails_MinHardness() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(HD_MNH_Version) INTO maxVersion
        FROM
            _tmp_HD_MNH_HrDetails_MinHardness;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_HD_MNH_HrDetails_MinHardness
            SET
                HD_MNH_StatementType =
                    CASE
                        WHEN MNH.HD_MNH_HD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_HD_MNH_HrDetails_MinHardness v
                    LEFT JOIN
                skllzz._HD_MNH_HrDetails_MinHardness MNH
                ON
                            MNH.HD_MNH_HD_ID = v.HD_MNH_HD_ID
                        AND
                            MNH.HD_MNH_HrDetails_MinHardness = v.HD_MNH_HrDetails_MinHardness
            WHERE
                    v.HD_MNH_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._HD_MNH_HrDetails_MinHardness (
                HD_MNH_HD_ID,
                Metadata_HD_MNH,
                HD_MNH_HrDetails_MinHardness
            )
            SELECT
                HD_MNH_HD_ID,
                Metadata_HD_MNH,
                HD_MNH_HrDetails_MinHardness
            FROM
                _tmp_HD_MNH_HrDetails_MinHardness
            WHERE
                    HD_MNH_Version = currentVersion
              AND
                    HD_MNH_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_HD_MNH_HrDetails_MinHardness;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaHD_MNH_HrDetails_MinHardness
    AFTER INSERT ON skllzz.HD_MNH_HrDetails_MinHardness
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaHD_MNH_HrDetails_MinHardness();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbHD_AHR_HrDetails_AvgHardness ON skllzz.HD_AHR_HrDetails_AvgHardness;
-- DROP FUNCTION IF EXISTS skllzz.tcbHD_AHR_HrDetails_AvgHardness();
CREATE OR REPLACE FUNCTION skllzz.tcbHD_AHR_HrDetails_AvgHardness() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_HD_AHR_HrDetails_AvgHardness (
                                                                                   HD_AHR_HD_ID int not null,
                                                                                   Metadata_HD_AHR int not null,
                                                                                   HD_AHR_HrDetails_AvgHardness double precision not null,
                                                                                   HD_AHR_Version bigint not null,
                                                                                   HD_AHR_StatementType char(1) not null,
                                                                                   primary key(
                                                                                               HD_AHR_Version,
                                                                                               HD_AHR_HD_ID
                                                                                       )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbHD_AHR_HrDetails_AvgHardness
    BEFORE INSERT ON skllzz.HD_AHR_HrDetails_AvgHardness
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbHD_AHR_HrDetails_AvgHardness();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciHD_AHR_HrDetails_AvgHardness ON skllzz.HD_AHR_HrDetails_AvgHardness;
-- DROP FUNCTION IF EXISTS skllzz.tciHD_AHR_HrDetails_AvgHardness();
CREATE OR REPLACE FUNCTION skllzz.tciHD_AHR_HrDetails_AvgHardness() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_HD_AHR_HrDetails_AvgHardness
        SELECT
            NEW.HD_AHR_HD_ID,
            NEW.Metadata_HD_AHR,
            NEW.HD_AHR_HrDetails_AvgHardness,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciHD_AHR_HrDetails_AvgHardness
    INSTEAD OF INSERT ON skllzz.HD_AHR_HrDetails_AvgHardness
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciHD_AHR_HrDetails_AvgHardness();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaHD_AHR_HrDetails_AvgHardness ON skllzz.HD_AHR_HrDetails_AvgHardness;
-- DROP FUNCTION IF EXISTS skllzz.tcaHD_AHR_HrDetails_AvgHardness();
CREATE OR REPLACE FUNCTION skllzz.tcaHD_AHR_HrDetails_AvgHardness() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(HD_AHR_Version) INTO maxVersion
        FROM
            _tmp_HD_AHR_HrDetails_AvgHardness;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_HD_AHR_HrDetails_AvgHardness
            SET
                HD_AHR_StatementType =
                    CASE
                        WHEN AHR.HD_AHR_HD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_HD_AHR_HrDetails_AvgHardness v
                    LEFT JOIN
                skllzz._HD_AHR_HrDetails_AvgHardness AHR
                ON
                            AHR.HD_AHR_HD_ID = v.HD_AHR_HD_ID
                        AND
                            AHR.HD_AHR_HrDetails_AvgHardness = v.HD_AHR_HrDetails_AvgHardness
            WHERE
                    v.HD_AHR_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._HD_AHR_HrDetails_AvgHardness (
                HD_AHR_HD_ID,
                Metadata_HD_AHR,
                HD_AHR_HrDetails_AvgHardness
            )
            SELECT
                HD_AHR_HD_ID,
                Metadata_HD_AHR,
                HD_AHR_HrDetails_AvgHardness
            FROM
                _tmp_HD_AHR_HrDetails_AvgHardness
            WHERE
                    HD_AHR_Version = currentVersion
              AND
                    HD_AHR_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_HD_AHR_HrDetails_AvgHardness;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaHD_AHR_HrDetails_AvgHardness
    AFTER INSERT ON skllzz.HD_AHR_HrDetails_AvgHardness
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaHD_AHR_HrDetails_AvgHardness();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbHD_MXH_HrDetails_MaxHardness ON skllzz.HD_MXH_HrDetails_MaxHardness;
-- DROP FUNCTION IF EXISTS skllzz.tcbHD_MXH_HrDetails_MaxHardness();
CREATE OR REPLACE FUNCTION skllzz.tcbHD_MXH_HrDetails_MaxHardness() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_HD_MXH_HrDetails_MaxHardness (
                                                                                   HD_MXH_HD_ID int not null,
                                                                                   Metadata_HD_MXH int not null,
                                                                                   HD_MXH_HrDetails_MaxHardness double precision not null,
                                                                                   HD_MXH_Version bigint not null,
                                                                                   HD_MXH_StatementType char(1) not null,
                                                                                   primary key(
                                                                                               HD_MXH_Version,
                                                                                               HD_MXH_HD_ID
                                                                                       )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbHD_MXH_HrDetails_MaxHardness
    BEFORE INSERT ON skllzz.HD_MXH_HrDetails_MaxHardness
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbHD_MXH_HrDetails_MaxHardness();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciHD_MXH_HrDetails_MaxHardness ON skllzz.HD_MXH_HrDetails_MaxHardness;
-- DROP FUNCTION IF EXISTS skllzz.tciHD_MXH_HrDetails_MaxHardness();
CREATE OR REPLACE FUNCTION skllzz.tciHD_MXH_HrDetails_MaxHardness() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_HD_MXH_HrDetails_MaxHardness
        SELECT
            NEW.HD_MXH_HD_ID,
            NEW.Metadata_HD_MXH,
            NEW.HD_MXH_HrDetails_MaxHardness,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciHD_MXH_HrDetails_MaxHardness
    INSTEAD OF INSERT ON skllzz.HD_MXH_HrDetails_MaxHardness
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciHD_MXH_HrDetails_MaxHardness();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaHD_MXH_HrDetails_MaxHardness ON skllzz.HD_MXH_HrDetails_MaxHardness;
-- DROP FUNCTION IF EXISTS skllzz.tcaHD_MXH_HrDetails_MaxHardness();
CREATE OR REPLACE FUNCTION skllzz.tcaHD_MXH_HrDetails_MaxHardness() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(HD_MXH_Version) INTO maxVersion
        FROM
            _tmp_HD_MXH_HrDetails_MaxHardness;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_HD_MXH_HrDetails_MaxHardness
            SET
                HD_MXH_StatementType =
                    CASE
                        WHEN MXH.HD_MXH_HD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_HD_MXH_HrDetails_MaxHardness v
                    LEFT JOIN
                skllzz._HD_MXH_HrDetails_MaxHardness MXH
                ON
                            MXH.HD_MXH_HD_ID = v.HD_MXH_HD_ID
                        AND
                            MXH.HD_MXH_HrDetails_MaxHardness = v.HD_MXH_HrDetails_MaxHardness
            WHERE
                    v.HD_MXH_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._HD_MXH_HrDetails_MaxHardness (
                HD_MXH_HD_ID,
                Metadata_HD_MXH,
                HD_MXH_HrDetails_MaxHardness
            )
            SELECT
                HD_MXH_HD_ID,
                Metadata_HD_MXH,
                HD_MXH_HrDetails_MaxHardness
            FROM
                _tmp_HD_MXH_HrDetails_MaxHardness
            WHERE
                    HD_MXH_Version = currentVersion
              AND
                    HD_MXH_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_HD_MXH_HrDetails_MaxHardness;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaHD_MXH_HrDetails_MaxHardness
    AFTER INSERT ON skllzz.HD_MXH_HrDetails_MaxHardness
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaHD_MXH_HrDetails_MaxHardness();
-- BEFORE INSERT trigger ----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcbHD_HRD_HrDetails_Device ON skllzz.HD_HRD_HrDetails_Device;
-- DROP FUNCTION IF EXISTS skllzz.tcbHD_HRD_HrDetails_Device();
CREATE OR REPLACE FUNCTION skllzz.tcbHD_HRD_HrDetails_Device() RETURNS trigger AS '
    BEGIN
        -- temporary table is used to create an insert order
        -- (so that rows are inserted in order with respect to temporality)
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_HD_HRD_HrDetails_Device (
                                                                              HD_HRD_HD_ID int not null,
                                                                              Metadata_HD_HRD int not null,
                                                                              HD_HRD_HrDetails_Device varchar(100)[] not null,
                                                                              HD_HRD_Version bigint not null,
                                                                              HD_HRD_StatementType char(1) not null,
                                                                              primary key(
                                                                                          HD_HRD_Version,
                                                                                          HD_HRD_HD_ID
                                                                                  )
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcbHD_HRD_HrDetails_Device
    BEFORE INSERT ON skllzz.HD_HRD_HrDetails_Device
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcbHD_HRD_HrDetails_Device();
-- INSTEAD OF INSERT trigger ------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tciHD_HRD_HrDetails_Device ON skllzz.HD_HRD_HrDetails_Device;
-- DROP FUNCTION IF EXISTS skllzz.tciHD_HRD_HrDetails_Device();
CREATE OR REPLACE FUNCTION skllzz.tciHD_HRD_HrDetails_Device() RETURNS trigger AS '
    BEGIN
        -- insert rows into the temporary table
        INSERT INTO _tmp_HD_HRD_HrDetails_Device
        SELECT
            NEW.HD_HRD_HD_ID,
            NEW.Metadata_HD_HRD,
            NEW.HD_HRD_HrDetails_Device,
            1,
            ''X'';
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tciHD_HRD_HrDetails_Device
    INSTEAD OF INSERT ON skllzz.HD_HRD_HrDetails_Device
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.tciHD_HRD_HrDetails_Device();
-- AFTER INSERT trigger -----------------------------------------------------------------------------------------------
-- DROP TRIGGER IF EXISTS tcaHD_HRD_HrDetails_Device ON skllzz.HD_HRD_HrDetails_Device;
-- DROP FUNCTION IF EXISTS skllzz.tcaHD_HRD_HrDetails_Device();
CREATE OR REPLACE FUNCTION skllzz.tcaHD_HRD_HrDetails_Device() RETURNS trigger AS '
    DECLARE maxVersion int;
        DECLARE currentVersion int = 0;
    BEGIN
        -- find max version
        SELECT
            MAX(HD_HRD_Version) INTO maxVersion
        FROM
            _tmp_HD_HRD_HrDetails_Device;
        -- is max version NULL?
        IF (maxVersion is null) THEN
            RETURN NULL;
        END IF;
        -- loop over versions
        LOOP
            currentVersion := currentVersion + 1;
            -- set statement types
            UPDATE _tmp_HD_HRD_HrDetails_Device
            SET
                HD_HRD_StatementType =
                    CASE
                        WHEN HRD.HD_HRD_HD_ID is not null
                            THEN ''D'' -- duplicate
                        ELSE ''N'' -- new statement
                        END
            FROM
                _tmp_HD_HRD_HrDetails_Device v
                    LEFT JOIN
                skllzz._HD_HRD_HrDetails_Device HRD
                ON
                            HRD.HD_HRD_HD_ID = v.HD_HRD_HD_ID
                        AND
                            HRD.HD_HRD_HrDetails_Device = v.HD_HRD_HrDetails_Device
            WHERE
                    v.HD_HRD_Version = currentVersion;
            -- insert data into attribute table
            INSERT INTO skllzz._HD_HRD_HrDetails_Device (
                HD_HRD_HD_ID,
                Metadata_HD_HRD,
                HD_HRD_HrDetails_Device
            )
            SELECT
                HD_HRD_HD_ID,
                Metadata_HD_HRD,
                HD_HRD_HrDetails_Device
            FROM
                _tmp_HD_HRD_HrDetails_Device
            WHERE
                    HD_HRD_Version = currentVersion
              AND
                    HD_HRD_StatementType in (''N'');
            EXIT WHEN currentVersion >= maxVersion;
        END LOOP;
        DROP TABLE IF EXISTS _tmp_HD_HRD_HrDetails_Device;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER tcaHD_HRD_HrDetails_Device
    AFTER INSERT ON skllzz.HD_HRD_HrDetails_Device
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.tcaHD_HRD_HrDetails_Device();
-- ANCHOR TRIGGERS ---------------------------------------------------------------------------------------------------
--
-- The following triggers on the latest view make it behave like a table.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- BEFORE INSERT trigger --------------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS itb_lTS_TrainingSession ON skllzz.lTS_TrainingSession;
--DROP FUNCTION IF EXISTS skllzz.itb_lTS_TrainingSession();
CREATE OR REPLACE FUNCTION skllzz.itb_lTS_TrainingSession() RETURNS trigger AS '
    BEGIN
        -- create temporary table to keep inserted rows in
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_it_TS_TrainingSession (
                                                                            TS_ID int not null,
                                                                            Metadata_TS int not null,
                                                                            TS_TID_TS_ID int null,
                                                                            Metadata_TS_TID int null,
                                                                            TS_TID_TrainingSession_Id varchar(100) null,
                                                                            TS_TSR_TS_ID int null,
                                                                            Metadata_TS_TSR int null,
                                                                            TS_TSR_TrainingSession_StartMillis bigint null,
                                                                            TS_TST_TS_ID int null,
                                                                            Metadata_TS_TST int null,
                                                                            TS_TST_TrainingSession_StopMillis bigint null,
                                                                            TS_TSZ_TS_ID int null,
                                                                            Metadata_TS_TSZ int null,
                                                                            TS_TSZ_TrainingSession_Skllzz DOUBLE PRECISION null,
                                                                            TS_TDL_TS_ID int null,
                                                                            Metadata_TS_TDL int null,
                                                                            TS_TDL_TrainingSession_Deleted boolean null,
                                                                            TS_TSI_TS_ID int null,
                                                                            Metadata_TS_TSI int null,
                                                                            TS_TSI_TrainingSession_SourceId varchar(100) null,
                                                                            TS_TSV_TS_ID int null,
                                                                            Metadata_TS_TSV int null,
                                                                            TS_TSV_TrainingSession_Version integer null,
                                                                            TS_TTZ_TS_ID int null,
                                                                            Metadata_TS_TTZ int null,
                                                                            TS_TTZ_TrainingSession_Timezone varchar(255) null,
                                                                            TS_TSY_TS_ID int null,
                                                                            Metadata_TS_TSY int null,
                                                                            TS_TSY_TrainingSession_SyncMillis bigint null,
                                                                            TS_TKC_TS_ID int null,
                                                                            Metadata_TS_TKC int null,
                                                                            TS_TKC_TrainingSession_KCal DOUBLE PRECISION null,
                                                                            TS_TPR_TS_ID int null,
                                                                            Metadata_TS_TPR int null,
                                                                            TS_TPR_TrainingSession_ProfileId varchar(100) null
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER itb_lTS_TrainingSession
    BEFORE INSERT ON skllzz.lTS_TrainingSession
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.itb_lTS_TrainingSession();
-- INSTEAD OF INSERT trigger ----------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS iti_lTS_TrainingSession ON skllzz.lTS_TrainingSession;
--DROP FUNCTION IF EXISTS skllzz.iti_lTS_TrainingSession();
CREATE OR REPLACE FUNCTION skllzz.iti_lTS_TrainingSession() RETURNS trigger AS '
    BEGIN
        -- generate anchor ID (if not provided)
        IF (NEW.TS_ID IS NULL) THEN
            INSERT INTO skllzz.TS_TrainingSession (
                Metadata_TS
            ) VALUES (
                         NEW.Metadata_TS
                     );
            SELECT
                lastval()
            INTO NEW.TS_ID;
            -- if anchor ID is provided then let''s insert it into the anchor table
            -- but only if that ID is not present in the anchor table
        ELSE
            INSERT INTO skllzz.TS_TrainingSession (
                Metadata_TS,
                TS_ID
            )
            SELECT
                NEW.Metadata_TS,
                NEW.TS_ID
            WHERE NOT EXISTS(
                    SELECT
                        TS_ID
                    FROM skllzz.TS_TrainingSession
                    WHERE TS_ID = NEW.TS_ID
                    LIMIT 1
                );
        END IF;
        -- insert row into temporary table
        INSERT INTO _tmp_it_TS_TrainingSession (
            TS_ID,
            Metadata_TS,
            TS_TID_TS_ID,
            Metadata_TS_TID,
            TS_TID_TrainingSession_Id,
            TS_TSR_TS_ID,
            Metadata_TS_TSR,
            TS_TSR_TrainingSession_StartMillis,
            TS_TST_TS_ID,
            Metadata_TS_TST,
            TS_TST_TrainingSession_StopMillis,
            TS_TSZ_TS_ID,
            Metadata_TS_TSZ,
            TS_TSZ_TrainingSession_Skllzz,
            TS_TDL_TS_ID,
            Metadata_TS_TDL,
            TS_TDL_TrainingSession_Deleted,
            TS_TSI_TS_ID,
            Metadata_TS_TSI,
            TS_TSI_TrainingSession_SourceId,
            TS_TSV_TS_ID,
            Metadata_TS_TSV,
            TS_TSV_TrainingSession_Version,
            TS_TTZ_TS_ID,
            Metadata_TS_TTZ,
            TS_TTZ_TrainingSession_Timezone,
            TS_TSY_TS_ID,
            Metadata_TS_TSY,
            TS_TSY_TrainingSession_SyncMillis,
            TS_TKC_TS_ID,
            Metadata_TS_TKC,
            TS_TKC_TrainingSession_KCal,
            TS_TPR_TS_ID,
            Metadata_TS_TPR,
            TS_TPR_TrainingSession_ProfileId
        ) VALUES (
                     NEW.TS_ID,
                     NEW.Metadata_TS,
                     COALESCE(NEW.TS_TID_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TID, NEW.Metadata_TS),
                     NEW.TS_TID_TrainingSession_Id,
                     COALESCE(NEW.TS_TSR_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TSR, NEW.Metadata_TS),
                     NEW.TS_TSR_TrainingSession_StartMillis,
                     COALESCE(NEW.TS_TST_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TST, NEW.Metadata_TS),
                     NEW.TS_TST_TrainingSession_StopMillis,
                     COALESCE(NEW.TS_TSZ_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TSZ, NEW.Metadata_TS),
                     NEW.TS_TSZ_TrainingSession_Skllzz,
                     COALESCE(NEW.TS_TDL_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TDL, NEW.Metadata_TS),
                     NEW.TS_TDL_TrainingSession_Deleted,
                     COALESCE(NEW.TS_TSI_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TSI, NEW.Metadata_TS),
                     NEW.TS_TSI_TrainingSession_SourceId,
                     COALESCE(NEW.TS_TSV_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TSV, NEW.Metadata_TS),
                     NEW.TS_TSV_TrainingSession_Version,
                     COALESCE(NEW.TS_TTZ_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TTZ, NEW.Metadata_TS),
                     NEW.TS_TTZ_TrainingSession_Timezone,
                     COALESCE(NEW.TS_TSY_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TSY, NEW.Metadata_TS),
                     NEW.TS_TSY_TrainingSession_SyncMillis,
                     COALESCE(NEW.TS_TKC_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TKC, NEW.Metadata_TS),
                     NEW.TS_TKC_TrainingSession_KCal,
                     COALESCE(NEW.TS_TPR_TS_ID, NEW.TS_ID),
                     COALESCE(NEW.Metadata_TS_TPR, NEW.Metadata_TS),
                     NEW.TS_TPR_TrainingSession_ProfileId
                 );
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER iti_lTS_TrainingSession
    INSTEAD OF INSERT ON skllzz.lTS_TrainingSession
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.iti_lTS_TrainingSession();
-- AFTER INSERT trigger ---------------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS ita_lTS_TrainingSession ON skllzz.lTS_TrainingSession;
--DROP FUNCTION IF EXISTS skllzz.ita_lTS_TrainingSession();
CREATE OR REPLACE FUNCTION skllzz.ita_lTS_TrainingSession() RETURNS trigger AS '
    BEGIN
        INSERT INTO skllzz.TS_TID_TrainingSession_Id (
            TS_TID_TS_ID,
            Metadata_TS_TID,
            TS_TID_TrainingSession_Id
        )
        SELECT
            i.TS_TID_TS_ID,
            i.Metadata_TS_TID,
            i.TS_TID_TrainingSession_Id
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TID_TrainingSession_Id is not null;
        INSERT INTO skllzz.TS_TSR_TrainingSession_StartMillis (
            TS_TSR_TS_ID,
            Metadata_TS_TSR,
            TS_TSR_TrainingSession_StartMillis
        )
        SELECT
            i.TS_TSR_TS_ID,
            i.Metadata_TS_TSR,
            i.TS_TSR_TrainingSession_StartMillis
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TSR_TrainingSession_StartMillis is not null;
        INSERT INTO skllzz.TS_TST_TrainingSession_StopMillis (
            TS_TST_TS_ID,
            Metadata_TS_TST,
            TS_TST_TrainingSession_StopMillis
        )
        SELECT
            i.TS_TST_TS_ID,
            i.Metadata_TS_TST,
            i.TS_TST_TrainingSession_StopMillis
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TST_TrainingSession_StopMillis is not null;
        INSERT INTO skllzz.TS_TSZ_TrainingSession_Skllzz (
            TS_TSZ_TS_ID,
            Metadata_TS_TSZ,
            TS_TSZ_TrainingSession_Skllzz
        )
        SELECT
            i.TS_TSZ_TS_ID,
            i.Metadata_TS_TSZ,
            i.TS_TSZ_TrainingSession_Skllzz
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TSZ_TrainingSession_Skllzz is not null;
        INSERT INTO skllzz.TS_TDL_TrainingSession_Deleted (
            TS_TDL_TS_ID,
            Metadata_TS_TDL,
            TS_TDL_TrainingSession_Deleted
        )
        SELECT
            i.TS_TDL_TS_ID,
            i.Metadata_TS_TDL,
            i.TS_TDL_TrainingSession_Deleted
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TDL_TrainingSession_Deleted is not null;
        INSERT INTO skllzz.TS_TSI_TrainingSession_SourceId (
            TS_TSI_TS_ID,
            Metadata_TS_TSI,
            TS_TSI_TrainingSession_SourceId
        )
        SELECT
            i.TS_TSI_TS_ID,
            i.Metadata_TS_TSI,
            i.TS_TSI_TrainingSession_SourceId
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TSI_TrainingSession_SourceId is not null;
        INSERT INTO skllzz.TS_TSV_TrainingSession_Version (
            TS_TSV_TS_ID,
            Metadata_TS_TSV,
            TS_TSV_TrainingSession_Version
        )
        SELECT
            i.TS_TSV_TS_ID,
            i.Metadata_TS_TSV,
            i.TS_TSV_TrainingSession_Version
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TSV_TrainingSession_Version is not null;
        INSERT INTO skllzz.TS_TTZ_TrainingSession_Timezone (
            TS_TTZ_TS_ID,
            Metadata_TS_TTZ,
            TS_TTZ_TrainingSession_Timezone
        )
        SELECT
            i.TS_TTZ_TS_ID,
            i.Metadata_TS_TTZ,
            i.TS_TTZ_TrainingSession_Timezone
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TTZ_TrainingSession_Timezone is not null;
        INSERT INTO skllzz.TS_TSY_TrainingSession_SyncMillis (
            TS_TSY_TS_ID,
            Metadata_TS_TSY,
            TS_TSY_TrainingSession_SyncMillis
        )
        SELECT
            i.TS_TSY_TS_ID,
            i.Metadata_TS_TSY,
            i.TS_TSY_TrainingSession_SyncMillis
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TSY_TrainingSession_SyncMillis is not null;
        INSERT INTO skllzz.TS_TKC_TrainingSession_KCal (
            TS_TKC_TS_ID,
            Metadata_TS_TKC,
            TS_TKC_TrainingSession_KCal
        )
        SELECT
            i.TS_TKC_TS_ID,
            i.Metadata_TS_TKC,
            i.TS_TKC_TrainingSession_KCal
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TKC_TrainingSession_KCal is not null;
        INSERT INTO skllzz.TS_TPR_TrainingSession_ProfileId (
            TS_TPR_TS_ID,
            Metadata_TS_TPR,
            TS_TPR_TrainingSession_ProfileId
        )
        SELECT
            i.TS_TPR_TS_ID,
            i.Metadata_TS_TPR,
            i.TS_TPR_TrainingSession_ProfileId
        FROM
            _tmp_it_TS_TrainingSession i
        WHERE
            i.TS_TPR_TrainingSession_ProfileId is not null;
        DROP TABLE IF EXISTS _tmp_it_TS_TrainingSession;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER ita_lTS_TrainingSession
    AFTER INSERT ON skllzz.lTS_TrainingSession
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.ita_lTS_TrainingSession();
-- BEFORE INSERT trigger --------------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS itb_lSD_StepDetails ON skllzz.lSD_StepDetails;
--DROP FUNCTION IF EXISTS skllzz.itb_lSD_StepDetails();
CREATE OR REPLACE FUNCTION skllzz.itb_lSD_StepDetails() RETURNS trigger AS '
    BEGIN
        -- create temporary table to keep inserted rows in
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_it_SD_StepDetails (
                                                                        SD_ID int not null,
                                                                        Metadata_SD int not null,
                                                                        SD_SDS_SD_ID int null,
                                                                        Metadata_SD_SDS int null,
                                                                        SD_SDS_StepDetails_Steps integer null,
                                                                        SD_SDD_SD_ID int null,
                                                                        Metadata_SD_SDD int null,
                                                                        SD_SDD_StepDetails_Day integer null,
                                                                        SD_SDM_SD_ID int null,
                                                                        Metadata_SD_SDM int null,
                                                                        SD_SDM_StepDetails_Meters integer null
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER itb_lSD_StepDetails
    BEFORE INSERT ON skllzz.lSD_StepDetails
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.itb_lSD_StepDetails();
-- INSTEAD OF INSERT trigger ----------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS iti_lSD_StepDetails ON skllzz.lSD_StepDetails;
--DROP FUNCTION IF EXISTS skllzz.iti_lSD_StepDetails();
CREATE OR REPLACE FUNCTION skllzz.iti_lSD_StepDetails() RETURNS trigger AS '
    BEGIN
        -- generate anchor ID (if not provided)
        IF (NEW.SD_ID IS NULL) THEN
            INSERT INTO skllzz.SD_StepDetails (
                Metadata_SD
            ) VALUES (
                         NEW.Metadata_SD
                     );
            SELECT
                lastval()
            INTO NEW.SD_ID;
            -- if anchor ID is provided then let''s insert it into the anchor table
            -- but only if that ID is not present in the anchor table
        ELSE
            INSERT INTO skllzz.SD_StepDetails (
                Metadata_SD,
                SD_ID
            )
            SELECT
                NEW.Metadata_SD,
                NEW.SD_ID
            WHERE NOT EXISTS(
                    SELECT
                        SD_ID
                    FROM skllzz.SD_StepDetails
                    WHERE SD_ID = NEW.SD_ID
                    LIMIT 1
                );
        END IF;
        -- insert row into temporary table
        INSERT INTO _tmp_it_SD_StepDetails (
            SD_ID,
            Metadata_SD,
            SD_SDS_SD_ID,
            Metadata_SD_SDS,
            SD_SDS_StepDetails_Steps,
            SD_SDD_SD_ID,
            Metadata_SD_SDD,
            SD_SDD_StepDetails_Day,
            SD_SDM_SD_ID,
            Metadata_SD_SDM,
            SD_SDM_StepDetails_Meters
        ) VALUES (
                     NEW.SD_ID,
                     NEW.Metadata_SD,
                     COALESCE(NEW.SD_SDS_SD_ID, NEW.SD_ID),
                     COALESCE(NEW.Metadata_SD_SDS, NEW.Metadata_SD),
                     NEW.SD_SDS_StepDetails_Steps,
                     COALESCE(NEW.SD_SDD_SD_ID, NEW.SD_ID),
                     COALESCE(NEW.Metadata_SD_SDD, NEW.Metadata_SD),
                     NEW.SD_SDD_StepDetails_Day,
                     COALESCE(NEW.SD_SDM_SD_ID, NEW.SD_ID),
                     COALESCE(NEW.Metadata_SD_SDM, NEW.Metadata_SD),
                     NEW.SD_SDM_StepDetails_Meters
                 );
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER iti_lSD_StepDetails
    INSTEAD OF INSERT ON skllzz.lSD_StepDetails
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.iti_lSD_StepDetails();
-- AFTER INSERT trigger ---------------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS ita_lSD_StepDetails ON skllzz.lSD_StepDetails;
--DROP FUNCTION IF EXISTS skllzz.ita_lSD_StepDetails();
CREATE OR REPLACE FUNCTION skllzz.ita_lSD_StepDetails() RETURNS trigger AS '
    BEGIN
        INSERT INTO skllzz.SD_SDS_StepDetails_Steps (
            SD_SDS_SD_ID,
            Metadata_SD_SDS,
            SD_SDS_StepDetails_Steps
        )
        SELECT
            i.SD_SDS_SD_ID,
            i.Metadata_SD_SDS,
            i.SD_SDS_StepDetails_Steps
        FROM
            _tmp_it_SD_StepDetails i
        WHERE
            i.SD_SDS_StepDetails_Steps is not null;
        INSERT INTO skllzz.SD_SDD_StepDetails_Day (
            SD_SDD_SD_ID,
            Metadata_SD_SDD,
            SD_SDD_StepDetails_Day
        )
        SELECT
            i.SD_SDD_SD_ID,
            i.Metadata_SD_SDD,
            i.SD_SDD_StepDetails_Day
        FROM
            _tmp_it_SD_StepDetails i
        WHERE
            i.SD_SDD_StepDetails_Day is not null;
        INSERT INTO skllzz.SD_SDM_StepDetails_Meters (
            SD_SDM_SD_ID,
            Metadata_SD_SDM,
            SD_SDM_StepDetails_Meters
        )
        SELECT
            i.SD_SDM_SD_ID,
            i.Metadata_SD_SDM,
            i.SD_SDM_StepDetails_Meters
        FROM
            _tmp_it_SD_StepDetails i
        WHERE
            i.SD_SDM_StepDetails_Meters is not null;
        DROP TABLE IF EXISTS _tmp_it_SD_StepDetails;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER ita_lSD_StepDetails
    AFTER INSERT ON skllzz.lSD_StepDetails
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.ita_lSD_StepDetails();
-- BEFORE INSERT trigger --------------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS itb_lHD_HrDetails ON skllzz.lHD_HrDetails;
--DROP FUNCTION IF EXISTS skllzz.itb_lHD_HrDetails();
CREATE OR REPLACE FUNCTION skllzz.itb_lHD_HrDetails() RETURNS trigger AS '
    BEGIN
        -- create temporary table to keep inserted rows in
        CREATE TEMPORARY TABLE IF NOT EXISTS _tmp_it_HD_HrDetails (
                                                                      HD_ID int not null,
                                                                      Metadata_HD int not null,
                                                                      HD_HMN_HD_ID int null,
                                                                      Metadata_HD_HMN int null,
                                                                      HD_HMN_HrDetails_MinHr integer null,
                                                                      HD_HAV_HD_ID int null,
                                                                      Metadata_HD_HAV int null,
                                                                      HD_HAV_HrDetails_AvgHr integer null,
                                                                      HD_MHR_HD_ID int null,
                                                                      Metadata_HD_MHR int null,
                                                                      HD_MHR_HrDetails_MaxHr integer null,
                                                                      HD_MNH_HD_ID int null,
                                                                      Metadata_HD_MNH int null,
                                                                      HD_MNH_HrDetails_MinHardness double precision null,
                                                                      HD_AHR_HD_ID int null,
                                                                      Metadata_HD_AHR int null,
                                                                      HD_AHR_HrDetails_AvgHardness double precision null,
                                                                      HD_MXH_HD_ID int null,
                                                                      Metadata_HD_MXH int null,
                                                                      HD_MXH_HrDetails_MaxHardness double precision null,
                                                                      HD_HRD_HD_ID int null,
                                                                      Metadata_HD_HRD int null,
                                                                      HD_HRD_HrDetails_Device varchar(100)[] null
        ) ON COMMIT DROP;
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER itb_lHD_HrDetails
    BEFORE INSERT ON skllzz.lHD_HrDetails
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.itb_lHD_HrDetails();
-- INSTEAD OF INSERT trigger ----------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS iti_lHD_HrDetails ON skllzz.lHD_HrDetails;
--DROP FUNCTION IF EXISTS skllzz.iti_lHD_HrDetails();
CREATE OR REPLACE FUNCTION skllzz.iti_lHD_HrDetails() RETURNS trigger AS '
    BEGIN
        -- generate anchor ID (if not provided)
        IF (NEW.HD_ID IS NULL) THEN
            INSERT INTO skllzz.HD_HrDetails (
                Metadata_HD
            ) VALUES (
                         NEW.Metadata_HD
                     );
            SELECT
                lastval()
            INTO NEW.HD_ID;
            -- if anchor ID is provided then let''s insert it into the anchor table
            -- but only if that ID is not present in the anchor table
        ELSE
            INSERT INTO skllzz.HD_HrDetails (
                Metadata_HD,
                HD_ID
            )
            SELECT
                NEW.Metadata_HD,
                NEW.HD_ID
            WHERE NOT EXISTS(
                    SELECT
                        HD_ID
                    FROM skllzz.HD_HrDetails
                    WHERE HD_ID = NEW.HD_ID
                    LIMIT 1
                );
        END IF;
        -- insert row into temporary table
        INSERT INTO _tmp_it_HD_HrDetails (
            HD_ID,
            Metadata_HD,
            HD_HMN_HD_ID,
            Metadata_HD_HMN,
            HD_HMN_HrDetails_MinHr,
            HD_HAV_HD_ID,
            Metadata_HD_HAV,
            HD_HAV_HrDetails_AvgHr,
            HD_MHR_HD_ID,
            Metadata_HD_MHR,
            HD_MHR_HrDetails_MaxHr,
            HD_MNH_HD_ID,
            Metadata_HD_MNH,
            HD_MNH_HrDetails_MinHardness,
            HD_AHR_HD_ID,
            Metadata_HD_AHR,
            HD_AHR_HrDetails_AvgHardness,
            HD_MXH_HD_ID,
            Metadata_HD_MXH,
            HD_MXH_HrDetails_MaxHardness,
            HD_HRD_HD_ID,
            Metadata_HD_HRD,
            HD_HRD_HrDetails_Device
        ) VALUES (
                     NEW.HD_ID,
                     NEW.Metadata_HD,
                     COALESCE(NEW.HD_HMN_HD_ID, NEW.HD_ID),
                     COALESCE(NEW.Metadata_HD_HMN, NEW.Metadata_HD),
                     NEW.HD_HMN_HrDetails_MinHr,
                     COALESCE(NEW.HD_HAV_HD_ID, NEW.HD_ID),
                     COALESCE(NEW.Metadata_HD_HAV, NEW.Metadata_HD),
                     NEW.HD_HAV_HrDetails_AvgHr,
                     COALESCE(NEW.HD_MHR_HD_ID, NEW.HD_ID),
                     COALESCE(NEW.Metadata_HD_MHR, NEW.Metadata_HD),
                     NEW.HD_MHR_HrDetails_MaxHr,
                     COALESCE(NEW.HD_MNH_HD_ID, NEW.HD_ID),
                     COALESCE(NEW.Metadata_HD_MNH, NEW.Metadata_HD),
                     NEW.HD_MNH_HrDetails_MinHardness,
                     COALESCE(NEW.HD_AHR_HD_ID, NEW.HD_ID),
                     COALESCE(NEW.Metadata_HD_AHR, NEW.Metadata_HD),
                     NEW.HD_AHR_HrDetails_AvgHardness,
                     COALESCE(NEW.HD_MXH_HD_ID, NEW.HD_ID),
                     COALESCE(NEW.Metadata_HD_MXH, NEW.Metadata_HD),
                     NEW.HD_MXH_HrDetails_MaxHardness,
                     COALESCE(NEW.HD_HRD_HD_ID, NEW.HD_ID),
                     COALESCE(NEW.Metadata_HD_HRD, NEW.Metadata_HD),
                     NEW.HD_HRD_HrDetails_Device
                 );
        RETURN NEW;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER iti_lHD_HrDetails
    INSTEAD OF INSERT ON skllzz.lHD_HrDetails
    FOR EACH ROW
EXECUTE PROCEDURE skllzz.iti_lHD_HrDetails();
-- AFTER INSERT trigger ---------------------------------------------------------------------------------------------------------
--DROP TRIGGER IF EXISTS ita_lHD_HrDetails ON skllzz.lHD_HrDetails;
--DROP FUNCTION IF EXISTS skllzz.ita_lHD_HrDetails();
CREATE OR REPLACE FUNCTION skllzz.ita_lHD_HrDetails() RETURNS trigger AS '
    BEGIN
        INSERT INTO skllzz.HD_HMN_HrDetails_MinHr (
            HD_HMN_HD_ID,
            Metadata_HD_HMN,
            HD_HMN_HrDetails_MinHr
        )
        SELECT
            i.HD_HMN_HD_ID,
            i.Metadata_HD_HMN,
            i.HD_HMN_HrDetails_MinHr
        FROM
            _tmp_it_HD_HrDetails i
        WHERE
            i.HD_HMN_HrDetails_MinHr is not null;
        INSERT INTO skllzz.HD_HAV_HrDetails_AvgHr (
            HD_HAV_HD_ID,
            Metadata_HD_HAV,
            HD_HAV_HrDetails_AvgHr
        )
        SELECT
            i.HD_HAV_HD_ID,
            i.Metadata_HD_HAV,
            i.HD_HAV_HrDetails_AvgHr
        FROM
            _tmp_it_HD_HrDetails i
        WHERE
            i.HD_HAV_HrDetails_AvgHr is not null;
        INSERT INTO skllzz.HD_MHR_HrDetails_MaxHr (
            HD_MHR_HD_ID,
            Metadata_HD_MHR,
            HD_MHR_HrDetails_MaxHr
        )
        SELECT
            i.HD_MHR_HD_ID,
            i.Metadata_HD_MHR,
            i.HD_MHR_HrDetails_MaxHr
        FROM
            _tmp_it_HD_HrDetails i
        WHERE
            i.HD_MHR_HrDetails_MaxHr is not null;
        INSERT INTO skllzz.HD_MNH_HrDetails_MinHardness (
            HD_MNH_HD_ID,
            Metadata_HD_MNH,
            HD_MNH_HrDetails_MinHardness
        )
        SELECT
            i.HD_MNH_HD_ID,
            i.Metadata_HD_MNH,
            i.HD_MNH_HrDetails_MinHardness
        FROM
            _tmp_it_HD_HrDetails i
        WHERE
            i.HD_MNH_HrDetails_MinHardness is not null;
        INSERT INTO skllzz.HD_AHR_HrDetails_AvgHardness (
            HD_AHR_HD_ID,
            Metadata_HD_AHR,
            HD_AHR_HrDetails_AvgHardness
        )
        SELECT
            i.HD_AHR_HD_ID,
            i.Metadata_HD_AHR,
            i.HD_AHR_HrDetails_AvgHardness
        FROM
            _tmp_it_HD_HrDetails i
        WHERE
            i.HD_AHR_HrDetails_AvgHardness is not null;
        INSERT INTO skllzz.HD_MXH_HrDetails_MaxHardness (
            HD_MXH_HD_ID,
            Metadata_HD_MXH,
            HD_MXH_HrDetails_MaxHardness
        )
        SELECT
            i.HD_MXH_HD_ID,
            i.Metadata_HD_MXH,
            i.HD_MXH_HrDetails_MaxHardness
        FROM
            _tmp_it_HD_HrDetails i
        WHERE
            i.HD_MXH_HrDetails_MaxHardness is not null;
        INSERT INTO skllzz.HD_HRD_HrDetails_Device (
            HD_HRD_HD_ID,
            Metadata_HD_HRD,
            HD_HRD_HrDetails_Device
        )
        SELECT
            i.HD_HRD_HD_ID,
            i.Metadata_HD_HRD,
            i.HD_HRD_HrDetails_Device
        FROM
            _tmp_it_HD_HrDetails i
        WHERE
            i.HD_HRD_HrDetails_Device is not null;
        DROP TABLE IF EXISTS _tmp_it_HD_HrDetails;
        RETURN NULL;
    END;
' LANGUAGE plpgsql;
CREATE TRIGGER ita_lHD_HrDetails
    AFTER INSERT ON skllzz.lHD_HrDetails
    FOR EACH STATEMENT
EXECUTE PROCEDURE skllzz.ita_lHD_HrDetails();
-- TIE TEMPORAL PERSPECTIVES ------------------------------------------------------------------------------------------
--
-- These table valued functions simplify temporal querying by providing a temporal
-- perspective of each tie. There are four types of perspectives: latest,
-- point-in-time, difference, and now.
--
-- The latest perspective shows the latest available information for each tie.
-- The now perspective shows the information as it is right now.
-- The point-in-time perspective lets you travel through the information to the given timepoint.
--
-- changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints.
--
-- intervalStart the start of the interval for finding changes
-- intervalEnd the end of the interval for finding changes
--
-- Under equivalence all these views default to equivalent = 0, however, corresponding
-- prepended-e perspectives are provided in order to select a specific equivalent.
--
-- equivalent the equivalent for which to retrieve data
--
-- DROP TIE TEMPORAL PERSPECTIVES ----------------------------------------------------------------------------------
/*
DROP VIEW IF EXISTS skllzz.nTS_holds_SD_containedIn;
DROP FUNCTION IF EXISTS skllzz.pTS_holds_SD_containedIn(
    timestamp without time zone
);
DROP VIEW IF EXISTS skllzz.lTS_holds_SD_containedIn;
DROP VIEW IF EXISTS skllzz.nTS_holds_HD_containedIn;
DROP FUNCTION IF EXISTS skllzz.pTS_holds_HD_containedIn(
    timestamp without time zone
);
DROP VIEW IF EXISTS skllzz.lTS_holds_HD_containedIn;
*/
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lTS_holds_SD_containedIn viewed by the latest available information (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.lTS_holds_SD_containedIn AS
SELECT
    tie.Metadata_TS_holds_SD_containedIn,
    tie.TS_ID_holds,
    tie.SD_ID_containedIn
FROM
    skllzz.TS_holds_SD_containedIn tie;
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lTS_holds_HD_containedIn viewed by the latest available information (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.lTS_holds_HD_containedIn AS
SELECT
    tie.Metadata_TS_holds_HD_containedIn,
    tie.TS_ID_holds,
    tie.HD_ID_containedIn
FROM
    skllzz.TS_holds_HD_containedIn tie;
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pTS_holds_SD_containedIn viewed by the latest available information (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION skllzz.pTS_holds_SD_containedIn (
    changingTimepoint timestamp without time zone
)
    RETURNS TABLE (
                      Metadata_TS_holds_SD_containedIn int,
                      TS_ID_holds int,
                      SD_ID_containedIn int
                  ) AS '
    SELECT
        tie.Metadata_TS_holds_SD_containedIn,
        tie.TS_ID_holds,
        tie.SD_ID_containedIn
    FROM
        skllzz.TS_holds_SD_containedIn tie;
' LANGUAGE SQL;
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pTS_holds_HD_containedIn viewed by the latest available information (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION skllzz.pTS_holds_HD_containedIn (
    changingTimepoint timestamp without time zone
)
    RETURNS TABLE (
                      Metadata_TS_holds_HD_containedIn int,
                      TS_ID_holds int,
                      HD_ID_containedIn int
                  ) AS '
    SELECT
        tie.Metadata_TS_holds_HD_containedIn,
        tie.TS_ID_holds,
        tie.HD_ID_containedIn
    FROM
        skllzz.TS_holds_HD_containedIn tie;
' LANGUAGE SQL;
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nTS_holds_SD_containedIn viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.nTS_holds_SD_containedIn AS
SELECT
    *
FROM
    skllzz.pTS_holds_SD_containedIn(LOCALTIMESTAMP);
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nTS_holds_HD_containedIn viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW skllzz.nTS_holds_HD_containedIn AS
SELECT
    *
FROM
    skllzz.pTS_holds_HD_containedIn(LOCALTIMESTAMP);
-- DESCRIPTIONS -------------------------------------------------------------------------------------------------------
COMMENT ON TABLE
    skllzz._TS_TID_TrainingSession_Id
    IS 'Уникальный идентификатор сессии';
COMMENT ON TABLE
    skllzz._TS_TSR_TrainingSession_StartMillis
    IS 'Начало тренировки в миллисекундах от эпохи';
COMMENT ON TABLE
    skllzz._TS_TST_TrainingSession_StopMillis
    IS 'Конец тренировки в миллисекундах от эпохи';
COMMENT ON TABLE
    skllzz._TS_TSZ_TrainingSession_Skllzz
    IS 'Начислено баллов за тренировку';
COMMENT ON TABLE
    skllzz._TS_TDL_TrainingSession_Deleted
    IS 'Признак удаленной сессии';
COMMENT ON TABLE
    skllzz._TS_TSI_TrainingSession_SourceId
    IS 'Идентификатор источника (уникален для мобильного телефона)';
COMMENT ON TABLE
    skllzz._TS_TSV_TrainingSession_Version
    IS 'Версия набора полей данных';
COMMENT ON TABLE
    skllzz._TS_TTZ_TrainingSession_Timezone
    IS 'Часовой пояс сессии';
COMMENT ON TABLE
    skllzz._TS_TSY_TrainingSession_SyncMillis
    IS 'Время синхронизации';
COMMENT ON TABLE
    skllzz._TS_TKC_TrainingSession_KCal
    IS 'Сожжено килокалорий за тренировку';
COMMENT ON TABLE
    skllzz._TS_TPR_TrainingSession_ProfileId
    IS 'Идентификатор профиля которому принадлежит тренировка';
COMMENT ON TABLE
    skllzz._SD_SDS_StepDetails_Steps
    IS 'Суммарное количество шагов в этот день';
COMMENT ON TABLE
    skllzz._SD_SDD_StepDetails_Day
    IS 'День от начала эпохи за который считаются шаги';
COMMENT ON TABLE
    skllzz._SD_SDM_StepDetails_Meters
    IS 'Пройденное расстояние в метрах';
COMMENT ON TABLE
    skllzz._HD_HMN_HrDetails_MinHr
    IS 'Минимальный пульс';
COMMENT ON TABLE
    skllzz._HD_HAV_HrDetails_AvgHr
    IS 'Средний пульс';
COMMENT ON TABLE
    skllzz._HD_MHR_HrDetails_MaxHr
    IS 'Максимальный пульс';
COMMENT ON TABLE
    skllzz._HD_MNH_HrDetails_MinHardness
    IS 'Минимальная нагрузка (0,1)';
COMMENT ON TABLE
    skllzz._HD_AHR_HrDetails_AvgHardness
    IS 'Средняя нагрузка (0,1)';
COMMENT ON TABLE
    skllzz._HD_MXH_HrDetails_MaxHardness
    IS 'Максимальная нагрузка (0,1)';
COMMENT ON TABLE
    skllzz._HD_HRD_HrDetails_Device
    IS 'Список устройств с которых была получена тренировка';