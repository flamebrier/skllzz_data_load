insert into skllzz._td_trainingdata (td_id, metadata_td)
values (nextval('skllzz.td_sequence'), 0);
insert into skllzz._td_dnt_trainingdata_devicename (td_dnt_td_id, td_dnt_trainingdata_devicename, metadata_td_dnt)
values (currval('skllzz.td_sequence'), @deviceName, 0);
insert into skllzz._td_dur_trainingdata_duration (td_dur_td_id, td_dur_trainingdata_duration, metadata_td_dur)
values (currval('skllzz.td_sequence'), @duration, 0);
insert into skllzz._td_dvt_trainingdata_deviceid (td_dvt_td_id, td_dvt_trainingdata_deviceid, metadata_td_dvt)
values (currval('skllzz.td_sequence'), @deviceId, 0);
insert into skllzz._td_hra_trainingdata_hravg (td_hra_td_id, td_hra_trainingdata_hravg, metadata_td_hra)
values (currval('skllzz.td_sequence'), @hrAvg, 0);
insert into skllzz._td_hrd_trainingdata_hardness (td_hrd_td_id, td_hrd_trainingdata_hardness, metadata_td_hrd)
values (currval('skllzz.td_sequence'), @hardness, 0);
insert into skllzz._td_pid_trainingdata_profileid (td_pid_td_id, td_pid_trainingdata_profileid, metadata_td_pid)
values (currval('skllzz.td_sequence'), @profileId, 0);
insert into skllzz._td_slz_trainingdata_skllzz (td_slz_td_id, td_slz_trainingdata_skllzz, metadata_td_slz)
values (currval('skllzz.td_sequence'), @skllzz, 0);
insert into skllzz._td_smt_trainingdata_stampmillis (td_smt_td_id, td_smt_trainingdata_stampmillis, metadata_td_smt)
values (currval('skllzz.td_sequence'), @stampMillis, 0);
insert into skllzz._td_tid_trainingdata_sessionid (td_tid_td_id, td_tid_trainingdata_sessionid, metadata_td_tid)
values (currval('skllzz.td_sequence'), @sessionId, 0);
insert into skllzz._td_tsy_trainingdata_syncmillis (td_tsy_td_id, td_tsy_trainingdata_syncmillis, metadata_td_tsy)
values (currval('skllzz.td_sequence'), @syncMillis, 0);