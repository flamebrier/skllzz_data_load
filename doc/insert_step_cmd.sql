insert into skllzz._st_stepsdata (st_id, metadata_st)
values (nextval('skllzz.st_sequence'), 0);
insert into skllzz._st_drs_stepsdata_duration (st_drs_st_id, st_drs_stepsdata_duration, metadata_st_drs)
values (currval('skllzz.st_sequence'), @duration, 0);
insert into skllzz._st_sps_stepsdata_steps (st_sps_st_id, st_sps_stepsdata_steps, metadata_st_sps)
values (currval('skllzz.st_sequence'), @steps, 0);
insert into skllzz._st_src_stepsdata_stepsource (st_src_st_id, st_src_stepsdata_stepsource, metadata_st_src)
values (currval('skllzz.st_sequence'), @source, 0);
insert into skllzz._st_srs_stepsdata_sourceid (st_srs_st_id, st_srs_stepsdata_sourceid, metadata_st_srs)
values (currval('skllzz.st_sequence'), @sourceId, 0);
insert into skllzz._st_ssd_stepsdata_syncmillis (st_ssd_st_id, st_ssd_stepsdata_syncmillis, metadata_st_ssd)
values (currval('skllzz.st_sequence'), @syncMillis, 0);
insert into skllzz._st_sts_stepsdata_stampmillis (st_sts_st_id, st_sts_stepsdata_stampmillis, metadata_st_sts)
values (currval('skllzz.st_sequence'), @stampMillis, 0);