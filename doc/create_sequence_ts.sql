CREATE SEQUENCE IF NOT EXISTS skllzz.hd_sequence INCREMENT BY 1
    START WITH 1 OWNED BY skllzz._hd_hrdetails.hd_id;

CREATE SEQUENCE IF NOT EXISTS skllzz.ts_sequence INCREMENT BY 1
    START WITH 1 OWNED BY skllzz._ts_trainingsession.ts_id;

CREATE SEQUENCE IF NOT EXISTS skllzz.sd_sequence INCREMENT BY 1
    START WITH 1 OWNED BY skllzz._sd_stepdetails.sd_id;