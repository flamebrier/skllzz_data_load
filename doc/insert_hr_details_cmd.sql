INSERT into skllzz._hd_hrdetails (hd_id, metadata_hd)
values (nextval('skllzz.hd_sequence'), 0);
INSERT into skllzz._hd_hmn_hrdetails_minhr (hd_hmn_hd_id, hd_hmn_hrdetails_minhr, metadata_hd_hmn)
values (currval('skllzz.hd_sequence'), @minHr, 0);
INSERT into skllzz._hd_hav_hrdetails_avghr (hd_hav_hd_id, hd_hav_hrdetails_avghr, metadata_hd_hav)
values (currval('skllzz.hd_sequence'), @avgHr, 0);
INSERT into skllzz._hd_mhr_hrdetails_maxhr (hd_mhr_hd_id, hd_mhr_hrdetails_maxhr, metadata_hd_mhr)
values (currval('skllzz.hd_sequence'), @maxHr, 0);
INSERT into skllzz._hd_mnh_hrdetails_minhardness (hd_mnh_hd_id, hd_mnh_hrdetails_minhardness, metadata_hd_mnh)
values (currval('skllzz.hd_sequence'), @minHardness, 0);
INSERT into skllzz._hd_mxh_hrdetails_maxhardness (hd_mxh_hd_id, hd_mxh_hrdetails_maxhardness, metadata_hd_mxh)
values (currval('skllzz.hd_sequence'), @maxHardness, 0);
INSERT into skllzz._hd_ahr_hrdetails_avghardness (hd_ahr_hd_id, hd_ahr_hrdetails_avghardness, metadata_hd_ahr)
values (currval('skllzz.hd_sequence'), @avgHardness, 0);
INSERT into skllzz._hd_hrd_hrdetails_device (hd_hrd_hd_id, hd_hrd_hrdetails_device, metadata_hd_hrd)
values (currval('skllzz.hd_sequence'), @device, 0);