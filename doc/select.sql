SELECT ts_id,
       ts_tid_trainingsession_id,
       ts_tkc_trainingsession_kcal,
       ts_tpr_trainingsession_profileid,
       ts_tsi_trainingsession_sourceid,
       ts_tsr_trainingsession_startmillis,
       ts_tst_trainingsession_stopmillis,
       ts_tsy_trainingsession_syncmillis,
       ts_tsz_trainingsession_skllzz,
       ts_ttz_trainingsession_timezone
from skllzz._ts_trainingsession
         JOIN skllzz._ts_tid_trainingsession_id ON ts_tid_ts_id = ts_id
         JOIN skllzz._ts_tkc_trainingsession_kcal ON ts_tkc_ts_id = ts_id
         JOIN skllzz._ts_tpr_trainingsession_profileid ON ts_tpr_ts_id = ts_id
         JOIN skllzz._ts_tsi_trainingsession_sourceid ON ts_tsi_ts_id = ts_id
         JOIN skllzz._ts_tsr_trainingsession_startmillis ON ts_tsr_ts_id = ts_id
         JOIN skllzz._ts_tst_trainingsession_stopmillis ON ts_tst_ts_id = ts_id
         JOIN skllzz._ts_tsy_trainingsession_syncmillis ON ts_tsy_ts_id = ts_id
         JOIN skllzz._ts_tsz_trainingsession_skllzz ON ts_tsz_ts_id = ts_id
         JOIN skllzz._ts_ttz_trainingsession_timezone ON ts_ttz_ts_id = ts_id;

create schema  if not exists olap;

create table if not exists olap.facts
(
    time_key integer,
    steps    integer,
    skllzz   integer,
    PRIMARY KEY (time_key)
);

create table if not exists olap.dimensions
(
    profileKey varchar(100),
    time_key   integer,
    steps      integer,
    skllzz     integer,
    timezone   varchar(100),
    PRIMARY KEY (profileKey, time_key)
);

insert into olap.dimensions (profileKey, time_key, steps, skllzz, timezone)
SELECT min(ts_tpr_trainingsession_profileid),
       (_ts_tsr_trainingsession_startmillis.ts_tsr_trainingsession_startmillis /
        1000000000::bigint)::integer as time_key,
       sum(sd_sds_stepdetails_steps),
       sum(ts_tsz_trainingsession_skllzz),
       min(ts_ttz_trainingsession_timezone)
from skllzz._ts_trainingsession tt
         join skllzz._ts_tpr_trainingsession_profileid on tt.ts_id = skllzz._ts_tpr_trainingsession_profileid.ts_tpr_ts_id
         join skllzz._ts_tsr_trainingsession_startmillis on tt.ts_id = skllzz._ts_tsr_trainingsession_startmillis.ts_tsr_ts_id
         join skllzz._sd_sds_stepdetails_steps on tt.ts_id = skllzz._sd_sds_stepdetails_steps.sd_sds_sd_id
         join skllzz._ts_tsz_trainingsession_skllzz on tt.ts_id = skllzz._ts_tsz_trainingsession_skllzz.ts_tsz_ts_id
         join skllzz._ts_ttz_trainingsession_timezone on tt.ts_id = skllzz._ts_ttz_trainingsession_timezone.ts_ttz_ts_id
group by time_key;

insert into olap.facts (time_key, steps, skllzz)
SELECT (_ts_tsr_trainingsession_startmillis.ts_tsr_trainingsession_startmillis /
        1000000000::bigint)::integer      as time_key,
       sum(sd_sds_stepdetails_steps)      as steps,
       sum(ts_tsz_trainingsession_skllzz) as skllzz
from skllzz._ts_trainingsession tt
         join skllzz._ts_tsr_trainingsession_startmillis on tt.ts_id = skllzz._ts_tsr_trainingsession_startmillis.ts_tsr_ts_id
         join skllzz._ts_tsz_trainingsession_skllzz on tt.ts_id = skllzz._ts_tsz_trainingsession_skllzz.ts_tsz_ts_id
         join skllzz._sd_sds_stepdetails_steps on tt.ts_id = skllzz._sd_sds_stepdetails_steps.sd_sds_sd_id
group by time_key;

CREATE VIEW olap.factsView AS
SELECT (_ts_tsr_trainingsession_startmillis.ts_tsr_trainingsession_startmillis /
        1000000000::bigint)::integer      as time_key,
       sum(sd_sds_stepdetails_steps)      as steps,
       sum(ts_tsz_trainingsession_skllzz) as skllzz
from _ts_trainingsession tt
         join _ts_tsr_trainingsession_startmillis on tt.ts_id = _ts_tsr_trainingsession_startmillis.ts_tsr_ts_id
         join _ts_tsz_trainingsession_skllzz on tt.ts_id = _ts_tsz_trainingsession_skllzz.ts_tsz_ts_id
         join _sd_sds_stepdetails_steps on tt.ts_id = _sd_sds_stepdetails_steps.sd_sds_sd_id
group by time_key;

SELECT min(facts.time_key), sum(facts.skllzz), dimensions.timezone
from olap.facts
         join olap.dimensions on dimensions.time_key = facts.time_key
where facts.time_key > 1300
group by dimensions.timezone
order by min(facts.time_key);

SELECT sum(facts.skllzz)
from olap.facts
         join olap.dimensions on dimensions.time_key = facts.time_key
where timezone = 'Europe/Moscow';

SELECT sum(facts.steps)
from olap.facts
         join olap.dimensions on dimensions.time_key = facts.time_key;

SELECT ts_tpr_trainingsession_profileid,
       ts_tsr_trainingsession_startmillis,
       sd_sds_stepdetails_steps,
       ts_tsz_trainingsession_skllzz,
       ts_ttz_trainingsession_timezone
from _ts_trainingsession tt
         join _ts_tpr_trainingsession_profileid on tt.ts_id = _ts_tpr_trainingsession_profileid.ts_tpr_ts_id
         join _ts_tsr_trainingsession_startmillis on tt.ts_id = _ts_tsr_trainingsession_startmillis.ts_tsr_ts_id
         join _sd_sds_stepdetails_steps on tt.ts_id = _sd_sds_stepdetails_steps.sd_sds_sd_id
         join _ts_tsz_trainingsession_skllzz on tt.ts_id = _ts_tsz_trainingsession_skllzz.ts_tsz_ts_id
         join _ts_ttz_trainingsession_timezone on tt.ts_id = _ts_ttz_trainingsession_timezone.ts_ttz_ts_id;

SELECT sum(_ts_tsz_trainingsession_skllzz.ts_tsz_trainingsession_skllzz)
FROM skllzz._ts_tsz_trainingsession_skllzz
         JOIN skllzz._ts_tsr_trainingsession_startmillis
              ON _ts_tsz_trainingsession_skllzz.ts_tsz_ts_id = _ts_tsr_trainingsession_startmillis.ts_tsr_ts_id
group by (_ts_tsr_trainingsession_startmillis.ts_tsr_trainingsession_startmillis / 1000000000::bigint)::integer;
SELECT COUNT(*)
FROM skllzz._hd_hrdetails;
SELECT COUNT(*)
FROM skllzz._sd_stepdetails;