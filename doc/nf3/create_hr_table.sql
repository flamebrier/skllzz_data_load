CREATE TABLE IF NOT EXISTS hr_details
(
    session_id      varchar(100),
    profile_id      varchar(100),
    min_hr          integer,
    avg_hr          integer,
    max_hr          integer,
    min_hardness    double precision,
    avg_hardness    double precision,
    max_hardness    double precision,
    PRIMARY KEY (session_id, profile_id),
    FOREIGN KEY (session_id, profile_id) REFERENCES training_session(id, profile_id)
);