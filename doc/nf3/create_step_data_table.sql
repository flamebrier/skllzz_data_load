CREATE TABLE IF NOT EXISTS step_data
(
    step_day        integer     NOT NULL    REFERENCES step_details(step_day),
    profile_id      varchar(100)    NOT NULL    REFERENCES training_session(profile_id),
    stamp           timestamp       NOT NULL,
    steps           integer         NOT NULL,
    PRIMARY KEY (step_day, profile_id, stamp)
);