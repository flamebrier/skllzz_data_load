CREATE TABLE IF NOT EXISTS training_data
(
    session_id      varchar(100)         REFERENCES training_session(id),
    stamp           timestamp,
    hr_avg          integer,
    profile_id      varchar(100)         REFERENCES training_session(profile_id),
    PRIMARY KEY (session_id, stamp)
);