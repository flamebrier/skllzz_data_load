CREATE TABLE IF NOT EXISTS training_session
(
    id              varchar(100)         NOT NULL,
    start           timestamp,
    stop            timestamp,
    skllzz          double precision,
    kkal            double precision,
    profile_id      varchar(100)         NOT NULL,
    timezone        varchar(100),
    source          varchar(100),
    PRIMARY KEY (id, profile_id)
);