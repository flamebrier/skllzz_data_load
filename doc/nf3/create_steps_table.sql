CREATE TABLE IF NOT EXISTS step_details
(
    session_id      varchar(100),
    profile_id      varchar(100),
    steps           integer,
    step_day        integer,
    meters          integer,
    PRIMARY KEY (session_id, profile_id),
    FOREIGN KEY (session_id, profile_id) REFERENCES training_session(id, profile_id)
);