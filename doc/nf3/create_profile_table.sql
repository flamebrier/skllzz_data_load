CREATE TABLE IF NOT EXISTS profile
(
    id              varchar(100)         PRIMARY KEY,
    name            varchar(100),
    nickname        varchar(100),
    birth_date      date,
    hr_max          integer,
    isFemale        boolean,
    weight          double precision,
    join_stamp      timestamp,
    lang            varchar(2),
    last_sync       timestamp
);