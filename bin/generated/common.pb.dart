///
//  Generated code. Do not modify.
//  source: skllzz/common/common.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'geo.pb.dart' as $13;
import 'artifact.pb.dart' as $11;

import 'common.pbenum.dart';

export 'common.pbenum.dart';

class Link extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Link', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'url')
    ..hasRequiredFields = false
  ;

  Link._() : super();
  factory Link({
    $core.String? url,
  }) {
    final _result = create();
    if (url != null) {
      _result.url = url;
    }
    return _result;
  }
  factory Link.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Link.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Link clone() => Link()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Link copyWith(void Function(Link) updates) => super.copyWith((message) => updates(message as Link)) as Link; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Link create() => Link._();
  Link createEmptyInstance() => create();
  static $pb.PbList<Link> createRepeated() => $pb.PbList<Link>();
  @$core.pragma('dart2js:noInline')
  static Link getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Link>(create);
  static Link? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get url => $_getSZ(0);
  @$pb.TagNumber(1)
  set url($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUrl() => $_has(0);
  @$pb.TagNumber(1)
  void clearUrl() => clearField(1);
}

class AbsoluteOffer extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AbsoluteOffer', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'min', $pb.PbFieldType.OF)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'max', $pb.PbFieldType.OF)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minOrderAmount', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  AbsoluteOffer._() : super();
  factory AbsoluteOffer({
    $core.double? min,
    $core.double? max,
    $core.String? currency,
    $core.double? minOrderAmount,
  }) {
    final _result = create();
    if (min != null) {
      _result.min = min;
    }
    if (max != null) {
      _result.max = max;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    if (minOrderAmount != null) {
      _result.minOrderAmount = minOrderAmount;
    }
    return _result;
  }
  factory AbsoluteOffer.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AbsoluteOffer.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AbsoluteOffer clone() => AbsoluteOffer()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AbsoluteOffer copyWith(void Function(AbsoluteOffer) updates) => super.copyWith((message) => updates(message as AbsoluteOffer)) as AbsoluteOffer; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AbsoluteOffer create() => AbsoluteOffer._();
  AbsoluteOffer createEmptyInstance() => create();
  static $pb.PbList<AbsoluteOffer> createRepeated() => $pb.PbList<AbsoluteOffer>();
  @$core.pragma('dart2js:noInline')
  static AbsoluteOffer getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AbsoluteOffer>(create);
  static AbsoluteOffer? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get min => $_getN(0);
  @$pb.TagNumber(1)
  set min($core.double v) { $_setFloat(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMin() => $_has(0);
  @$pb.TagNumber(1)
  void clearMin() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get max => $_getN(1);
  @$pb.TagNumber(2)
  set max($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMax() => $_has(1);
  @$pb.TagNumber(2)
  void clearMax() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get currency => $_getSZ(2);
  @$pb.TagNumber(3)
  set currency($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCurrency() => $_has(2);
  @$pb.TagNumber(3)
  void clearCurrency() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get minOrderAmount => $_getN(3);
  @$pb.TagNumber(4)
  set minOrderAmount($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMinOrderAmount() => $_has(3);
  @$pb.TagNumber(4)
  void clearMinOrderAmount() => clearField(4);
}

class RelativeOffer extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RelativeOffer', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'min', $pb.PbFieldType.OF)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'max', $pb.PbFieldType.OF)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minOrderAmount', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  RelativeOffer._() : super();
  factory RelativeOffer({
    $core.double? min,
    $core.double? max,
    $core.String? currency,
    $core.double? minOrderAmount,
  }) {
    final _result = create();
    if (min != null) {
      _result.min = min;
    }
    if (max != null) {
      _result.max = max;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    if (minOrderAmount != null) {
      _result.minOrderAmount = minOrderAmount;
    }
    return _result;
  }
  factory RelativeOffer.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RelativeOffer.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RelativeOffer clone() => RelativeOffer()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RelativeOffer copyWith(void Function(RelativeOffer) updates) => super.copyWith((message) => updates(message as RelativeOffer)) as RelativeOffer; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RelativeOffer create() => RelativeOffer._();
  RelativeOffer createEmptyInstance() => create();
  static $pb.PbList<RelativeOffer> createRepeated() => $pb.PbList<RelativeOffer>();
  @$core.pragma('dart2js:noInline')
  static RelativeOffer getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RelativeOffer>(create);
  static RelativeOffer? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get min => $_getN(0);
  @$pb.TagNumber(1)
  set min($core.double v) { $_setFloat(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMin() => $_has(0);
  @$pb.TagNumber(1)
  void clearMin() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get max => $_getN(1);
  @$pb.TagNumber(2)
  set max($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMax() => $_has(1);
  @$pb.TagNumber(2)
  void clearMax() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get currency => $_getSZ(2);
  @$pb.TagNumber(3)
  set currency($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCurrency() => $_has(2);
  @$pb.TagNumber(3)
  void clearCurrency() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get minOrderAmount => $_getN(3);
  @$pb.TagNumber(4)
  set minOrderAmount($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMinOrderAmount() => $_has(3);
  @$pb.TagNumber(4)
  void clearMinOrderAmount() => clearField(4);
}

class OnlineDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OnlineDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'price', $pb.PbFieldType.OD)
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offer')
    ..aOS(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coverageId')
    ..aOS(21, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coverageName')
    ..aOS(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'redirectUri')
    ..aOB(40, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reusable')
    ..aOB(50, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hiddenCode')
    ..hasRequiredFields = false
  ;

  OnlineDetails._() : super();
  factory OnlineDetails({
    $core.double? price,
    $core.String? offer,
    $core.String? coverageId,
    $core.String? coverageName,
    $core.String? redirectUri,
    $core.bool? reusable,
    $core.bool? hiddenCode,
  }) {
    final _result = create();
    if (price != null) {
      _result.price = price;
    }
    if (offer != null) {
      _result.offer = offer;
    }
    if (coverageId != null) {
      _result.coverageId = coverageId;
    }
    if (coverageName != null) {
      _result.coverageName = coverageName;
    }
    if (redirectUri != null) {
      _result.redirectUri = redirectUri;
    }
    if (reusable != null) {
      _result.reusable = reusable;
    }
    if (hiddenCode != null) {
      _result.hiddenCode = hiddenCode;
    }
    return _result;
  }
  factory OnlineDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OnlineDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OnlineDetails clone() => OnlineDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OnlineDetails copyWith(void Function(OnlineDetails) updates) => super.copyWith((message) => updates(message as OnlineDetails)) as OnlineDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OnlineDetails create() => OnlineDetails._();
  OnlineDetails createEmptyInstance() => create();
  static $pb.PbList<OnlineDetails> createRepeated() => $pb.PbList<OnlineDetails>();
  @$core.pragma('dart2js:noInline')
  static OnlineDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OnlineDetails>(create);
  static OnlineDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get price => $_getN(0);
  @$pb.TagNumber(1)
  set price($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPrice() => $_has(0);
  @$pb.TagNumber(1)
  void clearPrice() => clearField(1);

  @$pb.TagNumber(10)
  $core.String get offer => $_getSZ(1);
  @$pb.TagNumber(10)
  set offer($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(10)
  $core.bool hasOffer() => $_has(1);
  @$pb.TagNumber(10)
  void clearOffer() => clearField(10);

  @$pb.TagNumber(20)
  $core.String get coverageId => $_getSZ(2);
  @$pb.TagNumber(20)
  set coverageId($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(20)
  $core.bool hasCoverageId() => $_has(2);
  @$pb.TagNumber(20)
  void clearCoverageId() => clearField(20);

  @$pb.TagNumber(21)
  $core.String get coverageName => $_getSZ(3);
  @$pb.TagNumber(21)
  set coverageName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(21)
  $core.bool hasCoverageName() => $_has(3);
  @$pb.TagNumber(21)
  void clearCoverageName() => clearField(21);

  @$pb.TagNumber(30)
  $core.String get redirectUri => $_getSZ(4);
  @$pb.TagNumber(30)
  set redirectUri($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(30)
  $core.bool hasRedirectUri() => $_has(4);
  @$pb.TagNumber(30)
  void clearRedirectUri() => clearField(30);

  @$pb.TagNumber(40)
  $core.bool get reusable => $_getBF(5);
  @$pb.TagNumber(40)
  set reusable($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(40)
  $core.bool hasReusable() => $_has(5);
  @$pb.TagNumber(40)
  void clearReusable() => clearField(40);

  @$pb.TagNumber(50)
  $core.bool get hiddenCode => $_getBF(6);
  @$pb.TagNumber(50)
  set hiddenCode($core.bool v) { $_setBool(6, v); }
  @$pb.TagNumber(50)
  $core.bool hasHiddenCode() => $_has(6);
  @$pb.TagNumber(50)
  void clearHiddenCode() => clearField(50);
}

class OfflineDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OfflineDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..aOM<$13.LatLng>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'location', subBuilder: $13.LatLng.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'qrLink')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'posId')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'licenseId')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offer')
    ..hasRequiredFields = false
  ;

  OfflineDetails._() : super();
  factory OfflineDetails({
    $core.String? address,
    $13.LatLng? location,
    $core.String? qrLink,
    $core.String? posId,
    $core.String? licenseId,
    $core.String? offer,
  }) {
    final _result = create();
    if (address != null) {
      _result.address = address;
    }
    if (location != null) {
      _result.location = location;
    }
    if (qrLink != null) {
      _result.qrLink = qrLink;
    }
    if (posId != null) {
      _result.posId = posId;
    }
    if (licenseId != null) {
      _result.licenseId = licenseId;
    }
    if (offer != null) {
      _result.offer = offer;
    }
    return _result;
  }
  factory OfflineDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OfflineDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OfflineDetails clone() => OfflineDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OfflineDetails copyWith(void Function(OfflineDetails) updates) => super.copyWith((message) => updates(message as OfflineDetails)) as OfflineDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OfflineDetails create() => OfflineDetails._();
  OfflineDetails createEmptyInstance() => create();
  static $pb.PbList<OfflineDetails> createRepeated() => $pb.PbList<OfflineDetails>();
  @$core.pragma('dart2js:noInline')
  static OfflineDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OfflineDetails>(create);
  static OfflineDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get address => $_getSZ(0);
  @$pb.TagNumber(1)
  set address($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAddress() => $_has(0);
  @$pb.TagNumber(1)
  void clearAddress() => clearField(1);

  @$pb.TagNumber(2)
  $13.LatLng get location => $_getN(1);
  @$pb.TagNumber(2)
  set location($13.LatLng v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasLocation() => $_has(1);
  @$pb.TagNumber(2)
  void clearLocation() => clearField(2);
  @$pb.TagNumber(2)
  $13.LatLng ensureLocation() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get qrLink => $_getSZ(2);
  @$pb.TagNumber(3)
  set qrLink($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasQrLink() => $_has(2);
  @$pb.TagNumber(3)
  void clearQrLink() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get posId => $_getSZ(3);
  @$pb.TagNumber(4)
  set posId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPosId() => $_has(3);
  @$pb.TagNumber(4)
  void clearPosId() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get licenseId => $_getSZ(4);
  @$pb.TagNumber(5)
  set licenseId($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLicenseId() => $_has(4);
  @$pb.TagNumber(5)
  void clearLicenseId() => clearField(5);

  @$pb.TagNumber(10)
  $core.String get offer => $_getSZ(5);
  @$pb.TagNumber(10)
  set offer($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(10)
  $core.bool hasOffer() => $_has(5);
  @$pb.TagNumber(10)
  void clearOffer() => clearField(10);
}

class PromoCodes extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PromoCodes', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'posId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'codes')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validUntilEpoch')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validFromEpoch')
    ..hasRequiredFields = false
  ;

  PromoCodes._() : super();
  factory PromoCodes({
    $core.String? posId,
    $core.String? codes,
    $fixnum.Int64? validUntilEpoch,
    $fixnum.Int64? validFromEpoch,
  }) {
    final _result = create();
    if (posId != null) {
      _result.posId = posId;
    }
    if (codes != null) {
      _result.codes = codes;
    }
    if (validUntilEpoch != null) {
      _result.validUntilEpoch = validUntilEpoch;
    }
    if (validFromEpoch != null) {
      _result.validFromEpoch = validFromEpoch;
    }
    return _result;
  }
  factory PromoCodes.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PromoCodes.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PromoCodes clone() => PromoCodes()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PromoCodes copyWith(void Function(PromoCodes) updates) => super.copyWith((message) => updates(message as PromoCodes)) as PromoCodes; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PromoCodes create() => PromoCodes._();
  PromoCodes createEmptyInstance() => create();
  static $pb.PbList<PromoCodes> createRepeated() => $pb.PbList<PromoCodes>();
  @$core.pragma('dart2js:noInline')
  static PromoCodes getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PromoCodes>(create);
  static PromoCodes? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get posId => $_getSZ(0);
  @$pb.TagNumber(1)
  set posId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPosId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPosId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get codes => $_getSZ(1);
  @$pb.TagNumber(2)
  set codes($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCodes() => $_has(1);
  @$pb.TagNumber(2)
  void clearCodes() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get validUntilEpoch => $_getI64(2);
  @$pb.TagNumber(3)
  set validUntilEpoch($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasValidUntilEpoch() => $_has(2);
  @$pb.TagNumber(3)
  void clearValidUntilEpoch() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get validFromEpoch => $_getI64(3);
  @$pb.TagNumber(4)
  set validFromEpoch($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasValidFromEpoch() => $_has(3);
  @$pb.TagNumber(4)
  void clearValidFromEpoch() => clearField(4);
}

class PromoCode extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PromoCode', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'code')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validUntilEpoch')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validFromEpoch')
    ..hasRequiredFields = false
  ;

  PromoCode._() : super();
  factory PromoCode({
    $core.String? code,
    $fixnum.Int64? validUntilEpoch,
    $fixnum.Int64? validFromEpoch,
  }) {
    final _result = create();
    if (code != null) {
      _result.code = code;
    }
    if (validUntilEpoch != null) {
      _result.validUntilEpoch = validUntilEpoch;
    }
    if (validFromEpoch != null) {
      _result.validFromEpoch = validFromEpoch;
    }
    return _result;
  }
  factory PromoCode.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PromoCode.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PromoCode clone() => PromoCode()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PromoCode copyWith(void Function(PromoCode) updates) => super.copyWith((message) => updates(message as PromoCode)) as PromoCode; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PromoCode create() => PromoCode._();
  PromoCode createEmptyInstance() => create();
  static $pb.PbList<PromoCode> createRepeated() => $pb.PbList<PromoCode>();
  @$core.pragma('dart2js:noInline')
  static PromoCode getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PromoCode>(create);
  static PromoCode? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get code => $_getSZ(0);
  @$pb.TagNumber(1)
  set code($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCode() => $_has(0);
  @$pb.TagNumber(1)
  void clearCode() => clearField(1);

  @$pb.TagNumber(3)
  $fixnum.Int64 get validUntilEpoch => $_getI64(1);
  @$pb.TagNumber(3)
  set validUntilEpoch($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(3)
  $core.bool hasValidUntilEpoch() => $_has(1);
  @$pb.TagNumber(3)
  void clearValidUntilEpoch() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get validFromEpoch => $_getI64(2);
  @$pb.TagNumber(4)
  set validFromEpoch($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(4)
  $core.bool hasValidFromEpoch() => $_has(2);
  @$pb.TagNumber(4)
  void clearValidFromEpoch() => clearField(4);
}

class Branding extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Branding', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'backgroundColor')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'primaryColor')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'secondaryColor')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textColor')
    ..hasRequiredFields = false
  ;

  Branding._() : super();
  factory Branding({
    $core.String? backgroundColor,
    $core.String? primaryColor,
    $core.String? secondaryColor,
    $core.String? textColor,
  }) {
    final _result = create();
    if (backgroundColor != null) {
      _result.backgroundColor = backgroundColor;
    }
    if (primaryColor != null) {
      _result.primaryColor = primaryColor;
    }
    if (secondaryColor != null) {
      _result.secondaryColor = secondaryColor;
    }
    if (textColor != null) {
      _result.textColor = textColor;
    }
    return _result;
  }
  factory Branding.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Branding.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Branding clone() => Branding()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Branding copyWith(void Function(Branding) updates) => super.copyWith((message) => updates(message as Branding)) as Branding; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Branding create() => Branding._();
  Branding createEmptyInstance() => create();
  static $pb.PbList<Branding> createRepeated() => $pb.PbList<Branding>();
  @$core.pragma('dart2js:noInline')
  static Branding getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Branding>(create);
  static Branding? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get backgroundColor => $_getSZ(0);
  @$pb.TagNumber(1)
  set backgroundColor($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasBackgroundColor() => $_has(0);
  @$pb.TagNumber(1)
  void clearBackgroundColor() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get primaryColor => $_getSZ(1);
  @$pb.TagNumber(2)
  set primaryColor($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPrimaryColor() => $_has(1);
  @$pb.TagNumber(2)
  void clearPrimaryColor() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get secondaryColor => $_getSZ(2);
  @$pb.TagNumber(3)
  set secondaryColor($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSecondaryColor() => $_has(2);
  @$pb.TagNumber(3)
  void clearSecondaryColor() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get textColor => $_getSZ(3);
  @$pb.TagNumber(4)
  set textColor($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTextColor() => $_has(3);
  @$pb.TagNumber(4)
  void clearTextColor() => clearField(4);
}

class StepDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StepDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'steps', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'day', $pb.PbFieldType.OU3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'meters', $pb.PbFieldType.OU3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'movementFactor', $pb.PbFieldType.OU3)
    ..pc<StepsData>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'samples', $pb.PbFieldType.PM, subBuilder: StepsData.create)
    ..hasRequiredFields = false
  ;

  StepDetails._() : super();
  factory StepDetails({
    $core.int? steps,
    $core.int? day,
    $core.int? meters,
    $core.int? movementFactor,
    $core.Iterable<StepsData>? samples,
  }) {
    final _result = create();
    if (steps != null) {
      _result.steps = steps;
    }
    if (day != null) {
      _result.day = day;
    }
    if (meters != null) {
      _result.meters = meters;
    }
    if (movementFactor != null) {
      _result.movementFactor = movementFactor;
    }
    if (samples != null) {
      _result.samples.addAll(samples);
    }
    return _result;
  }
  factory StepDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StepDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StepDetails clone() => StepDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StepDetails copyWith(void Function(StepDetails) updates) => super.copyWith((message) => updates(message as StepDetails)) as StepDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StepDetails create() => StepDetails._();
  StepDetails createEmptyInstance() => create();
  static $pb.PbList<StepDetails> createRepeated() => $pb.PbList<StepDetails>();
  @$core.pragma('dart2js:noInline')
  static StepDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StepDetails>(create);
  static StepDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get steps => $_getIZ(0);
  @$pb.TagNumber(1)
  set steps($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSteps() => $_has(0);
  @$pb.TagNumber(1)
  void clearSteps() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get day => $_getIZ(1);
  @$pb.TagNumber(2)
  set day($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDay() => $_has(1);
  @$pb.TagNumber(2)
  void clearDay() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get meters => $_getIZ(2);
  @$pb.TagNumber(3)
  set meters($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMeters() => $_has(2);
  @$pb.TagNumber(3)
  void clearMeters() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get movementFactor => $_getIZ(3);
  @$pb.TagNumber(4)
  set movementFactor($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMovementFactor() => $_has(3);
  @$pb.TagNumber(4)
  void clearMovementFactor() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<StepsData> get samples => $_getList(4);
}

class HrDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'HrDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minHr', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avgHr', $pb.PbFieldType.OU3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxHr', $pb.PbFieldType.OU3)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minHardness', $pb.PbFieldType.OD)
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avgHardness', $pb.PbFieldType.OD)
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxHardness', $pb.PbFieldType.OD)
    ..aOM<Profile>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'profile', subBuilder: Profile.create)
    ..pPS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'device')
    ..pc<TrainingData>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'samples', $pb.PbFieldType.PM, subBuilder: TrainingData.create)
    ..hasRequiredFields = false
  ;

  HrDetails._() : super();
  factory HrDetails({
    $core.int? minHr,
    $core.int? avgHr,
    $core.int? maxHr,
    $core.double? minHardness,
    $core.double? avgHardness,
    $core.double? maxHardness,
    Profile? profile,
    $core.Iterable<$core.String>? device,
    $core.Iterable<TrainingData>? samples,
  }) {
    final _result = create();
    if (minHr != null) {
      _result.minHr = minHr;
    }
    if (avgHr != null) {
      _result.avgHr = avgHr;
    }
    if (maxHr != null) {
      _result.maxHr = maxHr;
    }
    if (minHardness != null) {
      _result.minHardness = minHardness;
    }
    if (avgHardness != null) {
      _result.avgHardness = avgHardness;
    }
    if (maxHardness != null) {
      _result.maxHardness = maxHardness;
    }
    if (profile != null) {
      _result.profile = profile;
    }
    if (device != null) {
      _result.device.addAll(device);
    }
    if (samples != null) {
      _result.samples.addAll(samples);
    }
    return _result;
  }
  factory HrDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory HrDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  HrDetails clone() => HrDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  HrDetails copyWith(void Function(HrDetails) updates) => super.copyWith((message) => updates(message as HrDetails)) as HrDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HrDetails create() => HrDetails._();
  HrDetails createEmptyInstance() => create();
  static $pb.PbList<HrDetails> createRepeated() => $pb.PbList<HrDetails>();
  @$core.pragma('dart2js:noInline')
  static HrDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<HrDetails>(create);
  static HrDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get minHr => $_getIZ(0);
  @$pb.TagNumber(1)
  set minHr($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMinHr() => $_has(0);
  @$pb.TagNumber(1)
  void clearMinHr() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get avgHr => $_getIZ(1);
  @$pb.TagNumber(2)
  set avgHr($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAvgHr() => $_has(1);
  @$pb.TagNumber(2)
  void clearAvgHr() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get maxHr => $_getIZ(2);
  @$pb.TagNumber(3)
  set maxHr($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMaxHr() => $_has(2);
  @$pb.TagNumber(3)
  void clearMaxHr() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get minHardness => $_getN(3);
  @$pb.TagNumber(4)
  set minHardness($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMinHardness() => $_has(3);
  @$pb.TagNumber(4)
  void clearMinHardness() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get avgHardness => $_getN(4);
  @$pb.TagNumber(5)
  set avgHardness($core.double v) { $_setDouble(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasAvgHardness() => $_has(4);
  @$pb.TagNumber(5)
  void clearAvgHardness() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get maxHardness => $_getN(5);
  @$pb.TagNumber(6)
  set maxHardness($core.double v) { $_setDouble(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasMaxHardness() => $_has(5);
  @$pb.TagNumber(6)
  void clearMaxHardness() => clearField(6);

  @$pb.TagNumber(7)
  Profile get profile => $_getN(6);
  @$pb.TagNumber(7)
  set profile(Profile v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasProfile() => $_has(6);
  @$pb.TagNumber(7)
  void clearProfile() => clearField(7);
  @$pb.TagNumber(7)
  Profile ensureProfile() => $_ensure(6);

  @$pb.TagNumber(8)
  $core.List<$core.String> get device => $_getList(7);

  @$pb.TagNumber(9)
  $core.List<TrainingData> get samples => $_getList(8);
}

enum TrainingSession_Details {
  steps, 
  hr, 
  notSet
}

class TrainingSession extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, TrainingSession_Details> _TrainingSession_DetailsByTag = {
    10 : TrainingSession_Details.steps,
    11 : TrainingSession_Details.hr,
    0 : TrainingSession_Details.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TrainingSession', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..oo(0, [10, 11])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'syncMillis')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startMillis')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stopMillis')
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'skllzz', $pb.PbFieldType.OD)
    ..aOB(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deleted')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sourceId')
    ..a<$core.int>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', $pb.PbFieldType.OU3)
    ..aOM<StepDetails>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'steps', subBuilder: StepDetails.create)
    ..aOM<HrDetails>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hr', subBuilder: HrDetails.create)
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timezone')
    ..a<$core.double>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'kkal', $pb.PbFieldType.OD)
    ..aOS(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'profileId')
    ..aInt64(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activityDay')
    ..a<$core.double>(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'skllzzWithoutArtifacts', $pb.PbFieldType.OD)
    ..pc<$11.ActiveArtifact>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activeArtifacts', $pb.PbFieldType.PM, subBuilder: $11.ActiveArtifact.create)
    ..hasRequiredFields = false
  ;

  TrainingSession._() : super();
  factory TrainingSession({
    $core.String? id,
    $fixnum.Int64? syncMillis,
    $fixnum.Int64? startMillis,
    $fixnum.Int64? stopMillis,
    $core.double? skllzz,
    $core.bool? deleted,
    $core.String? sourceId,
    $core.int? version,
    StepDetails? steps,
    HrDetails? hr,
    $core.String? timezone,
    $core.double? kkal,
    $core.String? profileId,
    $fixnum.Int64? activityDay,
    $core.double? skllzzWithoutArtifacts,
    $core.Iterable<$11.ActiveArtifact>? activeArtifacts,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (syncMillis != null) {
      _result.syncMillis = syncMillis;
    }
    if (startMillis != null) {
      _result.startMillis = startMillis;
    }
    if (stopMillis != null) {
      _result.stopMillis = stopMillis;
    }
    if (skllzz != null) {
      _result.skllzz = skllzz;
    }
    if (deleted != null) {
      _result.deleted = deleted;
    }
    if (sourceId != null) {
      _result.sourceId = sourceId;
    }
    if (version != null) {
      _result.version = version;
    }
    if (steps != null) {
      _result.steps = steps;
    }
    if (hr != null) {
      _result.hr = hr;
    }
    if (timezone != null) {
      _result.timezone = timezone;
    }
    if (kkal != null) {
      _result.kkal = kkal;
    }
    if (profileId != null) {
      _result.profileId = profileId;
    }
    if (activityDay != null) {
      _result.activityDay = activityDay;
    }
    if (skllzzWithoutArtifacts != null) {
      _result.skllzzWithoutArtifacts = skllzzWithoutArtifacts;
    }
    if (activeArtifacts != null) {
      _result.activeArtifacts.addAll(activeArtifacts);
    }
    return _result;
  }
  factory TrainingSession.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TrainingSession.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TrainingSession clone() => TrainingSession()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TrainingSession copyWith(void Function(TrainingSession) updates) => super.copyWith((message) => updates(message as TrainingSession)) as TrainingSession; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TrainingSession create() => TrainingSession._();
  TrainingSession createEmptyInstance() => create();
  static $pb.PbList<TrainingSession> createRepeated() => $pb.PbList<TrainingSession>();
  @$core.pragma('dart2js:noInline')
  static TrainingSession getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TrainingSession>(create);
  static TrainingSession? _defaultInstance;

  TrainingSession_Details whichDetails() => _TrainingSession_DetailsByTag[$_whichOneof(0)]!;
  void clearDetails() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get syncMillis => $_getI64(1);
  @$pb.TagNumber(2)
  set syncMillis($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSyncMillis() => $_has(1);
  @$pb.TagNumber(2)
  void clearSyncMillis() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get startMillis => $_getI64(2);
  @$pb.TagNumber(3)
  set startMillis($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStartMillis() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartMillis() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get stopMillis => $_getI64(3);
  @$pb.TagNumber(4)
  set stopMillis($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasStopMillis() => $_has(3);
  @$pb.TagNumber(4)
  void clearStopMillis() => clearField(4);

  @$pb.TagNumber(6)
  $core.double get skllzz => $_getN(4);
  @$pb.TagNumber(6)
  set skllzz($core.double v) { $_setDouble(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasSkllzz() => $_has(4);
  @$pb.TagNumber(6)
  void clearSkllzz() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get deleted => $_getBF(5);
  @$pb.TagNumber(7)
  set deleted($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasDeleted() => $_has(5);
  @$pb.TagNumber(7)
  void clearDeleted() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get sourceId => $_getSZ(6);
  @$pb.TagNumber(8)
  set sourceId($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasSourceId() => $_has(6);
  @$pb.TagNumber(8)
  void clearSourceId() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get version => $_getIZ(7);
  @$pb.TagNumber(9)
  set version($core.int v) { $_setUnsignedInt32(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasVersion() => $_has(7);
  @$pb.TagNumber(9)
  void clearVersion() => clearField(9);

  @$pb.TagNumber(10)
  StepDetails get steps => $_getN(8);
  @$pb.TagNumber(10)
  set steps(StepDetails v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasSteps() => $_has(8);
  @$pb.TagNumber(10)
  void clearSteps() => clearField(10);
  @$pb.TagNumber(10)
  StepDetails ensureSteps() => $_ensure(8);

  @$pb.TagNumber(11)
  HrDetails get hr => $_getN(9);
  @$pb.TagNumber(11)
  set hr(HrDetails v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasHr() => $_has(9);
  @$pb.TagNumber(11)
  void clearHr() => clearField(11);
  @$pb.TagNumber(11)
  HrDetails ensureHr() => $_ensure(9);

  @$pb.TagNumber(12)
  $core.String get timezone => $_getSZ(10);
  @$pb.TagNumber(12)
  set timezone($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(12)
  $core.bool hasTimezone() => $_has(10);
  @$pb.TagNumber(12)
  void clearTimezone() => clearField(12);

  @$pb.TagNumber(13)
  $core.double get kkal => $_getN(11);
  @$pb.TagNumber(13)
  set kkal($core.double v) { $_setDouble(11, v); }
  @$pb.TagNumber(13)
  $core.bool hasKkal() => $_has(11);
  @$pb.TagNumber(13)
  void clearKkal() => clearField(13);

  @$pb.TagNumber(14)
  $core.String get profileId => $_getSZ(12);
  @$pb.TagNumber(14)
  set profileId($core.String v) { $_setString(12, v); }
  @$pb.TagNumber(14)
  $core.bool hasProfileId() => $_has(12);
  @$pb.TagNumber(14)
  void clearProfileId() => clearField(14);

  @$pb.TagNumber(15)
  $fixnum.Int64 get activityDay => $_getI64(13);
  @$pb.TagNumber(15)
  set activityDay($fixnum.Int64 v) { $_setInt64(13, v); }
  @$pb.TagNumber(15)
  $core.bool hasActivityDay() => $_has(13);
  @$pb.TagNumber(15)
  void clearActivityDay() => clearField(15);

  @$pb.TagNumber(16)
  $core.double get skllzzWithoutArtifacts => $_getN(14);
  @$pb.TagNumber(16)
  set skllzzWithoutArtifacts($core.double v) { $_setDouble(14, v); }
  @$pb.TagNumber(16)
  $core.bool hasSkllzzWithoutArtifacts() => $_has(14);
  @$pb.TagNumber(16)
  void clearSkllzzWithoutArtifacts() => clearField(16);

  @$pb.TagNumber(20)
  $core.List<$11.ActiveArtifact> get activeArtifacts => $_getList(15);
}

class TrainingData extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TrainingData', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sessionId')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'syncMillis')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stampMillis')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deviceId')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deviceName')
    ..a<$core.double>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hrAvg', $pb.PbFieldType.OD, protoName: 'hrAvg')
    ..a<$core.int>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'duration', $pb.PbFieldType.O3)
    ..a<$core.double>(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'skllzz', $pb.PbFieldType.OD)
    ..a<$core.double>(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hardness', $pb.PbFieldType.OD)
    ..a<$core.int>(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', $pb.PbFieldType.OU3)
    ..aOS(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'profileId')
    ..hasRequiredFields = false
  ;

  TrainingData._() : super();
  factory TrainingData({
    $core.String? sessionId,
    $fixnum.Int64? syncMillis,
    $fixnum.Int64? stampMillis,
    $core.String? deviceId,
    $core.String? deviceName,
    $core.double? hrAvg,
    $core.int? duration,
    $core.double? skllzz,
    $core.double? hardness,
    $core.int? version,
    $core.String? profileId,
  }) {
    final _result = create();
    if (sessionId != null) {
      _result.sessionId = sessionId;
    }
    if (syncMillis != null) {
      _result.syncMillis = syncMillis;
    }
    if (stampMillis != null) {
      _result.stampMillis = stampMillis;
    }
    if (deviceId != null) {
      _result.deviceId = deviceId;
    }
    if (deviceName != null) {
      _result.deviceName = deviceName;
    }
    if (hrAvg != null) {
      _result.hrAvg = hrAvg;
    }
    if (duration != null) {
      _result.duration = duration;
    }
    if (skllzz != null) {
      _result.skllzz = skllzz;
    }
    if (hardness != null) {
      _result.hardness = hardness;
    }
    if (version != null) {
      _result.version = version;
    }
    if (profileId != null) {
      _result.profileId = profileId;
    }
    return _result;
  }
  factory TrainingData.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TrainingData.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TrainingData clone() => TrainingData()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TrainingData copyWith(void Function(TrainingData) updates) => super.copyWith((message) => updates(message as TrainingData)) as TrainingData; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TrainingData create() => TrainingData._();
  TrainingData createEmptyInstance() => create();
  static $pb.PbList<TrainingData> createRepeated() => $pb.PbList<TrainingData>();
  @$core.pragma('dart2js:noInline')
  static TrainingData getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TrainingData>(create);
  static TrainingData? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get sessionId => $_getSZ(0);
  @$pb.TagNumber(1)
  set sessionId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSessionId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSessionId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get syncMillis => $_getI64(1);
  @$pb.TagNumber(2)
  set syncMillis($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSyncMillis() => $_has(1);
  @$pb.TagNumber(2)
  void clearSyncMillis() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get stampMillis => $_getI64(2);
  @$pb.TagNumber(3)
  set stampMillis($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStampMillis() => $_has(2);
  @$pb.TagNumber(3)
  void clearStampMillis() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get deviceId => $_getSZ(3);
  @$pb.TagNumber(4)
  set deviceId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDeviceId() => $_has(3);
  @$pb.TagNumber(4)
  void clearDeviceId() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get deviceName => $_getSZ(4);
  @$pb.TagNumber(5)
  set deviceName($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDeviceName() => $_has(4);
  @$pb.TagNumber(5)
  void clearDeviceName() => clearField(5);

  @$pb.TagNumber(12)
  $core.double get hrAvg => $_getN(5);
  @$pb.TagNumber(12)
  set hrAvg($core.double v) { $_setDouble(5, v); }
  @$pb.TagNumber(12)
  $core.bool hasHrAvg() => $_has(5);
  @$pb.TagNumber(12)
  void clearHrAvg() => clearField(12);

  @$pb.TagNumber(13)
  $core.int get duration => $_getIZ(6);
  @$pb.TagNumber(13)
  set duration($core.int v) { $_setSignedInt32(6, v); }
  @$pb.TagNumber(13)
  $core.bool hasDuration() => $_has(6);
  @$pb.TagNumber(13)
  void clearDuration() => clearField(13);

  @$pb.TagNumber(14)
  $core.double get skllzz => $_getN(7);
  @$pb.TagNumber(14)
  set skllzz($core.double v) { $_setDouble(7, v); }
  @$pb.TagNumber(14)
  $core.bool hasSkllzz() => $_has(7);
  @$pb.TagNumber(14)
  void clearSkllzz() => clearField(14);

  @$pb.TagNumber(15)
  $core.double get hardness => $_getN(8);
  @$pb.TagNumber(15)
  set hardness($core.double v) { $_setDouble(8, v); }
  @$pb.TagNumber(15)
  $core.bool hasHardness() => $_has(8);
  @$pb.TagNumber(15)
  void clearHardness() => clearField(15);

  @$pb.TagNumber(16)
  $core.int get version => $_getIZ(9);
  @$pb.TagNumber(16)
  set version($core.int v) { $_setUnsignedInt32(9, v); }
  @$pb.TagNumber(16)
  $core.bool hasVersion() => $_has(9);
  @$pb.TagNumber(16)
  void clearVersion() => clearField(16);

  @$pb.TagNumber(17)
  $core.String get profileId => $_getSZ(10);
  @$pb.TagNumber(17)
  set profileId($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(17)
  $core.bool hasProfileId() => $_has(10);
  @$pb.TagNumber(17)
  void clearProfileId() => clearField(17);
}

class StepsData extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StepsData', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', $pb.PbFieldType.OU3)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'syncMillis')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stampMillis')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'duration', $pb.PbFieldType.O3)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'steps', $pb.PbFieldType.O3)
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sourceId')
    ..pPS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stepSource')
    ..hasRequiredFields = false
  ;

  StepsData._() : super();
  factory StepsData({
    $core.int? version,
    $fixnum.Int64? syncMillis,
    $fixnum.Int64? stampMillis,
    $core.int? duration,
    $core.int? steps,
    $core.String? sourceId,
    $core.Iterable<$core.String>? stepSource,
  }) {
    final _result = create();
    if (version != null) {
      _result.version = version;
    }
    if (syncMillis != null) {
      _result.syncMillis = syncMillis;
    }
    if (stampMillis != null) {
      _result.stampMillis = stampMillis;
    }
    if (duration != null) {
      _result.duration = duration;
    }
    if (steps != null) {
      _result.steps = steps;
    }
    if (sourceId != null) {
      _result.sourceId = sourceId;
    }
    if (stepSource != null) {
      _result.stepSource.addAll(stepSource);
    }
    return _result;
  }
  factory StepsData.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StepsData.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StepsData clone() => StepsData()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StepsData copyWith(void Function(StepsData) updates) => super.copyWith((message) => updates(message as StepsData)) as StepsData; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StepsData create() => StepsData._();
  StepsData createEmptyInstance() => create();
  static $pb.PbList<StepsData> createRepeated() => $pb.PbList<StepsData>();
  @$core.pragma('dart2js:noInline')
  static StepsData getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StepsData>(create);
  static StepsData? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get version => $_getIZ(0);
  @$pb.TagNumber(1)
  set version($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasVersion() => $_has(0);
  @$pb.TagNumber(1)
  void clearVersion() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get syncMillis => $_getI64(1);
  @$pb.TagNumber(2)
  set syncMillis($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSyncMillis() => $_has(1);
  @$pb.TagNumber(2)
  void clearSyncMillis() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get stampMillis => $_getI64(2);
  @$pb.TagNumber(3)
  set stampMillis($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStampMillis() => $_has(2);
  @$pb.TagNumber(3)
  void clearStampMillis() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get duration => $_getIZ(3);
  @$pb.TagNumber(4)
  set duration($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDuration() => $_has(3);
  @$pb.TagNumber(4)
  void clearDuration() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get steps => $_getIZ(4);
  @$pb.TagNumber(5)
  set steps($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasSteps() => $_has(4);
  @$pb.TagNumber(5)
  void clearSteps() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get sourceId => $_getSZ(5);
  @$pb.TagNumber(6)
  set sourceId($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasSourceId() => $_has(5);
  @$pb.TagNumber(6)
  void clearSourceId() => clearField(6);

  @$pb.TagNumber(7)
  $core.List<$core.String> get stepSource => $_getList(6);
}

class Profile extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Profile', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nickName')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'birthDate', $pb.PbFieldType.O3)
    ..a<$core.int>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hrRest', $pb.PbFieldType.O3)
    ..a<$core.int>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hrMax', $pb.PbFieldType.O3)
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatarUrl')
    ..e<Sex>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sex', $pb.PbFieldType.OE, defaultOrMaker: Sex.undefined, valueOf: Sex.valueOf, enumValues: Sex.values)
    ..a<$core.double>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'weight', $pb.PbFieldType.OF)
    ..aInt64(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'joinStamp')
    ..aOB(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tester')
    ..a<$core.int>(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', $pb.PbFieldType.OU3)
    ..aOM<Achievements>(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'achievements', subBuilder: Achievements.create)
    ..aOS(18, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timezone')
    ..aOS(19, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'garminAccessToken')
    ..aOS(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'polarAccessToken')
    ..aOS(21, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'suuntoAccessToken')
    ..aOS(22, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fitbitAccessToken')
    ..aOS(23, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inviteLink')
    ..aOS(24, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lang')
    ..m<$core.String, $core.bool>(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'subscriptions', entryClassName: 'Profile.SubscriptionsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OB, packageName: const $pb.PackageName('com.skllzz.api'))
    ..aInt64(40, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'syncMillis')
    ..hasRequiredFields = false
  ;

  Profile._() : super();
  factory Profile({
    $core.String? id,
    $core.String? name,
    $core.String? nickName,
    $core.String? email,
    $core.String? phone,
    $core.int? birthDate,
    $core.int? hrRest,
    $core.int? hrMax,
    $core.String? avatarUrl,
    Sex? sex,
    $core.double? weight,
    $fixnum.Int64? joinStamp,
    $core.bool? tester,
    $core.int? version,
    Achievements? achievements,
    $core.String? timezone,
    $core.String? garminAccessToken,
    $core.String? polarAccessToken,
    $core.String? suuntoAccessToken,
    $core.String? fitbitAccessToken,
    $core.String? inviteLink,
    $core.String? lang,
    $core.Map<$core.String, $core.bool>? subscriptions,
    $fixnum.Int64? syncMillis,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (nickName != null) {
      _result.nickName = nickName;
    }
    if (email != null) {
      _result.email = email;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    if (birthDate != null) {
      _result.birthDate = birthDate;
    }
    if (hrRest != null) {
      _result.hrRest = hrRest;
    }
    if (hrMax != null) {
      _result.hrMax = hrMax;
    }
    if (avatarUrl != null) {
      _result.avatarUrl = avatarUrl;
    }
    if (sex != null) {
      _result.sex = sex;
    }
    if (weight != null) {
      _result.weight = weight;
    }
    if (joinStamp != null) {
      _result.joinStamp = joinStamp;
    }
    if (tester != null) {
      _result.tester = tester;
    }
    if (version != null) {
      _result.version = version;
    }
    if (achievements != null) {
      _result.achievements = achievements;
    }
    if (timezone != null) {
      _result.timezone = timezone;
    }
    if (garminAccessToken != null) {
      _result.garminAccessToken = garminAccessToken;
    }
    if (polarAccessToken != null) {
      _result.polarAccessToken = polarAccessToken;
    }
    if (suuntoAccessToken != null) {
      _result.suuntoAccessToken = suuntoAccessToken;
    }
    if (fitbitAccessToken != null) {
      _result.fitbitAccessToken = fitbitAccessToken;
    }
    if (inviteLink != null) {
      _result.inviteLink = inviteLink;
    }
    if (lang != null) {
      _result.lang = lang;
    }
    if (subscriptions != null) {
      _result.subscriptions.addAll(subscriptions);
    }
    if (syncMillis != null) {
      _result.syncMillis = syncMillis;
    }
    return _result;
  }
  factory Profile.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Profile.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Profile clone() => Profile()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Profile copyWith(void Function(Profile) updates) => super.copyWith((message) => updates(message as Profile)) as Profile; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Profile create() => Profile._();
  Profile createEmptyInstance() => create();
  static $pb.PbList<Profile> createRepeated() => $pb.PbList<Profile>();
  @$core.pragma('dart2js:noInline')
  static Profile getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Profile>(create);
  static Profile? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get nickName => $_getSZ(2);
  @$pb.TagNumber(3)
  set nickName($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasNickName() => $_has(2);
  @$pb.TagNumber(3)
  void clearNickName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get email => $_getSZ(3);
  @$pb.TagNumber(4)
  set email($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasEmail() => $_has(3);
  @$pb.TagNumber(4)
  void clearEmail() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get phone => $_getSZ(4);
  @$pb.TagNumber(5)
  set phone($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPhone() => $_has(4);
  @$pb.TagNumber(5)
  void clearPhone() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get birthDate => $_getIZ(5);
  @$pb.TagNumber(6)
  set birthDate($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasBirthDate() => $_has(5);
  @$pb.TagNumber(6)
  void clearBirthDate() => clearField(6);

  @$pb.TagNumber(7)
  $core.int get hrRest => $_getIZ(6);
  @$pb.TagNumber(7)
  set hrRest($core.int v) { $_setSignedInt32(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasHrRest() => $_has(6);
  @$pb.TagNumber(7)
  void clearHrRest() => clearField(7);

  @$pb.TagNumber(8)
  $core.int get hrMax => $_getIZ(7);
  @$pb.TagNumber(8)
  set hrMax($core.int v) { $_setSignedInt32(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasHrMax() => $_has(7);
  @$pb.TagNumber(8)
  void clearHrMax() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get avatarUrl => $_getSZ(8);
  @$pb.TagNumber(9)
  set avatarUrl($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasAvatarUrl() => $_has(8);
  @$pb.TagNumber(9)
  void clearAvatarUrl() => clearField(9);

  @$pb.TagNumber(11)
  Sex get sex => $_getN(9);
  @$pb.TagNumber(11)
  set sex(Sex v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasSex() => $_has(9);
  @$pb.TagNumber(11)
  void clearSex() => clearField(11);

  @$pb.TagNumber(12)
  $core.double get weight => $_getN(10);
  @$pb.TagNumber(12)
  set weight($core.double v) { $_setFloat(10, v); }
  @$pb.TagNumber(12)
  $core.bool hasWeight() => $_has(10);
  @$pb.TagNumber(12)
  void clearWeight() => clearField(12);

  @$pb.TagNumber(13)
  $fixnum.Int64 get joinStamp => $_getI64(11);
  @$pb.TagNumber(13)
  set joinStamp($fixnum.Int64 v) { $_setInt64(11, v); }
  @$pb.TagNumber(13)
  $core.bool hasJoinStamp() => $_has(11);
  @$pb.TagNumber(13)
  void clearJoinStamp() => clearField(13);

  @$pb.TagNumber(15)
  $core.bool get tester => $_getBF(12);
  @$pb.TagNumber(15)
  set tester($core.bool v) { $_setBool(12, v); }
  @$pb.TagNumber(15)
  $core.bool hasTester() => $_has(12);
  @$pb.TagNumber(15)
  void clearTester() => clearField(15);

  @$pb.TagNumber(16)
  $core.int get version => $_getIZ(13);
  @$pb.TagNumber(16)
  set version($core.int v) { $_setUnsignedInt32(13, v); }
  @$pb.TagNumber(16)
  $core.bool hasVersion() => $_has(13);
  @$pb.TagNumber(16)
  void clearVersion() => clearField(16);

  @$pb.TagNumber(17)
  Achievements get achievements => $_getN(14);
  @$pb.TagNumber(17)
  set achievements(Achievements v) { setField(17, v); }
  @$pb.TagNumber(17)
  $core.bool hasAchievements() => $_has(14);
  @$pb.TagNumber(17)
  void clearAchievements() => clearField(17);
  @$pb.TagNumber(17)
  Achievements ensureAchievements() => $_ensure(14);

  @$pb.TagNumber(18)
  $core.String get timezone => $_getSZ(15);
  @$pb.TagNumber(18)
  set timezone($core.String v) { $_setString(15, v); }
  @$pb.TagNumber(18)
  $core.bool hasTimezone() => $_has(15);
  @$pb.TagNumber(18)
  void clearTimezone() => clearField(18);

  @$pb.TagNumber(19)
  $core.String get garminAccessToken => $_getSZ(16);
  @$pb.TagNumber(19)
  set garminAccessToken($core.String v) { $_setString(16, v); }
  @$pb.TagNumber(19)
  $core.bool hasGarminAccessToken() => $_has(16);
  @$pb.TagNumber(19)
  void clearGarminAccessToken() => clearField(19);

  @$pb.TagNumber(20)
  $core.String get polarAccessToken => $_getSZ(17);
  @$pb.TagNumber(20)
  set polarAccessToken($core.String v) { $_setString(17, v); }
  @$pb.TagNumber(20)
  $core.bool hasPolarAccessToken() => $_has(17);
  @$pb.TagNumber(20)
  void clearPolarAccessToken() => clearField(20);

  @$pb.TagNumber(21)
  $core.String get suuntoAccessToken => $_getSZ(18);
  @$pb.TagNumber(21)
  set suuntoAccessToken($core.String v) { $_setString(18, v); }
  @$pb.TagNumber(21)
  $core.bool hasSuuntoAccessToken() => $_has(18);
  @$pb.TagNumber(21)
  void clearSuuntoAccessToken() => clearField(21);

  @$pb.TagNumber(22)
  $core.String get fitbitAccessToken => $_getSZ(19);
  @$pb.TagNumber(22)
  set fitbitAccessToken($core.String v) { $_setString(19, v); }
  @$pb.TagNumber(22)
  $core.bool hasFitbitAccessToken() => $_has(19);
  @$pb.TagNumber(22)
  void clearFitbitAccessToken() => clearField(22);

  @$pb.TagNumber(23)
  $core.String get inviteLink => $_getSZ(20);
  @$pb.TagNumber(23)
  set inviteLink($core.String v) { $_setString(20, v); }
  @$pb.TagNumber(23)
  $core.bool hasInviteLink() => $_has(20);
  @$pb.TagNumber(23)
  void clearInviteLink() => clearField(23);

  @$pb.TagNumber(24)
  $core.String get lang => $_getSZ(21);
  @$pb.TagNumber(24)
  set lang($core.String v) { $_setString(21, v); }
  @$pb.TagNumber(24)
  $core.bool hasLang() => $_has(21);
  @$pb.TagNumber(24)
  void clearLang() => clearField(24);

  @$pb.TagNumber(30)
  $core.Map<$core.String, $core.bool> get subscriptions => $_getMap(22);

  @$pb.TagNumber(40)
  $fixnum.Int64 get syncMillis => $_getI64(23);
  @$pb.TagNumber(40)
  set syncMillis($fixnum.Int64 v) { $_setInt64(23, v); }
  @$pb.TagNumber(40)
  $core.bool hasSyncMillis() => $_has(23);
  @$pb.TagNumber(40)
  void clearSyncMillis() => clearField(40);
}

class Achievements extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Achievements', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..e<Level>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'level', $pb.PbFieldType.OE, defaultOrMaker: Level.basic, valueOf: Level.valueOf, enumValues: Level.values)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'totalSkllzz', $pb.PbFieldType.O3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'iamokSkllzzEarned', $pb.PbFieldType.O3)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'iamokSkllzzRequired', $pb.PbFieldType.O3)
    ..aInt64(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'iamokStart')
    ..aInt64(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'iamokDuration')
    ..a<$core.double>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'skllzzDayLimit', $pb.PbFieldType.OD)
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'iamokUuid')
    ..a<$core.int>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'totalSkllzzEarned', $pb.PbFieldType.O3)
    ..a<$core.int>(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gameAgeDays', $pb.PbFieldType.O3)
    ..m<$core.String, $11.ActiveArtifact>(200, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'artifacts', entryClassName: 'Achievements.ArtifactsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OM, valueCreator: $11.ActiveArtifact.create, packageName: const $pb.PackageName('com.skllzz.api'))
    ..hasRequiredFields = false
  ;

  Achievements._() : super();
  factory Achievements({
    Level? level,
    $core.int? totalSkllzz,
    $core.int? iamokSkllzzEarned,
    $core.int? iamokSkllzzRequired,
    $fixnum.Int64? iamokStart,
    $fixnum.Int64? iamokDuration,
    $core.double? skllzzDayLimit,
    $core.String? iamokUuid,
    $core.int? totalSkllzzEarned,
    $core.int? gameAgeDays,
    $core.Map<$core.String, $11.ActiveArtifact>? artifacts,
  }) {
    final _result = create();
    if (level != null) {
      _result.level = level;
    }
    if (totalSkllzz != null) {
      _result.totalSkllzz = totalSkllzz;
    }
    if (iamokSkllzzEarned != null) {
      _result.iamokSkllzzEarned = iamokSkllzzEarned;
    }
    if (iamokSkllzzRequired != null) {
      _result.iamokSkllzzRequired = iamokSkllzzRequired;
    }
    if (iamokStart != null) {
      _result.iamokStart = iamokStart;
    }
    if (iamokDuration != null) {
      _result.iamokDuration = iamokDuration;
    }
    if (skllzzDayLimit != null) {
      _result.skllzzDayLimit = skllzzDayLimit;
    }
    if (iamokUuid != null) {
      _result.iamokUuid = iamokUuid;
    }
    if (totalSkllzzEarned != null) {
      _result.totalSkllzzEarned = totalSkllzzEarned;
    }
    if (gameAgeDays != null) {
      _result.gameAgeDays = gameAgeDays;
    }
    if (artifacts != null) {
      _result.artifacts.addAll(artifacts);
    }
    return _result;
  }
  factory Achievements.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Achievements.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Achievements clone() => Achievements()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Achievements copyWith(void Function(Achievements) updates) => super.copyWith((message) => updates(message as Achievements)) as Achievements; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Achievements create() => Achievements._();
  Achievements createEmptyInstance() => create();
  static $pb.PbList<Achievements> createRepeated() => $pb.PbList<Achievements>();
  @$core.pragma('dart2js:noInline')
  static Achievements getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Achievements>(create);
  static Achievements? _defaultInstance;

  @$pb.TagNumber(1)
  Level get level => $_getN(0);
  @$pb.TagNumber(1)
  set level(Level v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasLevel() => $_has(0);
  @$pb.TagNumber(1)
  void clearLevel() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get totalSkllzz => $_getIZ(1);
  @$pb.TagNumber(2)
  set totalSkllzz($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTotalSkllzz() => $_has(1);
  @$pb.TagNumber(2)
  void clearTotalSkllzz() => clearField(2);

  @$pb.TagNumber(4)
  $core.int get iamokSkllzzEarned => $_getIZ(2);
  @$pb.TagNumber(4)
  set iamokSkllzzEarned($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(4)
  $core.bool hasIamokSkllzzEarned() => $_has(2);
  @$pb.TagNumber(4)
  void clearIamokSkllzzEarned() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get iamokSkllzzRequired => $_getIZ(3);
  @$pb.TagNumber(5)
  set iamokSkllzzRequired($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(5)
  $core.bool hasIamokSkllzzRequired() => $_has(3);
  @$pb.TagNumber(5)
  void clearIamokSkllzzRequired() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get iamokStart => $_getI64(4);
  @$pb.TagNumber(6)
  set iamokStart($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasIamokStart() => $_has(4);
  @$pb.TagNumber(6)
  void clearIamokStart() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get iamokDuration => $_getI64(5);
  @$pb.TagNumber(7)
  set iamokDuration($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasIamokDuration() => $_has(5);
  @$pb.TagNumber(7)
  void clearIamokDuration() => clearField(7);

  @$pb.TagNumber(8)
  $core.double get skllzzDayLimit => $_getN(6);
  @$pb.TagNumber(8)
  set skllzzDayLimit($core.double v) { $_setDouble(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasSkllzzDayLimit() => $_has(6);
  @$pb.TagNumber(8)
  void clearSkllzzDayLimit() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get iamokUuid => $_getSZ(7);
  @$pb.TagNumber(9)
  set iamokUuid($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasIamokUuid() => $_has(7);
  @$pb.TagNumber(9)
  void clearIamokUuid() => clearField(9);

  @$pb.TagNumber(12)
  $core.int get totalSkllzzEarned => $_getIZ(8);
  @$pb.TagNumber(12)
  set totalSkllzzEarned($core.int v) { $_setSignedInt32(8, v); }
  @$pb.TagNumber(12)
  $core.bool hasTotalSkllzzEarned() => $_has(8);
  @$pb.TagNumber(12)
  void clearTotalSkllzzEarned() => clearField(12);

  @$pb.TagNumber(30)
  $core.int get gameAgeDays => $_getIZ(9);
  @$pb.TagNumber(30)
  set gameAgeDays($core.int v) { $_setSignedInt32(9, v); }
  @$pb.TagNumber(30)
  $core.bool hasGameAgeDays() => $_has(9);
  @$pb.TagNumber(30)
  void clearGameAgeDays() => clearField(30);

  @$pb.TagNumber(200)
  $core.Map<$core.String, $11.ActiveArtifact> get artifacts => $_getMap(10);
}

class IamOk extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'IamOk', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'totalSkllzz', $pb.PbFieldType.OD)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'skllzzEarned', $pb.PbFieldType.OD)
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'skllzzRequired', $pb.PbFieldType.OD)
    ..aInt64(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startSeconds')
    ..aInt64(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stopSeconds')
    ..a<$core.int>(30, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gameAgeDays', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  IamOk._() : super();
  factory IamOk({
    $core.String? id,
    $core.double? totalSkllzz,
    $core.double? skllzzEarned,
    $core.double? skllzzRequired,
    $fixnum.Int64? startSeconds,
    $fixnum.Int64? stopSeconds,
    $core.int? gameAgeDays,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (totalSkllzz != null) {
      _result.totalSkllzz = totalSkllzz;
    }
    if (skllzzEarned != null) {
      _result.skllzzEarned = skllzzEarned;
    }
    if (skllzzRequired != null) {
      _result.skllzzRequired = skllzzRequired;
    }
    if (startSeconds != null) {
      _result.startSeconds = startSeconds;
    }
    if (stopSeconds != null) {
      _result.stopSeconds = stopSeconds;
    }
    if (gameAgeDays != null) {
      _result.gameAgeDays = gameAgeDays;
    }
    return _result;
  }
  factory IamOk.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory IamOk.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  IamOk clone() => IamOk()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  IamOk copyWith(void Function(IamOk) updates) => super.copyWith((message) => updates(message as IamOk)) as IamOk; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static IamOk create() => IamOk._();
  IamOk createEmptyInstance() => create();
  static $pb.PbList<IamOk> createRepeated() => $pb.PbList<IamOk>();
  @$core.pragma('dart2js:noInline')
  static IamOk getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<IamOk>(create);
  static IamOk? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get totalSkllzz => $_getN(1);
  @$pb.TagNumber(2)
  set totalSkllzz($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTotalSkllzz() => $_has(1);
  @$pb.TagNumber(2)
  void clearTotalSkllzz() => clearField(2);

  @$pb.TagNumber(4)
  $core.double get skllzzEarned => $_getN(2);
  @$pb.TagNumber(4)
  set skllzzEarned($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(4)
  $core.bool hasSkllzzEarned() => $_has(2);
  @$pb.TagNumber(4)
  void clearSkllzzEarned() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get skllzzRequired => $_getN(3);
  @$pb.TagNumber(5)
  set skllzzRequired($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(5)
  $core.bool hasSkllzzRequired() => $_has(3);
  @$pb.TagNumber(5)
  void clearSkllzzRequired() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get startSeconds => $_getI64(4);
  @$pb.TagNumber(6)
  set startSeconds($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasStartSeconds() => $_has(4);
  @$pb.TagNumber(6)
  void clearStartSeconds() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get stopSeconds => $_getI64(5);
  @$pb.TagNumber(7)
  set stopSeconds($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasStopSeconds() => $_has(5);
  @$pb.TagNumber(7)
  void clearStopSeconds() => clearField(7);

  @$pb.TagNumber(30)
  $core.int get gameAgeDays => $_getIZ(6);
  @$pb.TagNumber(30)
  set gameAgeDays($core.int v) { $_setSignedInt32(6, v); }
  @$pb.TagNumber(30)
  $core.bool hasGameAgeDays() => $_has(6);
  @$pb.TagNumber(30)
  void clearGameAgeDays() => clearField(30);
}

enum Property_Kind {
  known, 
  custom, 
  notSet
}

enum Property_Value {
  string, 
  int_4, 
  bool_5, 
  float, 
  notSet
}

class Property extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Property_Kind> _Property_KindByTag = {
    1 : Property_Kind.known,
    2 : Property_Kind.custom,
    0 : Property_Kind.notSet
  };
  static const $core.Map<$core.int, Property_Value> _Property_ValueByTag = {
    3 : Property_Value.string,
    4 : Property_Value.int_4,
    5 : Property_Value.bool_5,
    6 : Property_Value.float,
    0 : Property_Value.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Property', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..oo(1, [3, 4, 5, 6])
    ..e<Property_Known>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'known', $pb.PbFieldType.OE, defaultOrMaker: Property_Known.age, valueOf: Property_Known.valueOf, enumValues: Property_Known.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'custom')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'string')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'int')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bool')
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'float', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  Property._() : super();
  factory Property({
    Property_Known? known,
    $core.String? custom,
    $core.String? string,
    $fixnum.Int64? int_4,
    $core.bool? bool_5,
    $core.double? float,
  }) {
    final _result = create();
    if (known != null) {
      _result.known = known;
    }
    if (custom != null) {
      _result.custom = custom;
    }
    if (string != null) {
      _result.string = string;
    }
    if (int_4 != null) {
      _result.int_4 = int_4;
    }
    if (bool_5 != null) {
      _result.bool_5 = bool_5;
    }
    if (float != null) {
      _result.float = float;
    }
    return _result;
  }
  factory Property.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Property.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Property clone() => Property()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Property copyWith(void Function(Property) updates) => super.copyWith((message) => updates(message as Property)) as Property; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Property create() => Property._();
  Property createEmptyInstance() => create();
  static $pb.PbList<Property> createRepeated() => $pb.PbList<Property>();
  @$core.pragma('dart2js:noInline')
  static Property getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Property>(create);
  static Property? _defaultInstance;

  Property_Kind whichKind() => _Property_KindByTag[$_whichOneof(0)]!;
  void clearKind() => clearField($_whichOneof(0));

  Property_Value whichValue() => _Property_ValueByTag[$_whichOneof(1)]!;
  void clearValue() => clearField($_whichOneof(1));

  @$pb.TagNumber(1)
  Property_Known get known => $_getN(0);
  @$pb.TagNumber(1)
  set known(Property_Known v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasKnown() => $_has(0);
  @$pb.TagNumber(1)
  void clearKnown() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get custom => $_getSZ(1);
  @$pb.TagNumber(2)
  set custom($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCustom() => $_has(1);
  @$pb.TagNumber(2)
  void clearCustom() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get string => $_getSZ(2);
  @$pb.TagNumber(3)
  set string($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasString() => $_has(2);
  @$pb.TagNumber(3)
  void clearString() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get int_4 => $_getI64(3);
  @$pb.TagNumber(4)
  set int_4($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasInt_4() => $_has(3);
  @$pb.TagNumber(4)
  void clearInt_4() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get bool_5 => $_getBF(4);
  @$pb.TagNumber(5)
  set bool_5($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasBool_5() => $_has(4);
  @$pb.TagNumber(5)
  void clearBool_5() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get float => $_getN(5);
  @$pb.TagNumber(6)
  set float($core.double v) { $_setFloat(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasFloat() => $_has(5);
  @$pb.TagNumber(6)
  void clearFloat() => clearField(6);
}

class Empty extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Empty', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Empty._() : super();
  factory Empty() => create();
  factory Empty.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Empty.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Empty clone() => Empty()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Empty copyWith(void Function(Empty) updates) => super.copyWith((message) => updates(message as Empty)) as Empty; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Empty create() => Empty._();
  Empty createEmptyInstance() => create();
  static $pb.PbList<Empty> createRepeated() => $pb.PbList<Empty>();
  @$core.pragma('dart2js:noInline')
  static Empty getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Empty>(create);
  static Empty? _defaultInstance;
}

