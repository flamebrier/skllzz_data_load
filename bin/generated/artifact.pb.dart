///
//  Generated code. Do not modify.
//  source: skllzz/common/artifact.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'artifact.pbenum.dart';

export 'artifact.pbenum.dart';

class Artifact extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Artifact', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..e<Artifact_Type>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.OE, defaultOrMaker: Artifact_Type.unknown, valueOf: Artifact_Type.valueOf, enumValues: Artifact_Type.values)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastBought')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastUse')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'produceDuration')
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'proProduceDuration')
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'useAmount', $pb.PbFieldType.O3)
    ..a<$core.double>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'price', $pb.PbFieldType.OD)
    ..aOB(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'available')
    ..aInt64(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'applyDuration')
    ..aOB(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'proOnly')
    ..aOB(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'invisible')
    ..aOB(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'present')
    ..hasRequiredFields = false
  ;

  Artifact._() : super();
  factory Artifact({
    Artifact_Type? id,
    $fixnum.Int64? lastBought,
    $fixnum.Int64? lastUse,
    $fixnum.Int64? produceDuration,
    $fixnum.Int64? proProduceDuration,
    $core.int? useAmount,
    $core.double? price,
    $core.bool? available,
    $fixnum.Int64? applyDuration,
    $core.bool? proOnly,
    $core.bool? invisible,
    $core.bool? present,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (lastBought != null) {
      _result.lastBought = lastBought;
    }
    if (lastUse != null) {
      _result.lastUse = lastUse;
    }
    if (produceDuration != null) {
      _result.produceDuration = produceDuration;
    }
    if (proProduceDuration != null) {
      _result.proProduceDuration = proProduceDuration;
    }
    if (useAmount != null) {
      _result.useAmount = useAmount;
    }
    if (price != null) {
      _result.price = price;
    }
    if (available != null) {
      _result.available = available;
    }
    if (applyDuration != null) {
      _result.applyDuration = applyDuration;
    }
    if (proOnly != null) {
      _result.proOnly = proOnly;
    }
    if (invisible != null) {
      _result.invisible = invisible;
    }
    if (present != null) {
      _result.present = present;
    }
    return _result;
  }
  factory Artifact.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Artifact.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Artifact clone() => Artifact()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Artifact copyWith(void Function(Artifact) updates) => super.copyWith((message) => updates(message as Artifact)) as Artifact; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Artifact create() => Artifact._();
  Artifact createEmptyInstance() => create();
  static $pb.PbList<Artifact> createRepeated() => $pb.PbList<Artifact>();
  @$core.pragma('dart2js:noInline')
  static Artifact getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Artifact>(create);
  static Artifact? _defaultInstance;

  @$pb.TagNumber(1)
  Artifact_Type get id => $_getN(0);
  @$pb.TagNumber(1)
  set id(Artifact_Type v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get lastBought => $_getI64(1);
  @$pb.TagNumber(2)
  set lastBought($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLastBought() => $_has(1);
  @$pb.TagNumber(2)
  void clearLastBought() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get lastUse => $_getI64(2);
  @$pb.TagNumber(3)
  set lastUse($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasLastUse() => $_has(2);
  @$pb.TagNumber(3)
  void clearLastUse() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get produceDuration => $_getI64(3);
  @$pb.TagNumber(4)
  set produceDuration($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasProduceDuration() => $_has(3);
  @$pb.TagNumber(4)
  void clearProduceDuration() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get proProduceDuration => $_getI64(4);
  @$pb.TagNumber(5)
  set proProduceDuration($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasProProduceDuration() => $_has(4);
  @$pb.TagNumber(5)
  void clearProProduceDuration() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get useAmount => $_getIZ(5);
  @$pb.TagNumber(6)
  set useAmount($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasUseAmount() => $_has(5);
  @$pb.TagNumber(6)
  void clearUseAmount() => clearField(6);

  @$pb.TagNumber(7)
  $core.double get price => $_getN(6);
  @$pb.TagNumber(7)
  set price($core.double v) { $_setDouble(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasPrice() => $_has(6);
  @$pb.TagNumber(7)
  void clearPrice() => clearField(7);

  @$pb.TagNumber(8)
  $core.bool get available => $_getBF(7);
  @$pb.TagNumber(8)
  set available($core.bool v) { $_setBool(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasAvailable() => $_has(7);
  @$pb.TagNumber(8)
  void clearAvailable() => clearField(8);

  @$pb.TagNumber(9)
  $fixnum.Int64 get applyDuration => $_getI64(8);
  @$pb.TagNumber(9)
  set applyDuration($fixnum.Int64 v) { $_setInt64(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasApplyDuration() => $_has(8);
  @$pb.TagNumber(9)
  void clearApplyDuration() => clearField(9);

  @$pb.TagNumber(10)
  $core.bool get proOnly => $_getBF(9);
  @$pb.TagNumber(10)
  set proOnly($core.bool v) { $_setBool(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasProOnly() => $_has(9);
  @$pb.TagNumber(10)
  void clearProOnly() => clearField(10);

  @$pb.TagNumber(11)
  $core.bool get invisible => $_getBF(10);
  @$pb.TagNumber(11)
  set invisible($core.bool v) { $_setBool(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasInvisible() => $_has(10);
  @$pb.TagNumber(11)
  void clearInvisible() => clearField(11);

  @$pb.TagNumber(12)
  $core.bool get present => $_getBF(11);
  @$pb.TagNumber(12)
  set present($core.bool v) { $_setBool(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasPresent() => $_has(11);
  @$pb.TagNumber(12)
  void clearPresent() => clearField(12);
}

class Shelf extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Shelf', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'syncMillis')
    ..m<$core.String, Artifact>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'artifacts', entryClassName: 'Shelf.ArtifactsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OM, valueCreator: Artifact.create, packageName: const $pb.PackageName('com.skllzz.api'))
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Shelf._() : super();
  factory Shelf({
    $fixnum.Int64? syncMillis,
    $core.Map<$core.String, Artifact>? artifacts,
    $core.int? version,
  }) {
    final _result = create();
    if (syncMillis != null) {
      _result.syncMillis = syncMillis;
    }
    if (artifacts != null) {
      _result.artifacts.addAll(artifacts);
    }
    if (version != null) {
      _result.version = version;
    }
    return _result;
  }
  factory Shelf.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Shelf.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Shelf clone() => Shelf()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Shelf copyWith(void Function(Shelf) updates) => super.copyWith((message) => updates(message as Shelf)) as Shelf; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Shelf create() => Shelf._();
  Shelf createEmptyInstance() => create();
  static $pb.PbList<Shelf> createRepeated() => $pb.PbList<Shelf>();
  @$core.pragma('dart2js:noInline')
  static Shelf getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Shelf>(create);
  static Shelf? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get syncMillis => $_getI64(0);
  @$pb.TagNumber(1)
  set syncMillis($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSyncMillis() => $_has(0);
  @$pb.TagNumber(1)
  void clearSyncMillis() => clearField(1);

  @$pb.TagNumber(2)
  $core.Map<$core.String, Artifact> get artifacts => $_getMap(1);

  @$pb.TagNumber(3)
  $core.int get version => $_getIZ(2);
  @$pb.TagNumber(3)
  set version($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasVersion() => $_has(2);
  @$pb.TagNumber(3)
  void clearVersion() => clearField(3);
}

class Application extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Application', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..e<Artifact_Type>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: Artifact_Type.unknown, valueOf: Artifact_Type.valueOf, enumValues: Artifact_Type.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'targetProfileId')
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'attrs', $pb.PbFieldType.OY)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ownerProfileId')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'anonymous')
    ..hasRequiredFields = false
  ;

  Application._() : super();
  factory Application({
    Artifact_Type? type,
    $core.String? targetProfileId,
    $core.List<$core.int>? attrs,
    $core.String? ownerProfileId,
    $core.bool? anonymous,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (targetProfileId != null) {
      _result.targetProfileId = targetProfileId;
    }
    if (attrs != null) {
      _result.attrs = attrs;
    }
    if (ownerProfileId != null) {
      _result.ownerProfileId = ownerProfileId;
    }
    if (anonymous != null) {
      _result.anonymous = anonymous;
    }
    return _result;
  }
  factory Application.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Application.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Application clone() => Application()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Application copyWith(void Function(Application) updates) => super.copyWith((message) => updates(message as Application)) as Application; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Application create() => Application._();
  Application createEmptyInstance() => create();
  static $pb.PbList<Application> createRepeated() => $pb.PbList<Application>();
  @$core.pragma('dart2js:noInline')
  static Application getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Application>(create);
  static Application? _defaultInstance;

  @$pb.TagNumber(1)
  Artifact_Type get type => $_getN(0);
  @$pb.TagNumber(1)
  set type(Artifact_Type v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get targetProfileId => $_getSZ(1);
  @$pb.TagNumber(2)
  set targetProfileId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTargetProfileId() => $_has(1);
  @$pb.TagNumber(2)
  void clearTargetProfileId() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get attrs => $_getN(2);
  @$pb.TagNumber(3)
  set attrs($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAttrs() => $_has(2);
  @$pb.TagNumber(3)
  void clearAttrs() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get ownerProfileId => $_getSZ(3);
  @$pb.TagNumber(4)
  set ownerProfileId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasOwnerProfileId() => $_has(3);
  @$pb.TagNumber(4)
  void clearOwnerProfileId() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get anonymous => $_getBF(4);
  @$pb.TagNumber(5)
  set anonymous($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasAnonymous() => $_has(4);
  @$pb.TagNumber(5)
  void clearAnonymous() => clearField(5);
}

class TeleportAttrs extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TeleportAttrs', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'days', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  TeleportAttrs._() : super();
  factory TeleportAttrs({
    $core.int? days,
  }) {
    final _result = create();
    if (days != null) {
      _result.days = days;
    }
    return _result;
  }
  factory TeleportAttrs.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TeleportAttrs.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TeleportAttrs clone() => TeleportAttrs()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TeleportAttrs copyWith(void Function(TeleportAttrs) updates) => super.copyWith((message) => updates(message as TeleportAttrs)) as TeleportAttrs; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TeleportAttrs create() => TeleportAttrs._();
  TeleportAttrs createEmptyInstance() => create();
  static $pb.PbList<TeleportAttrs> createRepeated() => $pb.PbList<TeleportAttrs>();
  @$core.pragma('dart2js:noInline')
  static TeleportAttrs getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TeleportAttrs>(create);
  static TeleportAttrs? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get days => $_getIZ(0);
  @$pb.TagNumber(1)
  set days($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDays() => $_has(0);
  @$pb.TagNumber(1)
  void clearDays() => clearField(1);
}

class ActiveArtifact extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ActiveArtifact', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..e<Artifact_Type>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: Artifact_Type.unknown, valueOf: Artifact_Type.valueOf, enumValues: Artifact_Type.values)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'firstActivityDay')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastActivityDay')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'applicatorProfileId')
    ..hasRequiredFields = false
  ;

  ActiveArtifact._() : super();
  factory ActiveArtifact({
    Artifact_Type? type,
    $fixnum.Int64? firstActivityDay,
    $fixnum.Int64? lastActivityDay,
    $core.String? applicatorProfileId,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (firstActivityDay != null) {
      _result.firstActivityDay = firstActivityDay;
    }
    if (lastActivityDay != null) {
      _result.lastActivityDay = lastActivityDay;
    }
    if (applicatorProfileId != null) {
      _result.applicatorProfileId = applicatorProfileId;
    }
    return _result;
  }
  factory ActiveArtifact.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ActiveArtifact.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ActiveArtifact clone() => ActiveArtifact()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ActiveArtifact copyWith(void Function(ActiveArtifact) updates) => super.copyWith((message) => updates(message as ActiveArtifact)) as ActiveArtifact; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ActiveArtifact create() => ActiveArtifact._();
  ActiveArtifact createEmptyInstance() => create();
  static $pb.PbList<ActiveArtifact> createRepeated() => $pb.PbList<ActiveArtifact>();
  @$core.pragma('dart2js:noInline')
  static ActiveArtifact getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ActiveArtifact>(create);
  static ActiveArtifact? _defaultInstance;

  @$pb.TagNumber(1)
  Artifact_Type get type => $_getN(0);
  @$pb.TagNumber(1)
  set type(Artifact_Type v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get firstActivityDay => $_getI64(1);
  @$pb.TagNumber(2)
  set firstActivityDay($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFirstActivityDay() => $_has(1);
  @$pb.TagNumber(2)
  void clearFirstActivityDay() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get lastActivityDay => $_getI64(2);
  @$pb.TagNumber(3)
  set lastActivityDay($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasLastActivityDay() => $_has(2);
  @$pb.TagNumber(3)
  void clearLastActivityDay() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get applicatorProfileId => $_getSZ(3);
  @$pb.TagNumber(4)
  set applicatorProfileId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasApplicatorProfileId() => $_has(3);
  @$pb.TagNumber(4)
  void clearApplicatorProfileId() => clearField(4);
}

