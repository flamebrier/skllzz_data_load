///
//  Generated code. Do not modify.
//  source: skllzz/common/artifact.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Artifact_Type extends $pb.ProtobufEnum {
  static const Artifact_Type unknown = Artifact_Type._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'unknown');
  static const Artifact_Type booster = Artifact_Type._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'booster');
  static const Artifact_Type chains = Artifact_Type._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'chains');
  static const Artifact_Type energy_drink = Artifact_Type._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'energy_drink');
  static const Artifact_Type i_am_ok_virus = Artifact_Type._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'i_am_ok_virus');
  static const Artifact_Type pay_freeze = Artifact_Type._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'pay_freeze');
  static const Artifact_Type sneakers = Artifact_Type._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'sneakers');
  static const Artifact_Type teleport = Artifact_Type._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'teleport');
  static const Artifact_Type vaccine = Artifact_Type._(8, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'vaccine');
  static const Artifact_Type vitamins = Artifact_Type._(9, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'vitamins');
  static const Artifact_Type aging_virus = Artifact_Type._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'aging_virus');
  static const Artifact_Type christmas_mood = Artifact_Type._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'christmas_mood');
  static const Artifact_Type aeg_virus = Artifact_Type._(12, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'aeg_virus');
  static const Artifact_Type anti_gravity_shield = Artifact_Type._(13, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'anti_gravity_shield');
  static const Artifact_Type pizza_present = Artifact_Type._(14, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'pizza_present');
  static const Artifact_Type yummy_present = Artifact_Type._(15, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'yummy_present');
  static const Artifact_Type train_swap = Artifact_Type._(16, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'train_swap');
  static const Artifact_Type block_train_swap = Artifact_Type._(18, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'block_train_swap');

  static const $core.List<Artifact_Type> values = <Artifact_Type> [
    unknown,
    booster,
    chains,
    energy_drink,
    i_am_ok_virus,
    pay_freeze,
    sneakers,
    teleport,
    vaccine,
    vitamins,
    aging_virus,
    christmas_mood,
    aeg_virus,
    anti_gravity_shield,
    pizza_present,
    yummy_present,
    train_swap,
    block_train_swap,
  ];

  static final $core.Map<$core.int, Artifact_Type> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Artifact_Type? valueOf($core.int value) => _byValue[value];

  const Artifact_Type._($core.int v, $core.String n) : super(v, n);
}

