///
//  Generated code. Do not modify.
//  source: skllzz/common/artifact.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use artifactDescriptor instead')
const Artifact$json = const {
  '1': 'Artifact',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 14, '6': '.com.skllzz.api.Artifact.Type', '8': const {}, '10': 'id'},
    const {'1': 'last_bought', '3': 2, '4': 1, '5': 3, '10': 'lastBought'},
    const {'1': 'last_use', '3': 3, '4': 1, '5': 3, '10': 'lastUse'},
    const {'1': 'produce_duration', '3': 4, '4': 1, '5': 3, '8': const {}, '10': 'produceDuration'},
    const {'1': 'pro_produce_duration', '3': 5, '4': 1, '5': 3, '8': const {}, '10': 'proProduceDuration'},
    const {'1': 'use_amount', '3': 6, '4': 1, '5': 5, '10': 'useAmount'},
    const {'1': 'price', '3': 7, '4': 1, '5': 1, '8': const {}, '10': 'price'},
    const {'1': 'available', '3': 8, '4': 1, '5': 8, '8': const {}, '10': 'available'},
    const {'1': 'apply_duration', '3': 9, '4': 1, '5': 3, '8': const {}, '10': 'applyDuration'},
    const {'1': 'pro_only', '3': 10, '4': 1, '5': 8, '8': const {}, '10': 'proOnly'},
    const {'1': 'invisible', '3': 11, '4': 1, '5': 8, '8': const {}, '10': 'invisible'},
    const {'1': 'present', '3': 12, '4': 1, '5': 8, '8': const {}, '10': 'present'},
  ],
  '4': const [Artifact_Type$json],
};

@$core.Deprecated('Use artifactDescriptor instead')
const Artifact_Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'unknown', '2': 0},
    const {'1': 'booster', '2': 1},
    const {'1': 'chains', '2': 2},
    const {'1': 'energy_drink', '2': 3},
    const {'1': 'i_am_ok_virus', '2': 4},
    const {'1': 'pay_freeze', '2': 5},
    const {'1': 'sneakers', '2': 6},
    const {'1': 'teleport', '2': 7},
    const {'1': 'vaccine', '2': 8},
    const {'1': 'vitamins', '2': 9},
    const {'1': 'aging_virus', '2': 10},
    const {'1': 'christmas_mood', '2': 11},
    const {'1': 'aeg_virus', '2': 12},
    const {'1': 'anti_gravity_shield', '2': 13},
    const {'1': 'pizza_present', '2': 14},
    const {'1': 'yummy_present', '2': 15},
    const {'1': 'train_swap', '2': 16},
    const {'1': 'block_train_swap', '2': 18},
  ],
};

/// Descriptor for `Artifact`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List artifactDescriptor = $convert.base64Decode('CghBcnRpZmFjdBIzCgJpZBgBIAEoDjIdLmNvbS5za2xsenouYXBpLkFydGlmYWN0LlR5cGVCBJC1GAFSAmlkEh8KC2xhc3RfYm91Z2h0GAIgASgDUgpsYXN0Qm91Z2h0EhkKCGxhc3RfdXNlGAMgASgDUgdsYXN0VXNlEi8KEHByb2R1Y2VfZHVyYXRpb24YBCABKANCBJC1GAFSD3Byb2R1Y2VEdXJhdGlvbhI2ChRwcm9fcHJvZHVjZV9kdXJhdGlvbhgFIAEoA0IEkLUYAVIScHJvUHJvZHVjZUR1cmF0aW9uEh0KCnVzZV9hbW91bnQYBiABKAVSCXVzZUFtb3VudBIaCgVwcmljZRgHIAEoAUIEkLUYAVIFcHJpY2USIgoJYXZhaWxhYmxlGAggASgIQgSQtRgBUglhdmFpbGFibGUSKwoOYXBwbHlfZHVyYXRpb24YCSABKANCBJC1GAFSDWFwcGx5RHVyYXRpb24SHwoIcHJvX29ubHkYCiABKAhCBJC1GAFSB3Byb09ubHkSIgoJaW52aXNpYmxlGAsgASgIQgSQtRgBUglpbnZpc2libGUSHgoHcHJlc2VudBgMIAEoCEIEkLUYAVIHcHJlc2VudCKxAgoEVHlwZRILCgd1bmtub3duEAASCwoHYm9vc3RlchABEgoKBmNoYWlucxACEhAKDGVuZXJneV9kcmluaxADEhEKDWlfYW1fb2tfdmlydXMQBBIOCgpwYXlfZnJlZXplEAUSDAoIc25lYWtlcnMQBhIMCgh0ZWxlcG9ydBAHEgsKB3ZhY2NpbmUQCBIMCgh2aXRhbWlucxAJEg8KC2FnaW5nX3ZpcnVzEAoSEgoOY2hyaXN0bWFzX21vb2QQCxINCglhZWdfdmlydXMQDBIXChNhbnRpX2dyYXZpdHlfc2hpZWxkEA0SEQoNcGl6emFfcHJlc2VudBAOEhEKDXl1bW15X3ByZXNlbnQQDxIOCgp0cmFpbl9zd2FwEBASFAoQYmxvY2tfdHJhaW5fc3dhcBAS');
@$core.Deprecated('Use shelfDescriptor instead')
const Shelf$json = const {
  '1': 'Shelf',
  '2': const [
    const {'1': 'sync_millis', '3': 1, '4': 1, '5': 3, '8': const {}, '10': 'syncMillis'},
    const {'1': 'artifacts', '3': 2, '4': 3, '5': 11, '6': '.com.skllzz.api.Shelf.ArtifactsEntry', '10': 'artifacts'},
    const {'1': 'version', '3': 3, '4': 1, '5': 5, '8': const {}, '10': 'version'},
  ],
  '3': const [Shelf_ArtifactsEntry$json],
};

@$core.Deprecated('Use shelfDescriptor instead')
const Shelf_ArtifactsEntry$json = const {
  '1': 'ArtifactsEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 11, '6': '.com.skllzz.api.Artifact', '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `Shelf`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List shelfDescriptor = $convert.base64Decode('CgVTaGVsZhIlCgtzeW5jX21pbGxpcxgBIAEoA0IEkLUYAVIKc3luY01pbGxpcxJCCglhcnRpZmFjdHMYAiADKAsyJC5jb20uc2tsbHp6LmFwaS5TaGVsZi5BcnRpZmFjdHNFbnRyeVIJYXJ0aWZhY3RzEh4KB3ZlcnNpb24YAyABKAVCBJC1GAFSB3ZlcnNpb24aVgoOQXJ0aWZhY3RzRW50cnkSEAoDa2V5GAEgASgJUgNrZXkSLgoFdmFsdWUYAiABKAsyGC5jb20uc2tsbHp6LmFwaS5BcnRpZmFjdFIFdmFsdWU6AjgB');
@$core.Deprecated('Use applicationDescriptor instead')
const Application$json = const {
  '1': 'Application',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.com.skllzz.api.Artifact.Type', '10': 'type'},
    const {'1': 'target_profile_id', '3': 2, '4': 1, '5': 9, '10': 'targetProfileId'},
    const {'1': 'attrs', '3': 3, '4': 1, '5': 12, '10': 'attrs'},
    const {'1': 'owner_profile_id', '3': 4, '4': 1, '5': 9, '10': 'ownerProfileId'},
    const {'1': 'anonymous', '3': 5, '4': 1, '5': 8, '10': 'anonymous'},
  ],
};

/// Descriptor for `Application`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List applicationDescriptor = $convert.base64Decode('CgtBcHBsaWNhdGlvbhIxCgR0eXBlGAEgASgOMh0uY29tLnNrbGx6ei5hcGkuQXJ0aWZhY3QuVHlwZVIEdHlwZRIqChF0YXJnZXRfcHJvZmlsZV9pZBgCIAEoCVIPdGFyZ2V0UHJvZmlsZUlkEhQKBWF0dHJzGAMgASgMUgVhdHRycxIoChBvd25lcl9wcm9maWxlX2lkGAQgASgJUg5vd25lclByb2ZpbGVJZBIcCglhbm9ueW1vdXMYBSABKAhSCWFub255bW91cw==');
@$core.Deprecated('Use teleportAttrsDescriptor instead')
const TeleportAttrs$json = const {
  '1': 'TeleportAttrs',
  '2': const [
    const {'1': 'days', '3': 1, '4': 1, '5': 5, '10': 'days'},
  ],
};

/// Descriptor for `TeleportAttrs`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List teleportAttrsDescriptor = $convert.base64Decode('Cg1UZWxlcG9ydEF0dHJzEhIKBGRheXMYASABKAVSBGRheXM=');
@$core.Deprecated('Use activeArtifactDescriptor instead')
const ActiveArtifact$json = const {
  '1': 'ActiveArtifact',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.com.skllzz.api.Artifact.Type', '10': 'type'},
    const {'1': 'first_activity_day', '3': 2, '4': 1, '5': 3, '10': 'firstActivityDay'},
    const {'1': 'last_activity_day', '3': 3, '4': 1, '5': 3, '10': 'lastActivityDay'},
    const {'1': 'applicator_profile_id', '3': 4, '4': 1, '5': 9, '10': 'applicatorProfileId'},
  ],
};

/// Descriptor for `ActiveArtifact`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List activeArtifactDescriptor = $convert.base64Decode('Cg5BY3RpdmVBcnRpZmFjdBIxCgR0eXBlGAEgASgOMh0uY29tLnNrbGx6ei5hcGkuQXJ0aWZhY3QuVHlwZVIEdHlwZRIsChJmaXJzdF9hY3Rpdml0eV9kYXkYAiABKANSEGZpcnN0QWN0aXZpdHlEYXkSKgoRbGFzdF9hY3Rpdml0eV9kYXkYAyABKANSD2xhc3RBY3Rpdml0eURheRIyChVhcHBsaWNhdG9yX3Byb2ZpbGVfaWQYBCABKAlSE2FwcGxpY2F0b3JQcm9maWxlSWQ=');
