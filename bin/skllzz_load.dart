import 'dart:convert';
import 'dart:io';

import 'package:fixnum/fixnum.dart';

import 'generated/common.pb.dart';
import 'postgres.dart';

loadData() async {
  final trainingsFile =
      // File('/home/flamebrier/Загрузки/skllzz_export/sessions.nljson');
  File('/home/flamebrier/Загрузки/skllzz_export/production/sessions.nljson');

  List<TrainingSession> trainingsController = <TrainingSession>[];
/*
  List<MapEntry<TrainingSession, HrDetails>> hrDetailsController =
      <MapEntry<TrainingSession, HrDetails>>[];
  List<MapEntry<TrainingSession, StepDetails>> stepDetailsController =
      <MapEntry<TrainingSession, StepDetails>>[];
*/

  /// Заполнение таблицы с тренировками
  final List<String> trainingJson = await trainingsFile.readAsLines();

  for (final i in trainingJson) {
    if (i.isNotEmpty) {
      try {
        Map<String, dynamic> training = jsonDecode(i);
        TrainingSession ts = TrainingSession(
          id: training["id"],
          startMillis: Int64.parseInt(training["startMillis"]),
          stopMillis: Int64.parseInt(training["stopMillis"]),
          skllzz: training["skllzz"].toDouble(),
          kkal: training["kkal"].toDouble(),
          profileId: training["profileId"],
          timezone: training["timezone"],
          sourceId: training["sourceId"],
        );
        if (training["hr"] != null) {
          final Map<String, dynamic> hr = training["hr"];
          ts = ts
            ..hr = HrDetails(
                minHr: hr["minHr"],
                avgHr: hr["avgHr"],
                maxHr: hr["maxHr"],
                minHardness: hr["minHardness"].toDouble(),
                avgHardness: hr["avgHardness"].toDouble(),
                maxHardness: hr["maxHardness"].toDouble());
        } else if (training["steps"] != null) {
          final Map<String, dynamic> steps = training["steps"];
          ts = ts
            ..steps = StepDetails(
                steps: steps["steps"],
                day: steps["day"],
                meters: steps["meters"]);
        }
        trainingsController.add(ts);
      } catch (e) {
        print(e);
        continue;
      }
    }
  }

  final PostgresHandle handle = PostgresHandle();

  try {
    await handle.disconnect();
    await handle.connect();
    await handle.createSequences();
    await handle.disconnect();
    await handle.trainingSessionFill(Stream.fromIterable(trainingsController));
    // await handle.hrDetailsFill(Stream.fromIterable(hrDetailsController));
    // await handle.stepDetailsFill(Stream.fromIterable(stepDetailsController));
    // await handle.hrDataFill(Stream.fromIterable());
    // await handle.stepDataFill(Stream.fromIterable());
  } catch (e, s) {
    print(e.toString());
    print(s.toString());
  } finally {
    await handle.disconnect();
  }
}

void main(List<String> arguments) async {
  await loadData();
}
