import 'dart:io';

import 'package:postgres/postgres.dart';

import 'generated/common.pb.dart';

/// postgresql://postgres:@192.168.16.1/postgres
/// docker run --network host --name pg --rm --env POSTGRES_HOST_AUTH_METHOD=trust --mount source=postgres,target=/app postgres
/// docker run --network host -d -p 8080:8080 --name pentaho --rm --mount source=pentaho,destination=/home/flamebrier/dev/pentaho advatys/pentaho-di

class PostgresHandle {
  PostgreSQLConnection? connection;

  PostgresHandle();

  Future<void> connect() async {
    var conn =
        PostgreSQLConnection("0.0.0.0", 5432, "postgres", username: "postgres");
    await conn.open();
    connection = conn;
  }

  Future<bool> checkConnection() async {
    final check = await connection?.query("SELECT datname FROM pg_database;");

    if ((check != null) && (check.isNotEmpty)) {
      print("Postgres работает");
      return true;
    } else {
      print("Ошибка подключения");
      return false;
    }
  }

  clear() async {
    if (connection == null) {
      throw Exception("Соединение не открыто!");
    }
    final fileDrop = File('doc/drop_tables.sql');
    final dropTablesCommand = await fileDrop.readAsLines();
    try {
      for (final cmd in dropTablesCommand) {
        final res = await connection?.query(cmd);
        print(res.toString());
      }
    } catch (e) {
      rethrow;
    }
  }

  createSequences() async {
    final fileSeq = File('doc/create_sequence_ts.sql');
    final List<String> seqCommand = (await fileSeq.readAsString()).split(";");

    try {
      await connection?.transaction((connection) async {
        for (String cmd in seqCommand) {
          await connection.query(cmd);
        }
      });
    } catch (e) {
      rethrow;
    }
  }

  int lastRows = 0;

  Future<int> insertValue(String cmd, String name, dynamic value,
      {required PostgreSQLExecutionContext connection}) async {
    try {
      if (!cmd.contains("@")) {
        await connection.query(cmd);
        return 1;
      }
      if (cmd.contains("@$name")) {
        return (await connection.query(cmd, substitutionValues: {name: value}))
            .affectedRowCount;
      }
    } catch (e) {
      print(e);
    }
    return 0;
  }

  int affectedRows = 0;

  trainingSessionFill(Stream<TrainingSession> objs) async {
    final fileInsert = File('doc/insert_ts_cmd.sql');
    final List<String> insertCommand =
        (await fileInsert.readAsString()).split(";");

    final fileHr = File('doc/insert_hr_details_cmd.sql');
    final List<String> insertHrCommand =
        (await fileHr.readAsString()).split(";");

    final fileSteps = File('doc/insert_step_details_cmd.sql');
    final List<String> insertStepsCommand =
        (await fileSteps.readAsString()).split(";");

    final startTime = DateTime.now();
    print("----Старт записи trainingSession в Postgres $startTime");
    await connect();
      await for (final i in objs) {
        int res = 0;
        await connection?.transaction((connection) async {
          for (String cmd in insertCommand) {
          if (!cmd.contains("@")) {
            connection.query(cmd);
            continue;
          }
          /*if (cmd.contains("@id")) {
            res +=
                (await connection.query(cmd, substitutionValues: {"id": i.id}))
                    .affectedRowCount;
            continue;
          }*/
          if (cmd.contains("@kkal")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"kkal": i.kkal}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@profile_id")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"profile_id": i.profileId}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@source_id")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"source_id": i.sourceId}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@start_millis")) {
            res += (await connection.query(cmd, substitutionValues: {
              "start_millis": i.startMillis.toInt()
            }))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@stop_millis")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"stop_millis": i.stopMillis.toInt()}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@sync_millis")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"sync_millis": i.syncMillis.toInt()}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@skllzz")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"skllzz": i.skllzz}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@timezone")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"timezone": i.timezone}))
                .affectedRowCount;
            continue;
          }
        }
        affectedRows += res > 3 ? 1 : 0;
        if (i.hasHr()) {
          HrDetails hd = i.hr;
          int res = 0;
          for (String cmd in insertHrCommand) {
            if (!cmd.contains("@")) {
              connection.query(cmd);
              continue;
            }
            if (cmd.contains("@minHr")) {
              res += (await connection
                      .query(cmd, substitutionValues: {"minHr": hd.minHr}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@avgHr")) {
              res += (await connection
                      .query(cmd, substitutionValues: {"avgHr": hd.avgHr}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@maxHr")) {
              res += (await connection
                      .query(cmd, substitutionValues: {"maxHr": hd.maxHr}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@minHardness")) {
              res += (await connection.query(cmd,
                      substitutionValues: {"minHardness": hd.minHardness}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@maxHardness")) {
              res += (await connection.query(cmd,
                      substitutionValues: {"maxHardness": hd.maxHardness}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@avgHardness")) {
              res += (await connection.query(cmd,
                      substitutionValues: {"avgHardness": hd.avgHardness}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@device")) {
              res += (await connection
                      .query(cmd, substitutionValues: {"device": hd.device}))
                  .affectedRowCount;
              continue;
            }
          }
          affectedRows += res > 3 ? 1 : 0;
        } else if (i.hasSteps()) {
          StepDetails st = i.steps;
          int res = 0;
          for (String cmd in insertStepsCommand) {
            if (!cmd.contains("@")) {
              connection.query(cmd);
              continue;
            }
            /*if (cmd.contains("@stId")) {
            res +=
                (await connection.query(cmd, substitutionValues: {"stId": ts}))
                    .affectedRowCount;
            continue;
          }*/
            if (cmd.contains("@stepDay")) {
              res += (await connection
                      .query(cmd, substitutionValues: {"stepDay": st.day}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@meters")) {
              res += (await connection
                      .query(cmd, substitutionValues: {"meters": st.meters}))
                  .affectedRowCount;
              continue;
            }
            if (cmd.contains("@steps")) {
              res += (await connection
                      .query(cmd, substitutionValues: {"steps": st.steps}))
                  .affectedRowCount;
              continue;
            }
          }
          affectedRows += res > 3 ? 1 : 0;
        }
        });
        if (affectedRows ~/ 100000 > lastRows) {
          lastRows = affectedRows ~/ 100000;
          print("${DateTime.now()} $affectedRows");
        }
      }
    await disconnect();
    print("----Добавлено записей: $affectedRows");
  }

  trainingSessionSelect() async {
    final fileSelect = File('doc/select.sql');
    final cmd = await fileSelect.readAsString();
    final res = await connection?.query(cmd);
    print("--- select res: ${res?.columnDescriptions}");
  }

  hrDetailsFill(HrDetails hd, List<String> insertCommand) async {
    await connection?.transaction((connection) async {
      int res = 0;
      for (String cmd in insertCommand) {
        if (!cmd.contains("@")) {
          connection.query(cmd);
          continue;
        }
        /*if (cmd.contains("@tdId")) {
          res +=
              (await connection.query(cmd, substitutionValues: {"tdId": tsId}))
                  .affectedRowCount;
          continue;
        }*/
        if (cmd.contains("@minHr")) {
          res += (await connection
                  .query(cmd, substitutionValues: {"minHr": hd.minHr}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@avgHr")) {
          res += (await connection
                  .query(cmd, substitutionValues: {"avgHr": hd.avgHr}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@maxHr")) {
          res += (await connection
                  .query(cmd, substitutionValues: {"maxHr": hd.maxHr}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@minHardness")) {
          res += (await connection.query(cmd,
                  substitutionValues: {"minHardness": hd.minHardness}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@maxHardness")) {
          res += (await connection.query(cmd,
                  substitutionValues: {"maxHardness": hd.maxHardness}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@avgHardness")) {
          res += (await connection.query(cmd,
                  substitutionValues: {"avgHardness": hd.avgHardness}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@device")) {
          res += (await connection
                  .query(cmd, substitutionValues: {"device": hd.device}))
              .affectedRowCount;
          continue;
        }
      }
      affectedRows += res > 3 ? 1 : 0;
    });
  }

  stepDetailsFill(StepDetails st, List<String> insertCommand) async {
    await connection?.transaction((connection) async {
      int res = 0;
      for (String cmd in insertCommand) {
        if (!cmd.contains("@")) {
          connection.query(cmd);
          continue;
        }
        /*if (cmd.contains("@stId")) {
            res +=
                (await connection.query(cmd, substitutionValues: {"stId": ts}))
                    .affectedRowCount;
            continue;
          }*/
        if (cmd.contains("@stepDay")) {
          res += (await connection
                  .query(cmd, substitutionValues: {"stepDay": st.day}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@meters")) {
          res += (await connection
                  .query(cmd, substitutionValues: {"meters": st.meters}))
              .affectedRowCount;
          continue;
        }
        if (cmd.contains("@steps")) {
          res += (await connection
                  .query(cmd, substitutionValues: {"steps": st.steps}))
              .affectedRowCount;
          continue;
        }
      }
      affectedRows += res > 3 ? 1 : 0;
    });
  }

  hrDataFill(Stream<TrainingData> objs) async {
    if (connection == null) {
      throw Exception("Соединение не открыто!");
    }
    final fileInsert = File('doc/insert_hr_cmd.sql');
    final List<String> insertCommand =
        (await fileInsert.readAsString()).split(";");

    int affectedRows = 0;

    final startTime = DateTime.now();
    print("----Старт записи stepsDetails в Postgres $startTime");
    await for (final i in objs) {
      // TrainingData ts = i;
      // HrDetails hd = i.value;
      await connection?.transaction((connection) async {
        int res = 0;
        for (String cmd in insertCommand) {
          if (!cmd.contains("@")) {
            connection.query(cmd);
            continue;
          }
          if (cmd.contains("@deviceName")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"deviceName": i.deviceName}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@duration")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"duration": i.duration}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@deviceId")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"deviceId": i.deviceId}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@hrAvg")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"hrAvg": i.hrAvg}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@hardness")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"hardness": i.hardness}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@profileId")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"profileId": i.profileId}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@skllzz")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"skllzz": i.skllzz}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@stampMillis")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"stampMillis": i.stampMillis.toInt()}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@sessionId")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"sessionId": i.sessionId}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@syncMillis")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"syncMillis": i.syncMillis.toInt()}))
                .affectedRowCount;
            continue;
          }
        }
        affectedRows += res > 3 ? 1 : 0;
      });
      print("$affectedRows");
    }
    print("----Изменено строк: $affectedRows");
  }

  stepDataFill(Stream<StepsData> objs) async {
    if (connection == null) {
      throw Exception("Соединение не открыто!");
    }

    final fileInsert = File('doc/insert_step_cmd.sql');
    final List<String> insertCommand =
        (await fileInsert.readAsString()).split(";");

    int affectedRows = 0;

    final startTime = DateTime.now();
    print("----Старт записи stepsDetails в Postgres $startTime");
    await for (final i in objs) {
      await connection?.transaction((connection) async {
        int res = 0;
        for (String cmd in insertCommand) {
          if (!cmd.contains("@")) {
            connection.query(cmd);
            continue;
          }
          if (cmd.contains("@duration")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"duration": i.duration}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@steps")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"steps": i.steps}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@source")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"source": i.stepSource}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@sourceId")) {
            res += (await connection
                    .query(cmd, substitutionValues: {"sourceId": i.sourceId}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@syncMillis")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"syncMillis": i.syncMillis.toInt()}))
                .affectedRowCount;
            continue;
          }
          if (cmd.contains("@stampMillis")) {
            res += (await connection.query(cmd,
                    substitutionValues: {"stampMillis": i.stampMillis.toInt()}))
                .affectedRowCount;
            continue;
          }
        }
        affectedRows += res > 3 ? 1 : 0;
      });
      print("$affectedRows");
    }
    print("----Изменено строк: $affectedRows");
  }

  Future<void> disconnect() async {
    await connection?.close();
  }
}
